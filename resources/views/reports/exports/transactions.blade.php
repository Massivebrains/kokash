<html>
<link rel="stylesheet" type="text/css" href="{{asset('css/report.css')}}">
<body>

    <table class="table table-sm table-striped">
        <thead>

            <tr>
                <th style="font-size:1rem !important; text-decoration: uppercase; text-align:center" colspan="9">
                    {{$company->name. ' '.$company->description}}
                </th>
            </tr>
            <tr>
                <th colspan="9" style="text-align:center">Transactions {{$start_month}} - {{$end_month}} Generated {{date('Y-m-d H:i:s')}}</th>
            </tr>
            <tr>
                <th>Member</th>
                <th>Type</th>
                <th>Source</th>
                <th>Reference</th>
                <th>Amount</th>
                <th>Balance</th>
                 <th>Status</th>
                <th>Transaction Date</th>
                <th>Description</th>
               
            </tr>
        </thead>

        <tbody>
            @foreach($transactions as $row)
            <tr>
                <td>{{$row->user->name}}</td>       
                <td>{{strtoupper($row->transaction_type->name)}}</td>       
                <td>{{strtoupper($row->payment_source->name)}}</td>       
                <td>{{$row->reference}}</td>       
                <td>{{_c($row->amount)}}</td>      
                <td>{{_c($row->running_balance)}}</td>      
                <td>{{strtoupper($row->status)}}</td>      
                <td>{{_d($row->created_at)}}</td>      
                <td><small>{{$row->description}}</small></td>       
            </tr>
            @endforeach

            <tr>
                <td style="text-align:right; text-color:#ccc" colspan="9"><small>Powered by <strong>kokash.ng</strong></small></td>
            </tr>
        </tbody>

    </table>
</body>
</html>