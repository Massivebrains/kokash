<html>
<link rel="stylesheet" type="text/css" href="{{asset('css/report.css')}}">
<body>

    <table class="table table-sm table-striped">
        <thead>

            <tr>
                <th colspan="6" style="font-size:1rem !important; text-decoration: uppercase; text-align:center">
                    {{$company->name. ' '.$company->description}}
                </th>
            </tr>
            <tr>
                <th colspan="6" style="text-align:center">Daily Payouts {{$date}} Generated {{date('Y-m-d H:i:s')}}</th>
            </tr>
            <tr>
                <th>Member</th>
                <th>Order Reference</th>
                <th>Amount</th>
                <th>Transaction Reference</th>
                <th>Date</th>
                <th>Status</th>
            </tr>
        </thead>

        <tbody>

            @php $total = 0 @endphp

            @foreach($orders as $row)

            @php $total+= $row->sub_total @endphp

            <tr>
                <td>{{$row->user->name}}</td>       
                <td>{{$row->order_reference}}</td>       
                <td>{{_c($row->sub_total)}}</td>       
                <td>{{$row->transaction_reference}}</td>       
                <td>{{_d($row->transaction_date)}}</td>       
                <td>{{strtoupper($row->status)}}</td>     
            </tr>
            @endforeach

            <tr>
                <th colspan="2">Total</th>       
                <th>{{_c($total)}}</th>  
                <th colspan="3">&nbsp;</th>  
            </tr>

            <tr>
                <td colspan="6" style="text-align:right; text-color:#ccc"><small>Powered by <strong>kokash.ng</strong></small></td>
            </tr>
        </tbody>

    </table>
</body>
</html>