<html>
<link rel="stylesheet" type="text/css" href="{{asset('css/report.css')}}">
<body>

    <table class="table table-sm table-striped">
        <thead>

            <tr>
                <th colspan="10" style="font-size:1rem !important; text-decoration: uppercase; text-align:center">
                    {{$company->name. ' '.$company->description}}
                </th>
            </tr>
            <tr>
                <th colspan="10" style="text-align:center">Savings Accounts Generated {{date('Y-m-d H:i:s')}}</th>
            </tr>
            <tr>
                <th>Member</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Status</th>
                <th>Kokash Account Number</th>
                <th>Bank</th>
                <th>Bank Account Number</th>
                <th>Bank Account Name</th>
                <th>Monthly Savings</th>
                <th>Current Balance</th>
            </tr>
        </thead>

        <tbody>
            @foreach($accounts as $row)
            <tr>
                <td>{{$row->name}}</td>       
                <td>{{$row->email}}</td>       
                <td>{{$row->phone}}</td>       
                <td>{{strtoupper($row->status)}}</td>      
                <td>{{$row->savings_account->account_number}}</td>       
                <td>{{$row->savings_account->bank->name}}</td>        
                <td>{{$row->savings_account->bank_account_number}}</td>     
                <td>{{$row->savings_account->bank_account_name}}</td>     
                <td>{{_c($row->savings_account->monthly_savings)}}</td>     
                <td>{{_c($row->savings_account->balance)}}</td>
            </tr>
            @endforeach

            <tr>
                <td colspan="10" style="text-align:right; text-color:#ccc"><small>Powered by <strong>kokash.ng</strong></small></td>
            </tr>
        </tbody>

    </table>
</body>
</html>