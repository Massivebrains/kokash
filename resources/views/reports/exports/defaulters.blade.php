<html>
<link rel="stylesheet" type="text/css" href="{{asset('css/report.css')}}">
<body>

    <table class="table table-sm table-striped">
        <thead>

            <tr>
                <th style="font-size:1rem !important; text-decoration: uppercase; text-align:center" colspan="12">
                    {{$company->name. ' '.$company->description}}
                </th>
            </tr>
            <tr>
                <th colspan="12" style="text-align:center">Loan Defaulters {{$start_month}} - {{$end_month}} Generated {{date('Y-m-d H:i:s')}}</th>
            </tr>
            <tr>
                <th>Member</th>
                <th>Loan</th>
                <th>Loan Reference</th>
                <th>Principal</th>
                <th>Date Disbursed</th>
                <th>Outstanding</th>
                <th>Month Defaulted</th>
                <th>Reference</th>
                <th>Fine</th>
                <th>Fine Reference</th>
                <th>Fine Date</th>
                <th>Fine Status</th>
            </tr>
        </thead>

        <tbody>
            @foreach($transactions as $row)
            <tr>
                <td>{{$row->user->name}}</td>       
                <td>{{$row->loan->name}}</td>       
                <td>{{$row->loan->reference}}</td>       
                <td>{{_c($row->loan->principal)}}</td>      
                <td>{{_d($row->loan->disbursed_at, false)}}</td>       
                <td>{{_c($row->loan->outstanding)}}</td>        
                <td>{{_month($row->date)}}</td>     
                <td>{{$row->reference}}</td>     
                <td>{{_c($row->fine->amount)}}</td>     
                <td>{{$row->fine->reference}}</td>     
                <td>{{_d($row->fine_date, false)}}</td>        
                <td>{{strtoupper($row->fine->status)}}</td>     
            </tr>
            @endforeach

            <tr>
                <td style="text-align:right; text-color:#ccc" colspan="12"><small>Powered by <strong>kokash.ng</strong></small></td>
            </tr>
        </tbody>

    </table>
</body>
</html>