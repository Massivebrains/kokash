<html>
<link rel="stylesheet" type="text/css" href="{{asset('css/report.css')}}">
<body>

    <table class="table table-sm table-striped">
        <thead>

            <tr>
                <th colspan="16" style="font-size:1rem !important; text-decoration: uppercase; text-align:center">
                    {{$company->name. ' '.$company->description}}
                </th>
            </tr>
            <tr>
                <th colspan="16" style="text-align:center">Loans {{$start_month}} - {{$end_month}} Generated {{date('Y-m-d H:i:s')}}</th>
            </tr>
            <tr>
                <th>Member</th>
                <th>Loan</th>
                <th>Reference</th>
                <th>Principal</th>
                <th>Rate</th>
                <th>Time</th>
                <th>Interest</th>
                <th>Gross</th>
                <th>Outstanding</th>
                <th>Paid</th>
                <th>Monthly Payment</th>
                <th>Type</th>
                <th>Status</th>
                <th>Start</th>
                <th>Disbursed</th>
                <th>Notes</th>
            </tr>
        </thead>

        <tbody>
            @foreach($loans as $row)
            <tr>
                <td>{{$row->user->name}}</td>       
                <td>{{$row->name}}</td>       
                <td>{{$row->reference}}</td>       
                <td>{{_c($row->principal)}}</td>       
                <td>{{_percent($row->rate)}}</td>      
                <td>{{_years($row->time)}}</td>      
                <td>{{_c($row->interest)}}</td>      
                <td>{{_c($row->amount)}}</td>      
                <td>{{_c($row->outstanding)}}</td>      
                <td>{{_c(\App\LoanTransaction::where(['loan_id' => $row->id, 'status' => 'completed'])->sum('amount'))}}</td>      
                <td>{{_c($row->monthly_payment)}}</td>      
                <td>{{strtoupper($row->type)}}</td>      
                <td>{{strtoupper($row->status)}}</td>      
                <td>{{_d($row->start_at, false)}}</td>      
                <td>{{_d($row->disbursed_at, false)}}</td>      
                <td><small>{{$row->disbursement_notes}}</small></td>       
            </tr>
            @endforeach

            <tr>
                <td style="text-align:right; text-color:#ccc" colspan="16"><small>Powered by <strong>kokash.ng</strong></small></td>
            </tr>
        </tbody>

    </table>
</body>
</html>