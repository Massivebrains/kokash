@extends('app')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Daily Payouts</h3>
            </div>
            <div class="block-content">

                <form action="{{url('report-daily-payouts')}}" method="post" target="_blank">

                    {{csrf_field()}}

                    <div class="row">


                        <div class="form-group col-md-12">
                            <label>Select Transaction Day</label>
                            <input type="text" class="form-control date" name="date" required>
                        </div>

                    </div>

                    <div class="form-group row">
                        <div class="col-md-9">
                            <input type="hidden" name="report_id" value="1">
                            <button type="submit" class="btn btn-alt-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

    <div class="col-md-12">

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Other Reports</h3>
            </div>
            <div class="block-content">

                <form action="{{url('report')}}" method="post" target="_blank">

                    {{csrf_field()}}

                    <div class="row">

                        <div class="form-group col-md-4">
                            <label>Select Report</label>
                            <select class="select2 form-control" name="report" required>
                               <option>--select--</option>
                               @foreach(_reports() as $row)
                               <option value="{{$row}}">{{$row}}</option>
                               @endforeach
                           </select>
                           
                       </div>

                       <div class="form-group col-md-4">
                        <label>Start Month</label>
                        <input type="text" class="form-control month" name="start_month" required>
                        
                    </div>

                    <div class="form-group col-md-4">
                       <label>End Month</label>
                       <input type="text" class="form-control month" name="end_month" required>
                       
                   </div>

               </div>

               <div class="form-group row">
                <div class="col-md-9">
                    <input type="hidden" name="report_id" value="1">
                    <button type="submit" class="btn btn-alt-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

</div>
</div>
@endsection