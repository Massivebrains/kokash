<!doctype html>
<html lang="en" class="no-focus"> 
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>KoKash {{$title ?? ''}}</title>

    <meta name="description" content="Cooperative Banking Platform">
    <meta name="author" content="olaiya segun">
    <meta name="robots" content="noindex, nofollow">        
    <link rel="shortcut icon" href="{{asset('img/kokash.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('img/kokash.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/kokash.png')}}">
    <link rel="stylesheet" id="css-main" href="{{asset('css/codebase.min.css')}}">
    <script src="/js/core/jquery.min.js"></script>

    <style type="text/css">

        button{

            cursor: pointer;
        }
    </style>
</head>
<body>
    <div id="page-container">

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Transaction #{{$transaction->reference}}</h3>
                <div class="block-options">

                    <button type="button" class="btn-block-option" onclick="window.print()">
                        <i class="si si-printer"></i> Print
                    </button>
                </div>
            </div>
            <div class="block-content">

                <div class="row my-20">

                    <div class="col-6">
                        <p class="h3">{{optional($company)->description}}</p>
                        <address>
                            {{optional($company)->address}}<br>
                            {{optional($company)->contact_person_email}}<br>
                            {{optional($company)->contact_person_phone}}
                        </address>
                    </div>


                    <div class="col-6 text-right">
                        <p class="h3">{{optional($user)->name}}</p>
                        <address>
                            {{optional($user)->name}}<br>
                            {{optional($user)->address}}<br>
                            {{optional($user)->email}}<br>
                            {{optional($user)->phone}}
                        </address>
                    </div>

                </div>



                <div class="table-responsive push">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th style="width:35%">Reference</th>
                                <td>{{$transaction->reference}}</td>

                            </tr>
                            <tr>
                                <th>Amount</th>
                                <td>{{_c($transaction->amount)}}</td>
                            </tr>
                            <tr>
                                <th>Running Balance</th>
                                <td>{{_c($transaction->running_balance)}}</td>
                            </tr>
                            <tr>
                                <th>Transaction Date</th>
                                <td>{{_d($transaction->created_at)}}</td>
                            </tr>
                            <tr>
                                <th>Payment Source</th>
                                <td>{{_badge($transaction->payment_source->name)}}</td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{{$transaction->description}}</td>
                            </tr>


                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>

</body>
</html>