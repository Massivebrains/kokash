@extends('app')

@section('content')

    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Transactions</h3>

            @can('update', \App\Transaction::class)
                <div class="block-options">
                    <div class="block-options-item">
                        <a href="{{ url('credit-form') }}" class="btn btn-alt-primary btn-sm"><i class="si si-plus"></i> New Transaction</a>
                        <a href="{{ url('bulk-transactions') }}" class="btn btn-alt-primary btn-sm"><i class="si si-plus"></i> Bulk Transactions</a>
                        <a href="/upload-bulk-transactions" class="btn btn-alt-primary btn-sm"><i class="si si-plus"></i> Upload Transactions</a>
                    </div>
                </div>
            @endcan

        </div>
        <div class="block-content">

            @component('components.search', ['placeholder' => 'Search Transactions by reference, amount status and description', 'url' => 'transactions', 'count' => $count ?? 0, 'query' => $query ?? null]) @endcomponent

            <table class="table table-striped table-vcenter">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Type</th>
                        <th>Source</th>
                        <th>Member</th>
                        <th>Balance</th>
                        <th>Reference</th>
                        <th class="text-center" style="width: 100px;">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transactions as $row)
                        <tr>
                            <td>{{ _d($row->created_at) }}</td>
                            <td>{{ _c($row->amount) }}</td>
                            <td>{{ _badge($row->transaction_type->name) }}</td>
                            <td>{{ _badge($row->payment_source->name) }}</td>
                            <td><a href="{{ url('member-info/' . $row->user->id) }}">{{ $row->user->name }}</a></td>
                            <td>{{ _c($row->running_balance) }}</td>
                            <td>{{ $row->reference }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ url('transaction/' . $row->id) }}" class="btn btn-sm btn-secondary js-tooltip-enabled">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>

            {{ $transactions->links() }}
        </div>
    </div>


@endsection
