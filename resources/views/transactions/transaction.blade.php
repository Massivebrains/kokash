@extends('app')

@section('content')

<div class="block">
	<div class="block-header block-header-default">
		<h3 class="block-title">{{$transaction->reference}}</h3>
		<div class="block-options">

			<button type="button" class="btn-block-option" onclick="Codebase.helpers('print-page');">
				<i class="si si-printer"></i> Print Transaction
			</button>
			<button type="button" class="btn-block-option" data-toggle="block-option" data-action="fullscreen_toggle"></button>
			<button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
				<i class="si si-refresh"></i>
			</button>
		</div>
	</div>
	<div class="block-content">

		<div class="row my-20">

			<div class="col-6">
				<p class="h3">{{$transaction->company->description}}</p>
				<address>
					{{$transaction->company->address}}<br>
					{{$transaction->company->contact_person_email}}<br>
					{{$transaction->company->contact_person_phone}}
				</address>
			</div>


			<div class="col-6 text-right">
				<p class="h3">{{$transaction->user->name}}</p>
				<address>
					{{$transaction->user->name}}<br>
					{{$transaction->user->address}}<br>
					{{$transaction->user->email}}<br>
					{{$transaction->user->phone}}
				</address>
			</div>

		</div>



		<div class="table-responsive push">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<th>Transaction Reference</th>
						<td>{{$transaction->reference}}</td>
					</tr>
					<tr>
						<th>Transaction Date</th>
						<td>{{_d($transaction->created_at)}}</td>
					</tr>
					<tr>
						<th>Transaction Amount</th>
						<th>{{_c($transaction->amount)}}</th>
					</tr>
					<tr>
						<th>Payment Source</th>
						<td>{{_badge($transaction->payment_source->name)}}</td>
					</tr>
					<tr>
						<th>Transaction Type</th>
						<td>{{_badge($transaction->transaction_type->name)}}</td>
					</tr>
					<tr>
						<th>Running Balance</th>
						<td>{{_c($transaction->running_balance)}}</td>
					</tr>
					<tr>
						<th>Description</th>
						<td><code>{{$transaction->description}}</code></td>
					</tr>

					@if($transaction->order_detail->id > 0)
					<tr>
						<th>Order</th>
						<td>
							<a href="{{url('order/'.$transaction->order_detail->order_id)}}">
								#{{$transaction->order_detail->order->order_reference}}
							</a>
						</td>
					</tr>
					@endif
					
				</tbody>
			</table>
		</div>

		<p class="text-muted text-center">Thank you very much for doing business with us. We look forward to working with you again!</p>

	</div>
</div>

@endsection