@extends('app')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">New Transaction</h3>
                <div class="block-options">

                </div>
            </div>
            <div class="block-content">

                <form action="{{url('save-credit-transaction')}}" method="post">

                    {{csrf_field()}}

                    <div class="row">

                        <div class="form-group col-md-6">
                            
                         <label>Member</label>
                         <select class="select2 form-control country" name="user_id" required>
                             <option>--select--</option>
                             @foreach($users as $row)
                             <option value="{{$row->id}}">{{$row->name}}</option>
                             @endforeach
                         </select>

                         @if($errors->has('user_id'))
                         <div class="invalid-feedback">{{$errors->first('user_id')}}</div>
                         @endif
                     </div>

                     <div class="form-group col-md-6 {{$errors->has('amount') ? 'is-invalid' : ''}}">
                        <label>Amount</label>
                        <input type="number" step="0.01" class="form-control" name="amount" value="{{old('amount')}}" required>

                        @if($errors->has('amount'))
                        <div class="invalid-feedback">{{$errors->first('amount')}}</div>
                        @endif
                    </div>

                </div>

                <div class="row">

                    <div class="form-group col-md-12 {{$errors->has('description') ? 'is-invalid' : ''}}">
                        <label>Description</label>
                        <textarea type="text" class="form-control" name="description" required>{{old('description')}}</textarea>

                        @if($errors->has('description'))
                        <div class="invalid-feedback">{{$errors->first('description')}}</div>
                        @endif
                    </div>

                </div>

                <div class="row">

                    <div class="form-group col-md-12 {{$errors->has('password') ? 'is-invalid' : ''}}">
                        <label>Your Account Password</label>
                        <input type="password" class="form-control" name="password" required>

                        @if($errors->has('password'))
                        <div class="invalid-feedback">{{$errors->first('password')}}</div>
                        @endif
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-alt-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
</div>
@endsection