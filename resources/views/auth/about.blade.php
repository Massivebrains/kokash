<!DOCTYPE html>
<html>
<head>
    <title>About Kokash</title>
    <link rel="stylesheet" id="css-main" href="{{asset('css/codebase.min.css')}}">
</head>

<body style="background:#fff">

    <div class="container" style="margin-top: 20px;">
        <div class="row">
            <div class="col-12">
                <h3>About Kokash</h3>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
            </div>

        </div>
    </div>

</body>

<script src="/js/core/jquery.min.js"></script>

</html>