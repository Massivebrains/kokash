@extends('auth')

@section('body')

<form action="{{url('login')}}" method="post">

    {{csrf_field()}}

    @if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
    @endif

    @if(session('message'))
    <div class="alert alert-success">
        {{session('message')}}
    </div>
    @endif

    <div class="form-group row">
        <div class="col-12">
            
            <label for="login-username">Email Address</label>
            <input type="text" class="form-control" name="email" required value="{{old('email')}}">
        </div>
    </div>
    <div class="form-group row">
        <div class="col-12">
            <label for="login-password">Password</label>
            <input type="password" class="form-control" name="password" required>
            
        </div>
    </div>
    <div class="form-group row gutters-tiny">
        <div class="col-12 mb-10">
            <button type="submit" class="btn btn-block btn-hero btn-noborder btn-rounded btn-alt-primary" style="cursor:pointer">
                <i class="si si-login mr-10"></i> Sign In
            </button>
        </div>
        <div class="col-sm-6 mb-5">
            <a class="btn btn-block btn-noborder btn-rounded btn-alt-secondary" href="{{url('magic-link')}}">
                <i class="fa fa-link text-muted mr-5"></i> Get Magic Link
            </a>
        </div>
        <div class="col-sm-6 mb-5">
            <a class="btn btn-block btn-noborder btn-rounded btn-alt-secondary" href="{{url('forgot-password')}}">
                <i class="fa fa-warning text-muted mr-5"></i> Forgot password
            </a>
        </div>
    </div>
</form>

@endsection