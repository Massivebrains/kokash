@extends('auth')

@section('body')

<form action="{{url('reset-password/'.$user->api_token)}}" method="post">

    {{csrf_field()}}

    @if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
    @endif

    @if(session('message'))
    <div class="alert alert-success">
        {{session('message')}}
    </div>
    @endif

    <div class="form-group row">
        <div class="col-12">
            <div class="form-material">
                <input type="text" class="form-control" required value="{{$user->email}}" disabled>
                <label for="login-username">Email Address</label>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-12 {{$errors->has('password') ? 'is-invalid' : ''}}">
            <div class="form-material">
                <input type="password" class="form-control" name="password" required>
                <label for="login-password">New Password</label>
            </div>
            @if($errors->has('password'))
            <div class="invalid-feedback">{{$errors->first('password')}}</div>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <div class="col-12">
            <div class="form-material">
                <input type="password" class="form-control" name="password_confirmation" required>
                <label for="login-password">Retype New Password</label>
            </div>
        </div>
    </div>

    <div class="form-group row gutters-tiny">
        <div class="col-12 mb-10">
            <button type="submit" class="btn btn-block btn-hero btn-noborder btn-rounded btn-alt-primary" style="cursor:pointer">
                <i class="fa fa-warning mr-5"></i> Reset Password
            </button>
        </div>
    </div>
</form>

@endsection