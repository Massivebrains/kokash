@extends('auth')

@section('body')

<form action="{{url('webview/password-reset')}}" method="post">

	{{csrf_field()}}

	@include('components.alert')

	<div class="form-group row">
		<div class="col-12">
			<label for="login-username">Phone Number</label>
			<input type="text" class="form-control" name="phone" required value="{{old('phone')}}" placeholder="Phone Number">
		</div>
	</div>

	<div class="form-group row">
		<div class="col-12">
			<label for="login-username">Cooperative</label>
			<select name="company_id" class="form-control" required>
				@foreach($companies as $row)
				<option value="{{$row->id}}">{{$row->description}} </option>
				@endforeach
			</select>
			
		</div>
	</div>

	<div class="form-group row gutters-tiny">
		<div class="col-12 mb-10">
			<button type="submit" class="btn btn-block btn-hero btn-noborder btn-rounded btn-alt-primary" style="cursor:pointer">
				<i class="fa fa-warning mr-5"></i> Reset Password
			</button>
		</div>
	</div>
</form>

@endsection