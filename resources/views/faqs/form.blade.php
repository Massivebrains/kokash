@extends('app')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Freqently Asked Question</h3>
                <div class="block-options">

                </div>
            </div>
            <div class="block-content">

                <form action="{{url('save-faq')}}" method="post">

                    {{csrf_field()}}

                    <div class="row">

                     <div class="form-group col-md-12 {{$errors->has('question') ? 'is-invalid' : ''}}">
                        <div class="form-material">
                            <textarea name="question" class="form-control" rows="3" required>{{old('question', $faq->question)}}</textarea>
                            <label>Question</label>
                        </div>
                        @if($errors->has('question'))
                        <div class="invalid-feedback">{{$errors->first('question')}}</div>
                        @endif
                    </div>
                </div>

                <div class="row">

                 <div class="form-group col-md-12 {{$errors->has('answer') ? 'is-invalid' : ''}}">
                    <div class="form-material">
                        <textarea name="answer" class="form-control" rows="3" required>{{old('answer', $faq->answer)}}</textarea>
                        <label>Answer</label>
                    </div>
                    @if($errors->has('question'))
                    <div class="invalid-feedback">{{$errors->first('answer')}}</div>
                    @endif
                </div>
            </div>

            <input type="hidden" name="id" value="{{$faq->id}}">

            @if(Gate::allows('update-faq'))
            <div class="form-group row">
                <div class="col-md-9">
                    <button type="submit" class="btn btn-alt-primary">Submit</button>
                </div>
            </div>
            @endif
            
        </form>
    </div>
</div>

</div>
</div>
@endsection