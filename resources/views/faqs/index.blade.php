@extends('app')

@section('content')

<div class="block">
	<div class="block-header block-header-default">
		<h3 class="block-title">Frequently Asked Questions</h3>
		@if(Gate::allows('update-faq'))
		<div class="block-options">
			<div class="block-options-item">
				<a href="{{url('faq/0')}}" class="btn btn-alt-primary btn-sm"><i class="si si-plus"></i> New FAQ</a>
			</div>
		</div>
		@endif
	</div>
	<div class="block-content">

		<table class="table table-striped table-vcenter">
			<thead>
				<tr>
					<th>Question</th>
					<th>Answer</th>
					<th class="text-center" style="width: 100px;">Actions</th>
				</tr>
			</thead>
			<tbody>

				@foreach($faqs as $row)
				<tr>
					<td style="width:30%">{{$row->question}}</td>
					<td>{{$row->answer}}</td>
					<td class="text-center">
						<div class="btn-group">
							<a href="{{url('faq/'.$row->id)}}" class="btn btn-sm btn-secondary js-tooltip-enabled">
								<i class="fa fa-pencil"></i>
							</a>
						</div>
					</td>
				</tr>
				@endforeach

			</tbody>
		</table>
	</div>
</div>

@endsection