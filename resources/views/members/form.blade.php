@extends('app')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">{{$user->id == 0 ? 'Setup a New Member' : $user->name}}</h3>
                <div class="block-options">

                </div>
            </div>
            <div class="block-content">

                <form action="{{url($user->id > 0 ? 'update-member' : 'create-member')}}" method="post">

                    {{csrf_field()}}

                    <div class="row">

                        <div class="form-group col-md-6 {{$errors->has('first_name') ? 'is-invalid' : ''}}">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="first_name" value="{{old('first_name', $user->first_name)}}" required>
                            @if($errors->has('first_name'))
                            <div class="invalid-feedback">{{$errors->first('first_name')}}</div>
                            @endif
                        </div>

                        <div class="form-group col-md-6 {{$errors->has('last_name') ? 'is-invalid' : ''}}">
                            <label>Last Name</label>
                            <input type="text" class="form-control" name="last_name" value="{{old('last_name', $user->last_name)}}" required>
                            @if($errors->has('last_name'))
                            <div class="invalid-feedback">{{$errors->first('last_name')}}</div>
                            @endif
                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-md-6 {{$errors->has('email') ? 'is-invalid' : ''}}">
                            <label>Email Address</label>
                            <input type="email" class="form-control" name="email" value="{{old('email', $user->email)}}">
                            @if($errors->has('email'))
                            <div class="invalid-feedback">{{$errors->first('email')}}</div>
                            @endif
                        </div>

                        <div class="form-group col-md-6 {{$errors->has('phone') ? 'is-invalid' : ''}}">
                           <label>Phone Number</label>
                           <input type="number" class="form-control" name="phone" value="{{old('phone', $user->phone)}}" required>
                           @if($errors->has('phone'))
                           <div class="invalid-feedback">{{$errors->first('phone')}}</div>
                           @endif
                       </div>

                   </div>

                   <div class="row">

                    <div class="form-group col-md-6">
                        <label>Sex</label>
                        <select class="select2 form-control" name="sex" required>
                            <option value="male" {{$user->sex == 'male' ? 'selected' : ''}}>Male</option>
                            <option value="female" {{$user->sex == 'female' ? 'selected' : ''}}>Female</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label>Marital Status</label>
                        <select class="select2 form-control" name="marital_status">
                            <option value="single" {{$user->sex == 'single' ? 'selected' : ''}}>Single</option>
                            <option value="married" {{$user->sex == 'married' ? 'selected' : ''}}>Married</option>
                        </select>
                    </div>

                </div>

                <div class="row">

                    <div class="form-group col-md-6">
                        <label>Country</label>
                        <select class="select2 form-control country" name="country_id" required>
                            @foreach(\App\Country::get() as $row)
                            @if($row->id == old('country_id', $user->country_id))
                            <option value="{{$row->id}}" selected>{{$row->name}}</option>
                            @else
                            <option value="{{$row->id}}">{{$row->name}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label>State</label>
                        <select class="select2 form-control state" name="state_id">
                            @php 
                            $state = \App\State::where(['id' => old('state_id', $user->state_id)])->first();
                            @endphp 
                            @if($state)
                            <option value="{{$state->id}}" selected>{{$state->name}}</option>
                            @else
                            <option>--select country--</option>
                            @endif
                        </select>
                    </div>

                </div>

                <div class="row">

                    <div class="form-group col-md-6 {{$errors->has('address') ? 'is-invalid' : ''}}">
                        <label>Address</label>
                        <input type="text" class="form-control" name="address" value="{{old('address', $user->address)}}">
                        @if($errors->has('address'))
                        <div class="invalid-feedback">{{$errors->first('address')}}</div>
                        @endif
                    </div>

                    <div class="form-group col-md-6 {{$errors->has('occupation') ? 'is-invalid' : ''}}">
                        <label>Occupation</label>
                        <input type="text" class="form-control" name="occupation" value="{{old('occupation', $user->occupation)}}">
                        @if($errors->has('occupation'))
                        <div class="invalid-feedback">{{$errors->first('occupation')}}</div>
                        @endif
                    </div>

                </div>

                <div class="row">

                    <div class="form-group col-md-6 {{$errors->has('employer') ? 'is-invalid' : ''}}">
                        <label>Employer</label>
                        <input type="text" class="form-control" name="employer" value="{{old('employer', $user->employer)}}">
                        @if($errors->has('employer'))
                        <div class="invalid-feedback">{{$errors->first('employer')}}</div>
                        @endif
                    </div>

                    <div class="form-group col-md-6 {{$errors->has('employer_address') ? 'is-invalid' : ''}}">
                        <label>Employer Address</label>
                        <input type="text" class="form-control" name="employer_address" value="{{old('employer_address', $user->employer_address)}}">
                        @if($errors->has('employer_adress'))
                        <div class="invalid-feedback">{{$errors->first('employer_adress')}}</div>
                        @endif
                    </div>

                </div>

                <div class="row">

                    <div class="form-group col-md-6 {{$errors->has('next_of_kin') ? 'is-invalid' : ''}}">
                        <label>Next of Kin</label>
                        <input type="text" class="form-control" name="next_of_kin"  value="{{old('next_of_kin', $user->next_of_kin)}}">
                        @if($errors->has('next_of_kin'))
                        <div class="invalid-feedback">{{$errors->first('next_of_kin')}}</div>
                        @endif
                    </div>

                    <div class="form-group col-md-6 {{$errors->has('next_of_kin_phone') ? 'is-invalid' : ''}}">
                        <label>Next of Kin Phone Number</label>
                        <input type="text" class="form-control" name="next_of_kin_phone" value="{{old('next_of_kin_phone', $user->next_of_kin_phone)}}">
                        @if($errors->has('next_of_kin_phone'))
                        <div class="invalid-feedback">{{$errors->first('next_of_kin_phone')}}</div>
                        @endif
                    </div>

                </div>

                @can('update', \App\User::class)

                <div class="form-group row">
                    <div class="col-md-9">
                        <input type="hidden" name="id" value="{{$user->id}}">
                        <button type="submit" class="btn btn-alt-primary">Submit</button>
                    </div>
                </div>

                @endcan

            </form>
        </div>
    </div>

</div>
</div>
@endsection