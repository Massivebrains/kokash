@extends('app')

@section('content')

    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">All members in {{ Auth::user()->company->name }}</h3>
            <div class="block-options">

                @can('update', \App\User::class)
                    <div class="block-options-item">
                        <a href="{{ url('member/0') }}" class="btn btn-alt-primary btn-sm"><i class="si si-plus"></i> New Member</a>
                    </div>
                @endcan

            </div>
        </div>
        <div class="block-content">

            @component('components.search', ['placeholder' => 'Search members in ' . Auth::user()->company->name, 'url' => 'members', 'count' => $count ?? 0, 'query' => $query ?? null]) @endcomponent

            <table class="table table-striped table-vcenter">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Savings Balance</th>
                        <th>Loans</th>
                        <th>Shares</th>
                        <th>Date Joined</th>
                        <th>Status</th>
                        <th class="text-center" style="width: 100px;">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $row)
                        <tr>
                            <td>{{ $row->id }}</td>
                            <td>{{ ucwords($row->name) }}</td>
                            <td>{{ $row->phone }}</td>
                            <td>{{ _c($row->savings_account->balance) }}</td>
                            <td>{{ $row->loans->count() }}</td>
                            <td>{{ number_format($row->share->units ?? 0) }}</td>
                            <td>{{ _d($row->created_at, false) }}</td>
                            <td>{{ _badge($row->status) }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ url('member/' . $row->id) }}" class="btn btn-sm btn-secondary js-tooltip-enabled">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    @if (Gate::allows('view-member-details'))
                                        <a href="{{ url('member-info/' . $row->id) }}" class="btn btn-sm btn-secondary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="View Member">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    @endif

                                    @if (Gate::allows('delete-member'))
                                        <a href="{{ url('delete-member/' . $row->id) }}" class="btn btn-sm btn-danger js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Delete Member" onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>

            {{ $users->links() }}
        </div>
    </div>

@endsection
