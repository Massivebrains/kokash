@extends('email')

@section('content')

<h2>Dear {{$user->name}},</h2>

<p>
	This is to inform you that your account has been activated. This implies
	that you would now have access to all services on the platform. 
</p>

<p style="margin-bottom:50px;">
	If you have not downloaded the Mobile App, 
	You can click <a href="https://play.google.com/store/apps/details?id=com.glcms">here</a> 
	to download the App from the Play Store.
</p>


@endsection