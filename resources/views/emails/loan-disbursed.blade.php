@extends('email')

@section('content')

<h2>Dear {{$loan->user->name}},</h2>

<strong>KoKash Notification Service</strong>
<br><br>
<p>
	We wish to inform you that your loan {{$loan->refrence}} 
	with total amount of {{_c($loan->principal)}} has been marked as disbursed on {{_d($loan->disbursed_at, false)}}
</p>

<p>Thank you for choosing KoKash.</p>

@endsection