@extends('email')

@section('content')

<h2>Dear {{ucfirst($user->name)}},</h2>

<p>Your account was modified on KoKash!</p>

<p>
	Your account was recently modified on Kokash. Kindly login to your app to view the changes.
</p>

<p>
	If you have not requested for this change. Please reach out to the management by using the contact feature in your app or 
	send an email to support@kokash.ng
</p>

<p style="margin-bottom:50px;">
	You can click <a href="https://play.google.com/store/apps/details?id=com.glcms">here</a> 
	to download the App from the 
	Play Store if you currently do not have.
</p>

@endsection