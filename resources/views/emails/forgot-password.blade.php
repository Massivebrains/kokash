@extends('email')

@section('content')

<h2>Dear {{$user->name}},</h2>

<p>Use the one time link below to reset to your kokash password.</p>

<p><a href="{{$link}}">{{$link}}</a></p>

@endsection