@extends('email')

@section('content')

<h2>Dear {{$user->name}},</h2>

<p>Your password just changed on kokash.ng If you did not initiate this password reset. Please contact us immediately.</p>

@endsection