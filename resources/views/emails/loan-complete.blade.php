@extends('email')

@section('content')

<h2>Dear {{$loan->user->name}},</h2>

<p>You have successfully completed the repayment of <strong>{{$loan->name}}</strong>. Congratulations!</p>

@endsection