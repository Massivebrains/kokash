@extends('email')

@section('content')
    <h2>Dear {{ $loan->user->name }},</h2>

    <strong>KoKash Notification Service</strong>
    <br><br>
    <p>
        We wish to inform you that your {{ $loan->name }} has been uploaded to Kokash. Please find details below
    </p>

    <br>
    <table style="border:0; width: 100%; text-align:left">

        <tr>
            <th>Loan</th>
            <td>{{ $loan->name }}</td>
        </tr>

        <tr>
            <th>Disbursed</th>
            <td>N/A</td>
        </tr>

        <tr>
            <th>Outstanding</th>
            <td>{{ _c($loan->outstanding) }}</td>
        </tr>

        <tr>
            <th>Balance as at Dec 2021</th>
            <td>{{ _c($loan->outstanding) }}</td>
        </tr>

    </table>

    <br><br><br>
    <p>Thank you for choosing GLCMS - Powered by Kokash.</p>
@endsection
