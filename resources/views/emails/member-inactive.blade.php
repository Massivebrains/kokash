@extends('email')

@section('content')

<h2>Dear {{$user->name}},</h2>

<p>
	This is to inform you that your account has been suspended. This suspension implies
	that you would not be able to use any of the services related to this account.
</p>

@endsection