@extends('email')

@section('content')

    <h2>Dear {{ $loan->user->name }},</h2>

    <strong>KoKash Notification Service</strong>
    <br><br>
    <p>
        We wish to inform you that a payment of {{ $amount }} was made on your {{ $loan->name }}. Please find details of your loan status below
    </p>

    <br>
    <table style="border:0; width: 100%; text-align:left">

        <tr>
            <th>Loan</th>
            <td>{{ $loan->name }}</td>
        </tr>

        <tr>
            <th>Disbursed</th>
            <td>{{ _c($loan->disbursed) }}</td>
        </tr>

        <tr>
            <th>Paid so far</th>
            <td>{{ _c($loan->paid) }}</td>
        </tr>

        <tr>
            <th>Outstanding</th>
            <td>{{ _c($loan->outstanding) }}</td>
        </tr>

    </table>
    <br><br>
    <table style="border:0; width: 100%; text-align:left">

        <tr>
            <th>Month</th>
            <th>Paid</th>
            <th>Oustanding</th>
            <th>Status</th>
        </tr>
        @foreach ($loan->loan_transactions as $row)
            <tr>
                <td>{{ _month($row->date) }}</td>
                <td>{{ _c($row->paid) }}</td>
                <td>{{ _c($row->outstanding) }}</td>
                <td>{{ _badge($row->status) }}</td>
            </tr>
        @endforeach
    </table>

    <br><br><br>
    <p>Thank you for choosing GLCMS - Powered by Kokash.</p>

@endsection
