@extends('email')

@section('content')

<h2>Dear {{$user->name}},</h2>

<p>Welcome to <b>KoKash</b>!</p>

<p>An Administrator account has been created for you on KoKash.ng</p>

<p style="margin-top:20px;">
	Use <strong>{{$password}}</strong> as your password. 
	You can change your password anytime.
</p>

@endsection