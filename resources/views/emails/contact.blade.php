@extends('email')

@section('content')

<h2>New Contact Message</h2>

<table style="border:0; width: 100%; text-align:left">

	<tr>
		<th>Visitor's Name</th>
		<td>{{$name ?? 'No Name Provided'}}</td>
	</tr>

	<tr>
		<th>Phone Number</th>
		<td>{{$phone ?? 'No Phone Number Provided'}}</td>
	</tr>

	<tr>
		<th>Email Address</th>
		<td>{{$email ?? 'No Email Address Provided'}}</td>
	</tr>

	<tr>
		<th>Message</th>
		<td>{{$message}}</td>
	</tr>

</table>

@endsection