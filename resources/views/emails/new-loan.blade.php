@extends('email')

@section('content')

<h2>Dear {{optional($loan->user)->name}},</h2>

<strong>KoKash Notification Service</strong>
<br><br>
<p>
	We wish to inform you that a new Loan is currently being processed for you. 
	You will be informed when the funds are disbursed into your bank account.
</p>

<p>The details of the Loan is shown below</p>

<strong style="text-decoration: underline;">Loan Information</strong>

<table style="border:0; width: 100%; text-align:left">

	<tr>
		<th>Amount</th>
		<td>{{_c($loan->principal)}} @ {{_percent($loan->rate)}} for {{_years($loan->time)}}</td>
	</tr>

	<tr>
		<th>Starting Month</th>
		<td>{{_month($loan->start_at)}}</td>
	</tr>

	<tr>
		<th>Total Amount Payable</th>
		<td>{{_c($loan->outstanding)}}</td>
	</tr>

</table>


<p>Thank you for choosing KoKash.</p>

@endsection