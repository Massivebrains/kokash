@extends('email')

@section('content')

<h2>Dear {{$user->name}},</h2>

<p>Welcome to <b>KoKash</b>!</p>

<p>
	An account has been created for you on KoKash - now you can enjoy all services ranging
	from Savings to Loans and much more!. Kindly click on the button below to confirm your account.
</p>

<p style="margin-bottom:50px;">
	You can click <a href="https://play.google.com/store/apps/details?id=com.glcms">here</a> 
	to download the App from the Play Store.
</p>

<a href="{{url('activate/'.$user->api_token)}}" style="padding : 15px; border-radius:3px; margin-top:30px; color : #fff; text-decoration:none; background : #4a90e2; font-family:arial">
	Activate my Account
</a>

<p style="margin-top:20px;">
	Use <strong>{{$password}}</strong> 
	as your password after you verify your email address. 
	You can change your password anytime.
</p>

@endsection