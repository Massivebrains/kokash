@extends('email')

@section('content')

    <h2>Dear {{ $transaction->user->name }},</h2>

    <strong>KoKash Notification Service</strong>
    <br><br>
    <p>
        We wish to inform you that a {{ $transaction->amount > 0 ? 'Credit' : 'Debit' }} transaction occured on your account with us.
    </p>

    <p>The details of this transaction are shown below</p>

    <strong style="text-decoration: underline;">Transaction Notification</strong>

    <table style="border:0; width: 100%; text-align:left">

        <tr>
            <th>Member Name</th>
            <td>{{ $transaction->user->name }}</td>
        </tr>

        <tr>
            <th>Transaction Descriptoin</th>
            <td>[{{ strtoupper($transaction->transaction_type->name) }}] {{ $transaction->description }}</td>
        </tr>

        <tr>
            <th>Transaction Date</th>
            <td>{{ _d($transaction->created_at) }}</td>
        </tr>

        <tr>
            <th>Amount</th>
            <td>{{ _c($transaction->amount) }}</td>
        </tr>

        <tr>
            <th>Balance</th>
            <td>{{ _c($transaction->running_balance) }}</td>
        </tr>

    </table>


    <p>Thank you for choosing KoKash.</p>

@endsection
