<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Complete Pending Payment {{$order->order_reference}}</h3>
        <div class="block-options">

        </div>
    </div>
    <div class="block-content">

        <form action="{{url('complete-order')}}" method="post">

            {{csrf_field()}}

            <div class="row">

                <div class="form-group col-md-6">
                    <label>Payment Source</label>
                    <select class="select2 form-control country" name="payment_source_id" required>
                        <option value="0">--select--</option>
                        @foreach($sources as $row)
                        <option value="{{$row->id}}">{{$row->name}}</option>
                        @endforeach
                    </select>

                    @if($errors->has('payment_source_id'))
                    <div class="invalid-feedback">{{$errors->first('payment_source_id')}}</div>
                    @endif
                </div>

                <div class="form-group col-md-6">
                    <label>Amount Paid</label>
                    <input type="number" class="form-control" name="amount" value="{{old('amount', $order->total)}}" disabled>
                </div>
            </div>

            <div class="row">

                <div class="form-group col-md-12 {{$errors->has('password') ? 'is-invalid' : ''}}">
                    
                    <label>Your Account Password</label>
                    <input type="password" class="form-control" name="password" required>

                    @if($errors->has('password'))
                    <div class="invalid-feedback">{{$errors->first('password')}}</div>
                    @endif
                </div>

            </div>

            <div class="form-group row">
                <div class="col-md-9">
                    <input type="hidden" name="order_id" value="{{$order->id}}">
                    <button type="submit" class="btn btn-alt-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>