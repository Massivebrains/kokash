@extends('app')

@section('content')

<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Pending Payments</h3>
    </div>
    <div class="block-content">

        @component('components.search', ['placeholder' => 'Search Pending payments by reference or amount', 'url' => 'orders', 'count' => $count ?? 0, 'query' => $query ?? null]) @endcomponent

        <table class="table table-striped table-vcenter">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Member</th>
                    <th>Amount</th>
                    <th>Reference</th>
                    <th>Status</th>
                    <th class="text-center" style="width: 100px;">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($orders as $row)
                <tr>
                    <td>{{_d($row->created_at)}}</td>
                    <td><a href="{{url('member-info/'.$row->user->id)}}">{{$row->user->name}}</a></td>
                    <td>{{_c($row->total)}}</td>
                    <td><strong>{{$row->order_reference}}</strong></td>
                    <td>{{_badge($row->status)}}</td>
                    <td class="text-center">
                        <div class="btn-group">
                            <a href="{{url('order/'.$row->id)}}" class="btn btn-sm btn-secondary js-tooltip-enabled">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a href="{{url('order-status/'.$row->id.'/cancelled')}}" class="btn btn-sm btn-danger js-tooltip-enabled" onclick="return confirm('Are you sure?')">
                                <i class="fa fa-trash"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>

        {{$orders->links()}}
    </div>
</div>


@endsection