<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Order #{{$order->order_reference}}</h3>
        <div class="block-options">

            <button type="button" class="btn-block-option" onclick="Codebase.helpers('print-page');">
                <i class="si si-printer"></i> Print Order
            </button>
            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="fullscreen_toggle"></button>
            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                <i class="si si-refresh"></i>
            </button>
        </div>
    </div>
    <div class="block-content">

        <div class="row my-20">

            <div class="col-6">
                <p class="h3">{{optional($order->company)->description}}</p>
                <address>
                    {{optional($order->company)->address}}<br>
                    {{optional($order->company)->contact_person_email}}<br>
                    {{optional($order->company)->contact_person_phone}}
                </address>
            </div>


            <div class="col-6 text-right">
                <p class="h3">{{optional($order->user)->name}}</p>
                <address>
                    {{optional($order->user)->name}}<br>
                    {{optional($order->user)->address}}<br>
                    {{optional($order->user)->email}}<br>
                    {{optional($order->user)->phone}}
                </address>
            </div>

        </div>



        <div class="table-responsive push">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>Order ID</th>
                        <td>{{$order->order_reference}}</td>
                        <th>Order Date</th>
                        <td>{{_d($order->created_at)}}</td>

                    </tr>
                    <tr>
                        <th>Order Total</th>
                        <td>{{_c($order->total)}}</td>
                        <th>Order Status</th>
                        <td>{{_badge($order->status)}}</td>
                    </tr>
                    <tr>
                        <th>Payment Source</th>
                        <td>{{_badge($order->payment_source->name)}}</td>
                        <th>Order Reference</th>
                        <td>{{$order->transaction_reference}}</td>
                    </tr>
                    <tr>
                        <th>Transaction Date</th>
                        <td>{{_d($order->transaction_date)}}</td>
                        <th>Transaction Charge</th>
                        <td>{{_c($order->transaction_charge)}}</td>
                    </tr>

                </tbody>
            </table>
        </div>

        <div class="table-responsive push">

            <h3 class="text-muted">Details</h3>
            <table class="table table-bordered table-striped">
                <thead>

                    <tr>
                        <th>Type</th>
                        <th>Description</th>
                        <th>Member</th>
                        <th>Amount</th>
                        <th>Status</th>                     
                    </tr>
                </thead>
                <tbody>

                    @foreach($order->order_details as $row)
                    <tr>
                        <td>{{_badge($row->type)}}</td>
                        <td>

                            @if($row->type == 'savings')
                            <a href="{{url('transaction/'.$row->transaction_id)}}">Savings [Transaction #{{$row->transaction->reference}}]</a>
                            @endif

                            @if($row->type == 'loan')
                            <a href="{{url('loan/'.$row->loan_id)}}">Repayment of {{$row->loan->name}}</a>
                            @endif

                        </td>
                        <td>
                            <a href="{{url('member-info/'.$row->user_id)}}">{{$row->user->name}}</a>
                            <code>[{{strtoupper($row->for)}}]</code>
                        </td>
                        <td>{{_c($row->amount)}}</td>
                        <td>{{_badge($row->status)}}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <th colspan="4">Total</th>
                        <th>{{_c($order->total)}}</th>
                    </tr>

                </tbody>
            </table>
        </div>

    </div>
</div>