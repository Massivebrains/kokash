@extends('app')

@section('content')

<div class="block">
	<div class="block-header block-header-default">
		<h3 class="block-title">Discarded Statements - {{Auth::user()->company->name}}</h3>
	</div>
	<div class="block-content">

		<div class="table-responsive" style="overflow:scroll;">
			<table class="table table-striped table-bordered table-vcenter">
				<thead>
					<tr>
						<th>Bank</th>
						<th style="width:120px !important">Date</th>
						<th>UploadDate</th>
						<th>Amount</th>
						<th>Reference</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					@foreach($statements as $row)
					<tr>
						<td>{{$row->bank}}/{{$row->account_number}}</td>
						<td>{{_d($row->transaction_date, false)}}</td>
						<td>{{_d($row->created_at, false)}}</td>
						<td>{{_c($row->amount)}}</td>
						<td><small>{{$row->reference}}</small></td>
						<td class="text-center">
							<div class="btn-group">
								<a href="/bank-statement/restore/{{$row->id}}" class="btn btn-sm btn-primary js-tooltip-enabled">
									Restore
								</a>
							</div>
						</td>
					</tr>
					@endforeach

					@if($statements->count() == 0)
					<tr>
						<td colspan="7" class="text-center">
							You currently have no discarded statement records.
						</td>
					</tr>
					@endif
				</tbody>
			</table>
		</div>
		

		{{$statements->links()}}
	</div>
</div>

@endsection