@extends('app')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Download Bank Statement Report</h3>
                <div class="block-options">

                </div>
            </div>
            <div class="block-content">

                <form action="/bank-statement/report" method="POST">

                 @csrf

                <div class="row">

                   <div class="form-group col-md-6 {{$errors->has('business_month') ? 'is-invalid' : ''}}">
                    <label>Business Month</label>
                    <input type="text" class="form-control month" name="business_month" required>
                    @if($errors->has('business_month'))
                    <div class="invalid-feedback">{{$errors->first('business_month')}}</div>
                    @endif
                </div>

            </div>

            @can('update', \App\BankStatement::class)

            <div class="form-group row">
                <div class="col-md-9">
                    <button type="submit" class="btn btn-alt-primary">Submit</button>
                </div>
            </div>

            @endcan

        </form>
    </div>
</div>

</div>
</div>
@endsection