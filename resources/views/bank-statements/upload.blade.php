@extends('app')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Upload new Bank Statement</h3>
                <div class="block-options">

                </div>
            </div>
            <div class="block-content">

                <form action="/bank-statement/upload" method="POST" enctype="multipart/form-data">

                 @csrf

                 @isset($upload_errors)

                 <div class="alert alert-danger">
                    <strong>Some rows in the statement were not uploaded</strong>
                    <ul>
                        @foreach($upload_errors as $row)
                        <li>{{$row}}</li>
                        @endforeach
                    </ul>
                </div>
                @endisset

                <div class="row">

                    <div class="form-group col-md-6 {{$errors->has('bank') ? 'is-invalid' : ''}}">
                        <label>Bank</label>
                        <select name="bank" class="form-control" require>
                            <option value="GTBank">GTBank Statement</option>
                            <option value="StanbicBank">StanbicBank Statement</option>
                        </select>
                        @if($errors->has('bank'))
                        <div class="invalid-feedback">{{$errors->first('bank')}}</div>
                        @endif
                    </div>

                    <div class="form-group col-md-6 {{$errors->has('account_number') ? 'is-invalid' : ''}}">
                        <label>Account Number</label>
                        <input type="number" class="form-control" name="account_number" value="{{old('account_number')}}" required>
                        @if($errors->has('account_number'))
                        <div class="invalid-feedback">{{$errors->first('account_number')}}</div>
                        @endif
                    </div>

                </div>

                <div class="row">

                   <div class="form-group col-md-6 {{$errors->has('business_month') ? 'is-invalid' : ''}}">
                    <label>Business Month</label>
                    <input type="text" class="form-control month" name="business_month" required>
                    @if($errors->has('business_month'))
                    <div class="invalid-feedback">{{$errors->first('business_month')}}</div>
                    @endif
                </div>

                <div class="form-group col-md-6 {{$errors->has('statement') ? 'is-invalid' : ''}}">
                    <label>Statement (.csv)</label>
                    <input type="file" class="form-control" name="statement" value="{{old('statement')}}">
                    @if($errors->has('statement'))
                    <div class="invalid-feedback">{{$errors->first('statement')}}</div>
                    @endif
                </div>

            </div>

            @can('update', \App\BankStatement::class)

            <div class="form-group row">
                <div class="col-md-9">
                    <button type="submit" class="btn btn-alt-primary">Submit</button>
                </div>
            </div>

            @endcan

        </form>
    </div>
</div>

</div>
</div>
@endsection