@extends('app')

@section('content')

<div class="block">
	<div class="block-header block-header-default">
		<h3 class="block-title">Matched Statements - {{Auth::user()->company->name}}</h3>
		<div class="block-options">

			@can('update', \App\BankStatement::class)
			<div class="block-options-item">
				<a href="/bank-statement/report" class="btn btn-alt-primary btn-sm"><i class="si si-graph"></i> Download Report</a>
			</div>
			@endcan

		</div>
	</div>
	<div class="block-content">

		@component('components.search', ['placeholder' => 'Search statements by reference', 'url' => 'bank-statement/matched', 'count' => $count ?? 0, 'query' => $query ?? null]) @endcomponent

		<table class="table table-striped table-vcenter table-responsive">
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th>Bank</th>
					<th style="width:110px;">Date</th>
					<th style="width:110px;">Uploaded</th>
					<th>Amount</th>
					<th>Reference</th>
					<th style="width:200px;">MatchedFor</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($statements as $row)
				<tr>
					<td>{{$row->id}}</td>
					<td>{{$row->bank}}/{{$row->account_number}}</td>
					<td>{{_d($row->transaction_date, false)}}</td>
					<td>{{_d($row->created_at, false)}}</td>
					<td>{{_c($row->amount)}}</td>
					<td><small>{{$row->reference}}</small></td>
					<td><a href="/member-info/{{$row->user_id}}">{{$row->user->name}}</a></td>
					<td class="text-center">
						<div class="btn-group">
							<a href="/bank-statement/breakdown/{{$row->id}}" class="btn btn-sm btn-secondary js-tooltip-enabled">
								<i class="fa fa-eye"></i>
							</a>
						</div>
					</td>
				</tr>
				@endforeach

				@if($statements->count() == 0)
				<tr>
					<td colspan="8" class="text-center">
						When you upload and match statement records, they will show up here.
					</td>
				</tr>
				@endif
			</tbody>
		</table>

		{{$statements->links()}}
	</div>
</div>

@endsection