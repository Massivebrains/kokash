@extends('app')

@section('content')

<div class="block">
	<div class="block-header block-header-default">
		<h3 class="block-title">Matched for {{$statement->user->name}} (From {{$statement->bank}}/{{$statement->account_number}})</h3>
		<div class="block-options">

			@can('update', \App\BankStatement::class)
			<div class="block-options-item">
				<a href="/bank-statement/reverse/{{$statement->id}}" class="btn btn-alt-danger btn-sm" onclick="return confirm('Are you sure?')">
					<i class="si si-arrow-left"></i> Unmatch Statement
				</a>
			</div>
			@endcan

		</div>
	</div>
	<div class="block-content">

		<table class="table table-striped table-vcenter table-responsive">
			<thead>
				<tr>
					<th>Type</th>
					<th>Transaction</th>
					<th>Loan</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody>
				@foreach($statement->breakdowns()->where('amount', '>', 0)->get() as $row)
				<tr>
					<th>{{strtoupper($row->type)}}</th>
					<td>
						@if($row->transaction_id > 0)
						<a href="/transaction/{{$row->transaction_id}}">{{$row->transaction->reference}}</a>
						@else
						N/A
						@endif
					</td>
					<td>
						@if($row->loan_id > 0)
						<a href="/loan/{{$row->loan_id}}">{{$row->loan->name}}</a>
						@else
						N/A
						@endif
					</td>
					<td>{{_c($row->amount)}}</td>
				</tr>
				@endforeach

			</tbody>
		</table>
	</div>
</div>

@endsection