@extends('app')

@section('style')
<link rel="stylesheet" href="https://unpkg.com/vue-select@latest/dist/vue-select.css">
<style>
  .style-chooser .vs__search::placeholder,
  .style-chooser .vs__dropdown-toggle,
  .style-chooser .vs__dropdown-menu {
    background: #dfe5fb;
    border: none;
    color: #394066;
    font-size: 20px;
    text-transform: lowercase;
    font-variant: small-caps;
  }

  .style-chooser .vs__clear,
  .style-chooser .vs__open-indicator {
    fill: #394066;
  }
</style>
@endsection

@section('content')

<div class="block" id="app">
	<div class="block-header block-header-default">
		<h3 class="block-title">Unmatched Statements - {{Auth::user()->company->name}} (Count: <span class="text-strong text-danger">@{{count}}</span>)</h3>
	</div>
	<div class="block-content">

		<table class="table table-vcenter table-responsive table-bordered table-hover">
			<thead>
				<tr>
					<th style="width:15px;">Bank</th>
					<th style="width:110px;">Date</th>
					<th>Amount</th>
					<th>Reference</th>
				</tr>
			</thead>
			<tbody>
				<tr v-for="row in statements" :key="row.id" style="cursor:pointer" v-on:click="getStatement(row.id)" :class="row.possible_user_id < 1 ? 'text-danger' : ''">
					<td>@{{row.bank}}</td>
					<td>@{{row.transaction_date}}</td>
					<td>@{{row.amount_text}}</td>
					<td>@{{row.reference}}</td>
				</tr>
				<tr v-if="statements.length == 0">
					<td colspan="4" class="text-center">There are currently no unmatched statements.</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="modal fade" id="modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<p class="modal-title text-center text-strong">
					@{{statement.reference}}
					<small v-if="statement.notes" class="text-center">
						<br><code>@{{statement.notes}}</code>
					</small>
				</p>
				</div>
				<div class="modal-body">
					<table class="table table-striped table-bordered">
						<tr>
							<td colspan="2">
								<v-select :options="users" label="name" v-model="user_id" style="width:100%" class="style-chooser"></v-select>
							</td>
						</tr>
						<tr>
							<th colspan="2" class="m-0 p-0 text-center">
								<h4 class="m-2 p-0 text-primary">@{{statement.amount_text}}</h4>
							</th>
						</tr>
						<tr>
							<th style="width:300px;">Savings</th>
							<td style="width:200px;">
								<input type="number" class="form-control" v-model="statement.breakdown.save">
							</td>
						</tr>
						<tr>
							<th>Shares</th>
							<td>
								<input type="number" class="form-control" v-model="statement.breakdown.share">
							</td>
						</tr>
						<tr>
							<th>Registration</th>
							<td>
								<input type="number" class="form-control" v-model="statement.breakdown.registration">
							</td>
						</tr>
						<tr v-for="loan in statement.breakdown.loans">
							<th>@{{loan.name}}</th>
							<td>
								<input type="number" class="form-control" v-model="loan.amount">
							</td>
						</tr>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
					<a :href="split_url" class="btn btn-warning">Split</a>
					<button type="button" class="btn btn-primary" v-on:click="match">Match</button>
					<button type="button" class="btn btn-danger" v-on:click="discard(statement.id)">Discard</button>
				</div>
			</div>
		</div>
	</div>

</div>

@endsection

@section('script')
<script src="https://unpkg.com/vue-select@latest"></script>
<script type="text/javascript">

	Vue.component('v-select', VueSelect.VueSelect);

	var app = new Vue({

		el: '#app',

		data: {

			statements: [],
			users: [],
			count: 0,
			statement: {

				possible_user_id: 0,
				reference: '',
				possible_user:{
					first_name: ''
				},
				breakdown: {
					save: 0,
					loans: []
				}
			},
			user_id: {},
			split_url: ''
		},

		async mounted(){

			this.init();
		},

		watch:{

			user_id: function(user){

				if(user.id != this.statement.possible_user_id)
					this.getStatement(this.statement.id, user.id);
			}
		},

		methods: {

			init(){

				fetch('/bank-statement/unmatched/statements').then(resp => resp.json()).then(response => {

					this.statements = response.data.statements;
					this.users = response.data.users;
					this.count = response.data.count;
				});
			},

			getStatement(id = 0, new_possible_user_id = 0){

				fetch(`/bank-statement/unmatched/statement/${id}/${new_possible_user_id}`).then(resp => resp.json()).then(response => {

					this.statement = response.data;

					if(new_possible_user_id < 1)
						$('#modal').modal();

					this.user_id = {id: 0, name: ''};
					this.split_url = `/bank-statement/split/${this.statement.id}`;

					this.users.forEach(row => {

						if(row.id == this.statement.possible_user_id)
							this.user_id = row;
					});
				});
				
			},

			match(){

				$.post('/bank-statement/match', this.statement, response => {

					if(response.status == false)
						alert(`Could not match statement: ${response.message}`);

					this.init();
					$('#modal').modal('hide');
				});

			},

			discard(id = 0){

				fetch(`/bank-statement/unmatched/discard/${id}`).then(resp => resp.json()).then(response => {

					this.init();
					$('#modal').modal('hide');
				});
				
			}
		}
	})
</script>
@endsection