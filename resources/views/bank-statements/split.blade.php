@extends('app')

@section('content')

<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Split - {{$statement->reference}}/{{$statement->bank}}</h3>
    </div>
    <div class="block-content p-5">

        <div class="jumbotron text-center" style="padding:20px;">
            <h1 class="text-center">{{_c($statement->amount)}}</h1>
            <p>
                {{$statement->reference}}
                @if($statement->notes)
                <br><code>{{$statement->notes}}</code>
                @endif
            </p>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <form action="/bank-statement/split" method="POST">

                        @csrf

                        <div class="form">
                            <div class="form-group">
                                <label>Select Member</label>
                                <select class="select2 form-control" name="user_id" required>
                                    <option>--select--</option>
                                    @foreach($users as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="from-group">
                                <label>Split Amount</label>
                                <input type="number" name="amount" step="0.001" class="form-control" placeholder="Split Amount" required>
                            </div>
                            <div class="form-group mt-3">
                                <input type="hidden" name="id" value="{{$statement->id}}">
                                <button type="submit" class="btn btn-primary btn-block">Split</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection