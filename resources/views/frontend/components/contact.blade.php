<div class="bg-gd-primary" id="contact">
    <div class="bg-pattern bg-black-op-25" style="background-image: url('assets/img/various/bg-fe-pattern.png');">
        <div class="content content-top text-center">
            <div class="py-50">
                <h1 class="font-w700 text-white mb-10">Get In Touch</h1>
                <h2 class="h4 font-w400 text-white-op">Do you have any questions or need our help?</h2>
            </div>
        </div>
    </div>
</div>



<div class="bg-white">
    <div class="content content-full">
        <div class="row justify-content-center py-50">
            <div class="col-lg-6">

                @if(session('contact-message'))
                <div class="alert alert-success">{{session('contact-message')}}</div>
                @endif

                <form action="{{url('contact')}}" method="post">

                    {{csrf_field()}}

                    <div class="form-group row">
                        <div class="col-12">
                            <label for="fe-contact-name">Name</label>
                            <input type="text" name="name" class="form-control form-control-lg" placeholder="Enter your name.." required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-12" for="fe-contact-email">Email</label>
                        <div class="col-12">
                            <input type="email" class="form-control form-control-lg" name="email" placeholder="Enter your email.." required>
                        </div>
                    </div>

                     <div class="form-group row">
                        <label class="col-12" for="fe-contact-email">Phone</label>
                        <div class="col-12">
                            <input type="number" class="form-control form-control-lg" name="phone" placeholder="Enter your phone number.." required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-12" for="fe-contact-message">Message</label>
                        <div class="col-12">
                            <textarea class="form-control form-control-lg" name="message" rows="10" placeholder="Enter your message.." required></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-hero btn-rounded btn-alt-primary min-width-175">
                                <i class="fa fa-send mr-5"></i> Send Message
                            </button>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
</div>