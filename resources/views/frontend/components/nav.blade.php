<nav id="sidebar">

    <div id="sidebar-scroll">

        <div class="sidebar-content">

            <div class="content-header content-header-fullrow bg-black-op-10">
                <div class="content-header-section text-center align-parent">


                    <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                        <i class="fa fa-times text-danger"></i>
                    </button>

                    <div class="content-header-item">
                        <a class="link-effect font-w700" href="{{url('/')}}">
                            <img src="{{asset('img/kokash-logo-text-white.png')}}">
                        </a>
                    </div>

                </div>
            </div>


            <div class="content-side content-side-full">

                <ul class="nav-main">
                    <li>
                        <a class="active" href="{{url('/')}}">Home</a>
                    </li>
                    <li>
                        <a  href="{{url('/#features')}}">Features</a>
                    </li>
                    <li>
                        <a  href="{{url('/#contact')}}">Contact</a>
                    </li>
                    <li>
                        <a  href="{{url('/login')}}">Login</a>
                    </li>
                    <li>
                        <a  href="{{url('/#contact')}}">Start Now</a>
                    </li>
                </ul>
            </div>

        </div>

    </div>

</nav>



<header id="page-header">

    <div class="content-header">

        <div class="content-header-section">

            <div class="content-header-item">
                <a class="link-effect font-w700 mr-5" href="{{url('/')}}">
                    <img src="{{asset('img/kokash-logo-text-white.png')}}" style="width:40%;">
                </a>
            </div>

        </div>


        <div class="content-header-section">

            <ul class="nav-main-header">
                <li>
                    <a class="active" href="{{url('/')}}">Home</a>
                </li>
                <li>
                    <a href="{{url('/#features')}}">Features</a>
                </li>
                <li>
                    <a href="{{url('/#contact')}}">Contact</a>
                </li>
                <li>
                    <a href="{{url('login')}}">Login</a>
                </li>
                <li>
                    <a href="{{url('/#contact')}}">Start Now</a>
                </li>

            </ul>

            <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none" data-toggle="layout" data-action="sidebar_toggle">
                <i class="fa fa-navicon"></i>
            </button>

        </div>

    </div>



    <div id="page-header-search" class="overlay-header">
        <div class="content-header content-header-fullrow">
            <form action="fe_search.html" method="post">
                <div class="input-group">
                    <span class="input-group-btn">

                        <button type="button" class="btn btn-secondary px-15" data-toggle="layout" data-action="header_search_off">
                            <i class="fa fa-times"></i>
                        </button>

                    </span>
                    <input type="text" class="form-control" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-secondary px-15">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>

    <div id="page-header-loader" class="overlay-header bg-primary">
        <div class="content-header content-header-fullrow text-center">
            <div class="content-header-item">
                <i class="fa fa-sun-o fa-spin text-white"></i>
            </div>
        </div>
    </div>

</header>