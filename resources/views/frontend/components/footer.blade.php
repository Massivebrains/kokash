   <footer id="page-footer" class="bg-white opacity-0">
    <div class="content content-full">

        <div class="row items-push-2x mt-30">
            <div class="col-6 col-md-4">
                <h3 class="h5 font-w700">Useful Links</h3>
                <ul class="list list-simple-mini font-size-sm">
                    <li>
                        <a class="link-effect font-w600" href="{{url('/')}}">Home</a>
                    </li>
                    <li>
                        <a class="link-effect font-w600" href="#">Features</a>
                    </li>
                    <li>
                        <a class="link-effect font-w600" href="#">About</a>
                    </li>
                    <li>
                        <a class="link-effect font-w600" href="{{url('/#contact')}}">Contact</a>
                    </li>
                    <li>
                        <a class="link-effect font-w600" href="#">Team</a>
                    </li>
                    <li>
                        <a class="link-effect font-w600" href="#">About</a>
                    </li>
                </ul>
            </div>
            <div class="col-6 col-md-4">
                <h3 class="h5 font-w700">Explore</h3>
                <ul class="list list-simple-mini font-size-sm">
                    <li>
                        <a class="link-effect font-w600" href="{{url('dashboard')}}">Dashboard</a>
                    </li>
                    <li>
                        <a class="link-effect font-w600" href="{{url('login')}}">Sign In</a>
                    </li>
                    <li>
                        <a class="link-effect font-w600" href="{{url('/#contact')}}">Start Now</a>
                    </li>
                    <li>
                        <a class="link-effect font-w600" href="{{url('/')}}">Get the App</a>
                    </li>
                    <li>
                        <a class="link-effect font-w600" href="javascript:void(0)">Resolve Transaction Issues</a>
                    </li>
                    <li>
                        <a class="link-effect font-w600" href="javascript:void(0)">Partner</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <h3 class="h5 font-w700">kokash.ng</h3>
                <div class="font-size-sm mb-30">
                    14, Farayibi Street, Bariga<br>
                    Lagos, Nigeria<br>
                    hello@kokash.ng
                    <abbr title="Phone">P:</abbr> +234 81 7502 0329
                </div>
                <h3 class="h5 font-w700">Newsletter</h3>
                <form action="#" method="post" onsubmit="return false;">
                    <div class="input-group">
                        <input type="email" class="form-control" id="fe-subscribe-email" name="fe-subscribe-email" placeholder="Your email..">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-secondary">Subscribe</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>



        <div class="font-size-xs clearfix border-t pt-20 pb-10">
            <div class="float-right">
                Crafted with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="mailto:vadeshayo@gmail.com" target="_blank">Olaiya Segun</a>
            </div>
            <div class="float-left">
                <a class="font-w600" href="#" target="_blank">Kokash 0.1.0</a> &copy; <span class="js-year-copy">{{date('Y')}}</span>
            </div>
        </div>

    </div>
</footer>