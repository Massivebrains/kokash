<!doctype html>


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>kokash.ng</title>
    <meta name="description" content="A cooperative banking platform.">
    <link rel="shortcut icon" href="/img/kokash.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/img/kokash.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/kokash.png">
    <link rel="stylesheet" href="/js/plugins/slick/slick.min.css"><link rel="stylesheet" href="/js/plugins/slick/slick-theme.min.css">
    <link rel="stylesheet" id="css-main" href="/css/codebase.min.css">
    <style type="text/css">

    button{

        cursor: pointer;
    }
</style>
</head>
<body>
    <div id="page-container" class="sidebar-inverse side-scroll page-header-fixed page-header-glass page-header-inverse main-content-boxed">


        @include('frontend.components.nav')

        <main id="main-container">

            <div class="bg-gd-primary overflow-hidden">
                <div class="hero bg-pattern bg-black-op-25" style="background-image: url('/img/various/bg-fe-pattern.png');">
                    <div class="hero-inner">
                        <div class="content content-full text-center">
                            <h1 class="display-3 font-w700 text-white mb-10 invisible" data-toggle="appear" data-class="animated fadeInDown">Cooperative Banking Platform</h1>
                            <h2 class="font-w400 text-white-op mb-50 invisible" data-toggle="appear" data-class="animated fadeInDown">Automate the money stuff.</h2>
                            <a class="btn btn-hero btn-noborder btn-rounded btn-success mr-5 mb-10 invisible" data-toggle="appear" data-class="animated fadeInUp" href="{{url('/#contact')}}">
                                <i class="fa fa-hourglass-start mr-10"></i> Start Now
                            </a>
                            <a class="btn btn-hero btn-noborder btn-rounded btn-primary mb-10 invisible" data-toggle="appear" data-class="animated fadeInUp" href="{{url('login')}}">
                                <i class="fa fa-sign-in mr-10"></i> Login
                            </a>
                        </div>
                    </div>
                </div>
            </div>



            <div class="bg-white" id="features">
                <div class="content content-full">
                    <div class="row nice-copy py-30">
                        <div class="col-sm-6 col-md-4 py-30">
                            <div class="item item-circle item-2x mx-auto bg-gd-cherry text-white mb-30">SV</div>
                            <h4 class="font-w400 text-black text-center mb-10">Savings Automation</h4>
                            <p class="mb-0 text-center">Let your members save conviently without stress directly from their mobile phones.</p>
                        </div>
                        <div class="col-sm-6 col-md-4 py-30">
                            <div class="item item-circle item-2x mx-auto bg-gd-emerald text-white mb-30">LN</div>
                            <h4 class="font-w400 text-black text-center mb-10">Loans Managmenet</h4>
                            <p class="mb-0 text-center">Disburse loans, manage loans repayment automatically</p>
                        </div>
                        <div class="col-sm-6 col-md-4 py-30">
                            <div class="item item-circle item-2x mx-auto bg-gd-dusk text-white mb-30">RN</div>
                            <h4 class="font-w400 text-black text-center mb-10">Reminders & Notifications</h4>
                            <p class="mb-0 text-center">Let your members enjoy SMS and Email notifications on Transactions, Reminders and more</p>
                        </div>
                        <div class="col-sm-6 col-md-4 py-30">
                            <div class="item item-circle item-2x mx-auto bg-gd-aqua text-white mb-30">
                                FP
                            </div>
                            <h4 class="font-w400 text-black text-center mb-10">Flexible Payments</h4>
                            <p class="mb-0 text-center">Stand on the Sholder of Giants by leveraging on 21st century payment systems to easy your cash flow.</p>
                        </div>
                        <div class="col-sm-6 col-md-4 py-30">
                            <div class="item item-circle item-2x mx-auto bg-gd-sea text-white mb-30">
                                RP
                            </div>
                            <h4 class="font-w400 text-black text-center mb-10">Reports</h4>
                            <p class="mb-0 text-center">Gain deep insights into your operations. Understand your members, transactions, and business health on a click of a button</p>
                        </div>
                        <div class="col-sm-6 col-md-4 py-30">
                            <div class="item item-circle item-2x mx-auto text-white bg-gd-lake mb-30">
                                ST
                            </div>
                            <h4 class="font-w400 text-black text-center mb-10">Statements</h4>
                            <p class="mb-0 text-center">Let your members request for their statements anytime anywhere on any device. Fully automated!</p>
                        </div>
                    </div>
                </div>
            </div>



            <div class="bg-body-light">
                <div class="content content-full text-center">
                    <div class="py-30 invisible" data-toggle="appear">


                        <div class="js-slider slick-nav-white" data-arrows="true" data-dots="true">
                            <div>
                                <h3 class="font-w400 text-black mb-10">Simple Administration</h3>
                                <h4 class="h5 font-w400 text-muted mb-30">Modern and simple user experience designed for with simplicity and maximum productivity at heart</h4>
                                <img class="img-fluid mx-auto" src="/img/various/fe-cb-promo1.png" alt="Promo">
                            </div>
                            <div>
                                <h3 class="font-w400 text-black mb-10">Members management</h3>
                                <h4 class="h5 font-w400 text-muted mb-30">Easy to work with and highly flexible options.</h4>
                                <img class="img-fluid mx-auto" src="/img/various/fe-cb-promo2.png" alt="Promo">
                            </div>
                            <div>
                                <h3 class="font-w400 text-black mb-10">Loans and Transactions</h3>
                                <h4 class="h5 font-w400 text-muted mb-30">Handcrafted to fit your use case.</h4>
                                <img class="img-fluid mx-auto" src="/img/various/fe-cb-promo3.png" alt="Promo">
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            @include('frontend.components.contact')

        </main>


        @include('frontend.components.footer')

    </div>



    <script src="/js/core/jquery.min.js"></script>
    <script src="/js/core/popper.min.js"></script>
    <script src="/js/core/bootstrap.min.js"></script>
    <script src="/js/core/jquery.slimscroll.min.js"></script>
    <script src="/js/core/jquery.scrollLock.min.js"></script>
    <script src="/js/core/jquery.appear.min.js"></script>
    <script src="/js/core/jquery.countTo.min.js"></script>
    <script src="/js/core/js.cookie.min.js"></script>
    <script src="/js/codebase.js"></script>


    <script src="/js/plugins/slick/slick.min.js"></script>


    <script>
        jQuery(function () {
                // Init page helpers (Slick Slider plugin)
                Codebase.helpers('slick');
            });
        </script>
    </body>
    </html>