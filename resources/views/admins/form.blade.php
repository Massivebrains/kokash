@extends('app')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">{{$user->id == 0 ? 'Setup a New Administrator' : $user->name}}</h3>
                <div class="block-options">

                </div>
            </div>
            <div class="block-content">

                <form action="{{url('save-admin/'.$user->id)}}" method="post">

                    {{csrf_field()}}


                    <div class="row">

                        <div class="form-group col-md-6 {{$errors->has('first_name') ? 'is-invalid' : ''}}">
                            <div class="form-material">
                                <input type="text" class="form-control" name="first_name" value="{{old('first_name', $user->first_name)}}" required>
                                <label>First Name</label>
                            </div>
                            @if($errors->has('first_name'))
                            <div class="invalid-feedback">{{$errors->first('first_name')}}</div>
                            @endif
                        </div>

                        <div class="form-group col-md-6 {{$errors->has('last_name') ? 'is-invalid' : ''}}">
                            <div class="form-material">
                                <input type="text" class="form-control" name="last_name" value="{{old('last_name', $user->last_name)}}" required>
                                <label>Last Name</label>
                            </div>
                            @if($errors->has('last_name'))
                            <div class="invalid-feedback">{{$errors->first('last_name')}}</div>
                            @endif
                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-md-6 {{$errors->has('email') ? 'is-invalid' : ''}}">
                            <div class="form-material">
                                <input type="email" class="form-control" name="email" value="{{old('email', $user->email)}}" required>
                                <label>Email Address</label>
                            </div>
                            @if($errors->has('email'))
                            <div class="invalid-feedback">{{$errors->first('email')}}</div>
                            @endif
                        </div>

                        <div class="form-group col-md-6 {{$errors->has('phone') ? 'is-invalid' : ''}}">
                            <div class="form-material">
                                <input type="number" class="form-control" name="phone" value="{{old('phone', $user->phone)}}" required>
                                <label>Phone Number</label>
                            </div>
                            @if($errors->has('phone'))
                            <div class="invalid-feedback">{{$errors->first('phone')}}</div>
                            @endif
                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-md-6">
                            <div class="form-material">
                                <select class="select2 form-control" name="status" required>
                                    <option value="active" {{$user->status == 'active' ? 'selected' : ''}}>Active</option>
                                    <option value="inactive" {{$user->status == 'inactive' ? 'selected' : ''}}>Inactive</option>
                                </select>
                                <label>Status</label>
                            </div>
                        </div>


                        <div class="form-group col-md-6">
                            <div class="form-material">
                                <select class="select2 form-control" name="access_level" required>

                                    @foreach(_access_levels() as $row)
                                        @if($row['id'] == old('access_level', $user->access_level))
                                            <option value="{{$row['id']}}" selected>{{$row['name']}}</option>
                                        @else
                                            <option value="{{$row['id']}}">{{$row['name']}}</option>
                                        @endif
                                    @endforeach
                                    
                                </select>
                                <label>Access Level</label>
                            </div>
                        </div>
                    </div>

                    @can('update', \App\User::class)

                    <div class="form-group row">
                        <div class="col-md-9">
                            <input type="hidden" name="id" value="{{$user->id}}">
                            <button type="submit" class="btn btn-alt-primary">Submit</button>
                        </div>
                    </div>

                    @endcan

                </form>
            </div>
        </div>

    </div>
</div>
@endsection