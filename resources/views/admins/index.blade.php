@extends('app')

@section('content')

<div class="block">
	<div class="block-header block-header-default">
		<h3 class="block-title">Administrators</h3>
		<div class="block-options">

			@can('update', \App\User::class)
			<div class="block-options-item">
				<a href="{{url('admin/0')}}" class="btn btn-alt-primary btn-sm"><i class="si si-plus"></i> New Administrator</a>
			</div>
			@endcan

		</div>
	</div>
	<div class="block-content">

		<table class="table table-striped table-vcenter">
			<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Access Level</th>
					<th>Date Created</th>
					<th>Last Login</th>
					<th>Status</th>
					<th class="text-center" style="width: 100px;">Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $row)
				<tr>
					<td>{{$row->name}}</td>
					<td>{{$row->email}}</td>
					<td>{{$row->phone}}</td>
					<td>{{_access_levels($row->access_level)}}</td>
					<td>{{_d($row->created_at)}}</td>
					<td>{{_d($row->last_login_at)}}</td>
					<td>{{_badge($row->status)}}</td>
					<td class="text-center">
						<div class="btn-group">
							<a href="{{url('admin/'.$row->id)}}" class="btn btn-sm btn-secondary js-tooltip-enabled">
								<i class="fa fa-pencil"></i>
							</a>
						</div>
					</td>
				</tr>
				@endforeach

			</tbody>
		</table>

		{{$users->links()}}
	</div>
</div>

@endsection