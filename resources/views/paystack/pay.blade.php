<html>
<head>
  <script src="https://js.paystack.co/v1/inline.js"></script>
  <link rel="stylesheet" type="text/css" href="{{asset('css/pay.css')}}">
  <link href="https://fonts.googleapis.com/css?family=K2D" rel="stylesheet">
    <style type="text/css">

    body, button, h1, h2, h3, h4, h5, p, span{
        
        font-family: 'K2D', sans-serif;
    }
  </style>
</head>
<body style="background-image:url({{asset('img/background.png')}})">

  @php

  $public_key = env('PAYSTACK_STATUS') == 'test' ? env('PAYSTAC_TEST_PUBLIC') : env('PAYSTAC_LIVE_PUBLIC');

  @endphp

    <img src="{{asset('img/svg/wallet.svg')}}" id="loading" alt="Loading...">
    <p id="processing" style="font-family: 'K2D', sans-serif;">Please wait while your transaction is processing...</p>

  <script>

    window.onload = function(){

      let key         = '{{$public_key}}'
      let email       = '{{$order->user->email}}'
      let amount      = {{$order->kobo}}
      let orderid     = {{$order->id}}
      let subaccount  = '{{$order->sub_account}}'

      var handler = PaystackPop.setup({

        key           : key,
        email         : email,
        amount        : amount,
        subaccount    : subaccount,
        metadata      : { orderid : orderid },

        callback: response => {

          document.getElementById('loading').style.display = 'block';
          document.getElementById('processing').style.display = 'block';

          let reference = response.reference;

          let requery_url = `{{url('paystack/requery/'.$order->id)}}/${reference}`;
          let order_url   = `{{url('api/process-order/'.$order->id)}}`;

          fetch(requery_url).then(resp => resp.json()).then(response => {

            fetch(order_url).then(resp => resp.json()).then(response => {

              window.postMessage(JSON.stringify(response), '*');

            });
            
          })

        },

        onClose: function(error){

          document.getElementById('loading').style.display = 'block';
          document.getElementById('processing').style.display = 'block';
          window.postMessage(JSON.stringify({status : false, data : 'Payment terminated.'}), '*');
        }

      });

      handler.openIframe();
    }
  </script>
</body>
</html>