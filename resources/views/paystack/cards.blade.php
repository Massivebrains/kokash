<!DOCTYPE html>
<html>
<head>
    <title>Payment Options</title>
    <link rel="stylesheet" id="css-main" href="{{asset('/css/codebase.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/pay.css')}}">
    <link href="https://fonts.googleapis.com/css?family=K2D" rel="stylesheet">
    <style type="text/css">

    *, body, button, h1, h2, h3, h4, h5, p, span{

        font-family: 'K2D', sans-serif;
    }
    .btn{

        margin-bottom:20px;
        cursor: pointer;
        background : #1c3d5a;
        color : #fff;
        font-weight: 'normal';
        border-color : #1c3d5a;
        border-radius : 0 !important;
        height : 45px;
    }
</style>
</head>

<body style="background-image:url({{asset('/img/background.png')}})">

    <img src="{{asset('/img/svg/wallet.svg')}}" id="loading" alt="Loading...">
    <p id="processing">Please wait while your transaction is processing...</p>

    <div class="container" style="margin-top: 30px;">
        <div class="row">
            <div class="col-12">

                @foreach($cards as $row)

                @php
                $icon = $row->brand == 'visa' ? 'visa' : 'mastercard';
                @endphp

                <button type="button" class="btn btn-alt-primary btn-block btn-lg" onclick="charge({{$row->id}})" id="{{$row->id}}">
                    <i class="fa fa-cc-{{$icon}} mr-5"></i> <span>Pay with ...{{$row->last4}}</span>
                    <span style="font-size:10px;">({{$row->bank}} {{$row->exp_month}}/{{$row->exp_year}})</span>
                </button>
                
                @endforeach

                <a href="{{url('paystack/pay/'.$order->id)}}" class="btn btn-alt-primary btn-block btn-lg card">
                    <i class="fa fa-credit-card mr-5"></i> <span>Use a different Card</span><br>
                </a>
            </div>
        </div>
    </div>

</body>

<script src="{{asset('')}}/js/core/jquery.min.js"></script>

<script type="text/javascript">

    let charge = (debit_card_id) => {

        try{

            $('#loading').show();
            $('#processing').show();
            $('.container').hide();

            $(`#${debit_card_id}`).prop('disabled', true);

            let charge_url = `{{url('paystack/charge')}}/{{$order->id}}/${debit_card_id}`;
            let order_url   = `{{url('api/process-order/'.$order->id)}}`;

            fetch(charge_url).then(resp => resp.json()).then(resp => {

                if(resp.status == true){

                    fetch(order_url).then(resp => resp.json()).then(response => {

                      window.postMessage(JSON.stringify(response), '*');

                  }); 

                }else{

                   window.location = '{{url('paystack/pay/'.$order->id)}}';

                }
                
            })

        }catch(ex){

            window.location = '{{url('paystack/pay/'.$order->id)}}';
        }
    }

    $('.card').click(function(){

        $('.card').html('<i class="fa fa-spinner fa-spin"></i> Loading...');
    })
</script>
</html>