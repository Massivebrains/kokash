<html>
<head>
  <link href="https://fonts.googleapis.com/css?family=K2D" rel="stylesheet">
  <style type="text/css">

    body, button, h1, h2, h3, h4, h5, p, span{

      font-family: 'K2D', sans-serif;
    }

    body{

      text-align : 'center';
    }

    h1{

      font-size : 50px;
      color : #1c3d5a;
    }

    p{

      font-size : 20px;
    }
    h1, p{

      margin : 0px auto;
      text-align: center;
    }

    .btn{
      display: inline-block;
      text-align: center;
      white-space: nowrap;
      vertical-align: middle;
      user-select: none;
      border: 1px solid transparent;
      padding: 8px 14px;
      font-size: 1rem;
      line-height: 16px;
      border-radius: .25rem;
      transition: all .15s ease-in-out;
      color: #fff;
      background-color: #1c3d5a;
      text-decoration : none;
      margin-top : 30px;
      width: 100%;
      align-self: center;
    }

    .btn:hover{

      color: #fff;
      background-color: #2a567c;
      border-color: #2a567c;
    }
  </style>
</head>
<body>

  @if($order->status == 'pending')
  <h1>{{$order->order_reference}}</h1>
  <p>Your Transaction request with total amount <strong>{{_c($order->total)}}</strong> has been logged. Kindly write down the Reference Number above and use it as Description when you want to Transfer or Use the ATM. After your payment, Your Savings/Loan will be updated accordingly.</p>
  @else
  <p>This Payment Request is no more valid. Please create a new transaction.</p>
  @endif

  <div style="width: 200px; margin: 0px auto">
    <a href="javascript:;" onclick="window.postMessage(true, '*')" class="btn">Close</a>
  </div>
</body>
</html>