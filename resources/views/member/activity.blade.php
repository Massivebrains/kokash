<div class="tab-pane" id="activities" role="tabpanel">

    <h4 class="font-w400">Notfications Messages</h4>

    <table class="table table-striped table-sm table-vcenter">
        <tbody>
            @foreach ($notifications as $row)
                <tr>
                    <td>{{ $row->title }}</td>
                    <td>
                        <small>{{ _d($row->updated_at) }}</small><br>
                        {{ $row->title == 'SMS' ? $row->notification : 'Email Body' }}
                    </td>
                    <td>{{ _badge($row->status) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <h4 class="font-w400">Activities</h4>

    <table class="table table-striped table-sm table-vcenter">
        <tbody>
            @foreach ($activities as $row)
                <tr>
                    <td>
                        {{ $row->description }}&nbsp;
                        <span class="pull-right badge badge-info">{{ _d($row->created_at) }}</span>
                    </td>
                </tr>
            @endforeach

        </tbody>
    </table>

    {{ $activities->links() }}


</div>
