<div class="tab-pane" id="settings" role="tabpanel">
    <h4 class="font-w400">Account Settings</h4>
    <div class="row">
        <div class="col-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width:20%;">Settings Key</th>
                        <th style="width: 20%">Setting Value</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Member Status</th>
                        <td>{{_badge($user->status)}}</td>
                        <td>
                            @if($user->status == 'inactive')
                            
                            <a href="{{url('member-status/'.$user->id)}}" class="btn btn-alt-primary btn-sm confirm">Activate Member</a>
                            
                            @else

                            <a href="{{url('member-status/'.$user->id)}}" class="btn btn-alt-danger btn-sm confirm">Deactivate Member</a>
                            
                            @endif
                        </td>
                    </tr>

                    <tr>
                        <th>Monthly Savings</th>
                        <td>{{_c($user->savings_account->monthly_savings)}}</td>
                        <td>
                            <form class="form-inline" action="{{url('save-monthly-savings')}}" method="post">
                                {{csrf_field()}}
                                <label class="sr-only">Montly Savings</label>
                                <input type="number" class="form-control mb-2 mr-sm-2 mb-sm-0" placeholder="Monthly Savings" value="{{old('monthly_savings', $user->savings_account->monthly_savings)}}" name="monthly_savings" required>
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                <button type="submit" class="btn btn-alt-primary">Update</button>
                            </form>
                        </td>
                    </tr>

                    <tr>
                        <th>Change Password</th>
                        <td colspan="2">
                            <form class="form-inline" action="{{url('change-member-password')}}" method="post">
                                {{csrf_field()}}
                                <input type="password" class="form-control mb-2 mr-sm-2 mb-sm-0" placeholder="New Password" name="password" required>
                                <input type="password" class="form-control mb-2 mr-sm-2 mb-sm-0" placeholder="Retype New Password" name="password_confirmation" required>
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                <button type="submit" class="btn btn-alt-primary">Reset Password</button>
                            </form>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
    <hr>
</div>