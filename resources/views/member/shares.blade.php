<div class="tab-pane" id="shares" role="tabpanel">
  
  <h4 class="font-w400">Total Share Units :: {{number_format($user->share->units ?? 0)}}</h4>

  <table class="table table-striped table-sm table-vcenter">
    <tbody>
        @foreach($share_activities as $row)
        <tr>
            <td>
                <small>{{_d($row->updated_at)}}</small><br>
                {{$row->description}}
            </td>
        </tr>
        @endforeach

    </tbody>
</table>

</div>