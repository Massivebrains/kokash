<div class="tab-pane" id="beneficiaries" role="tabpanel">
    <form action="{{url('save-beneficiary')}}" method="POST" role="form">

        {{csrf_field()}}

        <legend>Add a Beneficiary</legend>

        <div class="form-group {{$errors->has('partner_id') ? 'is-invalid' : ''}}">
            <select name="partner_id" class="form-control select2" style="width:100%" required>
                @foreach($users as $row)
                <option value="{{$row->id}}">{{$row->name}}</option>
                @endforeach
            </select>
            <input type="hidden" name="user_id" value="{{$user->id}}">
            @if($errors->has('partner_id'))
            <div class="invalid-feedback">{{$errors->first('partner_id')}}</div>
            @endif
        </div>
        
        <button type="submit" class="btn btn-alt-primary">Submit</button>
    </form>

    <hr>

    <h4 class="font-w400">Beneficiaries</h4>

    <table class="table table-striped table-vcenter">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Date Added</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($user->beneficiaries as $row)
            <tr>
                <td><a href="{{url('member-info/'.$row->partner->id)}}">{{$row->partner->name}}</a></td>
                <td>{{$row->partner->email}}</td>
                <td>{{$row->partner->phone}}</td>
                <td>{{_d($row->created_at, false)}}</td>
                <td>
                    <a href="{{url('remove-beneficiary/'.$row->id)}}" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure?')">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>
</div>