<div class="tab-pane" id="pending-payments" role="tabpanel">
    <h4 class="font-w400">Pending Payments</h4>

    <table class="table table-striped table-vcenter">
        <thead>
            <tr>
                <th>Date</th>
                <th>Reference</th>
                <th>Total Amount</th>
                <th>Status</th>
                <th class="text-center" style="width: 100px;">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($orders as $row)
            <tr>
                <td>{{_d($row->created_at)}}</td>
                <td>{{$row->order_reference}}</td>
                <td>{{_c($row->total)}}</td>
                <td>{{_badge($row->status)}}</td>
                <td class="text-center">
                    <div class="btn-group">
                        <a href="{{url('order/'.$row->id)}}" class="btn btn-sm btn-secondary js-tooltip-enabled">
                            <i class="fa fa-eye"></i>
                        </a>
                    </div>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>

    {{$orders->links()}}

</div>