<div class="tab-pane active" id="transactions" role="tabpanel">
    <h4 class="font-w400">Recent Transactions</h4>

    <table class="table table-striped table-vcenter">
        <thead>
            <tr>
                <th>Date</th>
                <th>Amount</th>
                <th>Type</th>
                <th>Source</th>
                <th>Member</th>
                <th>Balance</th>
                <th>Reference</th>
                <th class="text-center" style="width: 100px;">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($transactions as $row)
            <tr>
                <td>{{_d($row->created_at)}}</td>
                <td>{{_c($row->amount)}}</td>
                <td>{{_badge($row->transaction_type->name)}}</td>
                <td>{{_badge($row->payment_source->name)}}</td>
                <td>{{$row->user->name}}</td>
                <td>{{_c($row->running_balance)}}</td>
                <td>{{$row->reference}}</td>
                <td class="text-center">
                    <div class="btn-group">
                        <a href="{{url('transaction/'.$row->id)}}" class="btn btn-sm btn-secondary js-tooltip-enabled">
                            <i class="fa fa-eye"></i>
                        </a>
                    </div>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>

    {{$transactions->links()}}

</div>