@extends('app')

@section('content')

    <div class="row gutters-tiny">

        @can('view', \App\Transaction::class)
            <div class="col-md-6 col-xl-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10">
                            <i class="si si-wallet fa-3x text-body-bg-dark"></i>
                        </div>
                        <div class="font-size-h5 font-w600 text-success">{{ _c($user->savings_account->balance) }}</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Savings Balance</div>
                    </div>
                </a>
            </div>
        @endcan

        @can('view', \App\Loan::class)
            <div class="col-md-6 col-xl-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10">
                            <i class="si si-bar-chart fa-3x text-body-bg-dark"></i>
                        </div>
                        <div class="font-size-h5 font-w600 text-danger">{{ _c(ceil($outstanding_loan)) }}</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Loans Outstanding</div>
                    </div>
                </a>
            </div>
        @endcan

        <div class="col-md-6 col-xl-3">
            <a class="block block-link-shadow text-left" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-right mt-10">
                        <i class="si si-clock fa-3x text-body-bg-dark"></i>
                    </div>
                    <div class="font-size-h5 font-w600">{{ $user->last_login_at ? _d($user->last_login_at, false) : 'Never Logged In.' }}</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Last Login</div>
                </div>
            </a>
        </div>
        <div class="col-md-6 col-xl-3">
            <a class="block block-link-shadow text-left" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-right mt-10">
                        <i class="si si-info fa-3x text-body-bg-dark"></i>
                    </div>
                    <div class="font-size-h5 font-w600">{{ strtoupper($user->status) }}</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Status</div>
                </div>
            </a>
        </div>

    </div>

    <div class="row gutters-tiny mt-20">

        <div class="col-lg-12">

            <div class="block">
                <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">

                    @can('view', \App\Transaction::class)
                        <li class="nav-item">
                            <a class="nav-link active" href="#transactions">Recent Transactions</a>
                        </li>
                    @endcan

                    @can('view', \App\Loan::class)
                        <li class="nav-item">
                            <a class="nav-link" href="#loans">Loans</a>
                        </li>
                    @endcan

                    @can('view', \App\Transaction::class)
                        <li class="nav-item">
                            <a class="nav-link" href="#shares">Shares</a>
                        </li>
                    @endcan

                    <li class="nav-item">
                        <a class="nav-link" href="#activities">Activities</a>
                    </li>

                    @can('update', \App\User::class)
                        <li class="nav-item">
                            <a class="nav-link" href="#bank">Bank Information</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#beneficiaries">Beneficiaries</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#settings">Account Settings</a>
                        </li>
                    @endcan
                </ul>

                <div class="block-content tab-content">

                    @can('view', \App\Transaction::class)
                        @include('member.transactions')
                    @endcan

                    @can('view', \App\Loan::class)
                        @include('member.loans')
                    @endcan

                    @can('view', \App\Transaction::class)
                        @include('member.pending-payments')
                    @endcan

                    @can('view', \App\Transaction::class)
                        @include('member.shares')
                    @endcan

                    @include('member.activity')

                    @can('update', \App\User::class)

                        @include('member.bank')

                        @include('member.beneficiaries')

                        @include('member.settings')

                    @endcan

                </div>
            </div>

        </div>

    </div>

    <script type="text/javascript">
        $(() => {

            $('#bank_nav').trigger('click');
        })
    </script>
@endsection
