<div class="tab-pane" id="bank" role="tabpanel">
	<h4 class="font-w400">Bank Information</h4>

	<form action="{{url('save-bank-information/'.$user->id)}}" method="post">

		{{csrf_field()}}


		<div class="row">

			<div class="form-group col-md-4 {{$errors->has('bank_id') ? 'is-invalid' : ''}}">
				<label>Bank</label>
				<select name="bank_id" class="form-control" required>
					@foreach(\App\Bank::get() as $row)
					@if(old('bank_id', $user->savings_account->bank_id) == $row->id)
					<option value="{{$row->id}}" selected>{{$row->name}}</option>
					@else
					<option value="{{$row->id}}">{{$row->name}}</option>
					@endif
					@endforeach
				</select>
				
				@if($errors->has('bank_id'))
				<div class="invalid-feedback">{{$errors->first('bank_id')}}</div>
				@endif
			</div>

			<div class="form-group col-md-4 {{$errors->has('bank_account_number') ? 'is-invalid' : ''}}">
				<label>Bank Account Number</label>
				<input type="number" class="form-control" name="bank_account_number" value="{{old('bank_account_number', $user->savings_account->bank_account_number)}}" required>
				@if($errors->has('bank_account_number'))
				<div class="invalid-feedback">{{$errors->first('bank_account_number')}}</div>
				@endif
			</div>

			<div class="form-group col-md-4 {{$errors->has('bank_account_name') ? 'is-invalid' : ''}}">
				<label>Bank Account Name</label>
				<input type="text" class="form-control" name="bank_account_name" value="{{old('bank_account_name', $user->savings_account->bank_account_name)}}" required>
				@if($errors->has('bank_account_name'))
				<div class="invalid-feedback">{{$errors->first('bank_account_name')}}</div>
				@endif
			</div>

		</div>

		<div class="form-group row">
			<div class="col-md-9">
				<button type="submit" class="btn btn-alt-primary">Submit</button>
			</div>
		</div>
	</form>

</div>