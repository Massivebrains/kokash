<div class="tab-pane" id="loans" role="tabpanel">
	<h4 class="font-w400">Loans</h4>

	<table class="table table-striped table-vcenter">
		<thead>
			<tr>
				<th>Name</th>
				<th>Reference</th>
				<th>Principal</th>				
				<th>Outstanding</th>
				<th>Status</th>
				<th>Disbursed</th>
				<th>Started</th>
				<th class="text-center" style="width: 100px;">Actions</th>
			</tr>
		</thead>
		<tbody>
			@foreach($loans as $row)
			<tr>
				<td>{{$row->name}}</td>
				<td>{{$row->reference}}</td>
				<td>{{_c($row->principal)}}<br><small>{{_percent($row->rate)}} / {{_years($row->time)}}</small></td>
				<td>{{_c($row->outstanding)}}</td>
				<td>{{_badge($row->status)}}</td>
				<td>{{_d($row->disbursed_at)}}</td>
				<td>{{_month($row->start_at)}}</td>
				<td class="text-center">
					<div class="btn-group">
						<a href="{{url('loan/'.$row->id)}}" class="btn btn-sm btn-secondary js-tooltip-enabled">
							<i class="fa fa-eye"></i>
						</a>
					</div>
				</td>
			</tr>
			@endforeach

		</tbody>
	</table>

	{{$loans->links()}}

</div>