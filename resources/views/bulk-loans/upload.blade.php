@extends('app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Bulk Loan Repayments</h3>
                </div>
                <div class="block-content">
                    <form action="/upload-bulk-loans" method="post" enctype=multipart/form-data>

                        {{ csrf_field() }}

                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>Loan Category</label>
                                <select class="form-control" name="loan_category_id" required>
                                    @foreach ($loan_categories as $row)
                                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-12 {{ $errors->has('description') ? 'is-invalid' : '' }}">
                                <label>Description</label>
                                <textarea type="text" class="form-control" name="description" required>{{ old('description') }}</textarea>
                                @if ($errors->has('description'))
                                    <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                                @endif
                            </div>

                        </div>

                        <div class="alert alert-info">See sample .csv file below. (Include the headers)</div>
                        <table class="table table-striped table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th>Member ID</th>
                                    <th>Loan Repayment Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>5000</td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="form-group col-md-12 {{ $errors->has('description') ? 'is-invalid' : '' }}">
                            <label>Select File</label>
                            <input type="file" name="loans" required class="form-control" />
                            @if ($errors->has('loans'))
                                <div class="invalid-feedback">{{ $errors->first('loans') }}</div>
                            @endif
                        </div>

                        <div class="form-group row">
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-alt-primary" onclick="return confirm('Are you sure? This action cannot be reversed.')">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
