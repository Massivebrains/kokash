@extends('app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Review Loan Repayments</h3>
                </div>
                <div class="block-content">

                    <form action="/save-bulk-loans" method="POST">
                        @csrf
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>Loan Category</label>
                                <input type="text" class="form-control" name="loan_category_id" value="{{ $loan_category->name }}" disabled readonly />
                                <input type="hidden" name="loan_category_id" value="{{ $loan_category->id }}" />
                            </div>
                            <div class="form-group col-md-12">
                                <label>Description</label>
                                <input type="text" class="form-control" disabled readonly value="{{ $description }}" />
                                <input type="hidden" name="description" value="{{ $description }}" />
                            </div>
                        </div>

                        @php($sum = 0)
                        <table class="table table-striped table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th>Loan</th>
                                    <th>Member</th>
                                    <th>Amount</th>
                                    <th>Current Outstanding</th>
                                    <th>Outstanding after Repayment</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($values as $row)
                                    <tr>
                                        <td>{{ $row->loan->name }}</td>
                                        <td>{{ $row->loan->user->name }}</td>
                                        <td>{{ _c($row->amount) }}</td>
                                        <td>{{ _c($row->loan->outstanding) }}</td>
                                        <td>
                                            {{ _c($row->loan->outstanding - $row->amount) }}
                                            <input type="hidden" class="form-control" name="amounts[{{ $row->loan->id }}]" value="{{ $row->amount }}">
                                        </td>
                                    </tr>

                                    @php($sum += $row->amount)
                                @endforeach

                                <tr>
                                    <td>
                                        <strong>Total Amount</strong>
                                    </td>
                                    <td></td>
                                    <td><strong>{{ _c($sum) }}</strong></td>
                                    <td colspan="3"></td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="row">

                            <div class="form-group col-md-12 {{ $errors->has('password') ? 'is-invalid' : '' }}">
                                <label>Your Account Password</label>
                                <input type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                                @endif
                            </div>

                        </div>


                        <div class="form-group row">
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-alt-primary" onclick="return confirm('Are you sure?')">Confirm Repayments</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
