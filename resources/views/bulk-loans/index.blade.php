@extends('app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Bulk Loan Repayments</h3>
                </div>
                <div class="block-content">

                    @isset($upload_errors)
                        @foreach ($upload_errors as $row)
                            <div class="alert alert-danger">
                                {{ $row }}
                            </div>
                        @endforeach
                    @endisset

                    <form action="/review-bulk-loans" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>Loan Category</label>
                                <select class="form-control" name="loan_category_id" required>
                                    @foreach ($loan_categories as $row)
                                        @if (isset($loan_category_id) && $loan_category_id == $row->id)
                                            <option value="{{ $row->id }}" selected>{{ $row->name }}</option>
                                        @else
                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('loan_category_id'))
                                    <div class="invalid-feedback">{{ $errors->first('loan_category_id') }}</div>
                                @endif
                            </div>
                            @isset($loans)
                                <div class="form-group col-md-12 {{ $errors->has('description') ? 'is-invalid' : '' }}">
                                    <label>Description</label>
                                    <textarea type="text" class="form-control" name="description" required>{{ old('description') }}</textarea>
                                    @if ($errors->has('description'))
                                        <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                                    @endif
                                </div>
                            @endisset
                        </div>

                        @isset($loans)
                            <table class="table table-striped table-sm table-bordered">
                                <thead>
                                    <tr>
                                        <th>Loan</th>
                                        <th>Member</th>
                                        <th>Outstanding</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($loans->count() == 0)
                                        <tr>
                                            <td colspan="4" class="text-center">There are no running Loans for this Category</td>
                                        </tr>
                                    @endif
                                    @foreach ($loans as $row)
                                        <tr>
                                            <td>{{ $row->name }}</td>
                                            <td>
                                                <a href="/member-info/{{ $row->user->id }}">{{ $row->user->name }}</a>
                                            </td>
                                            <td>{{ _c($row->outstanding) }}</td>
                                            <td>
                                                <input type="number" class="form-control" name="amounts[{{ $row->id }}]" style="width:200px" placeholder="0.00 Max: {{ $row->outstanding }}" step="0.01" max="{{ $row->outstanding }}">
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endisset

                        <div class="form-group row">
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-alt-primary">{{ isset($loans) ? 'Submit for Review' : 'Get Loans' }}</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
