@extends('app')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Bulk New Transactions</h3>
                <div class="block-options">

                </div>
            </div>
            <div class="block-content">

                <form action="{{url('save-bulk-transactions')}}" method="post">

                    {{csrf_field()}}

                    <div class="row">

                        <div class="form-group col-md-12 {{$errors->has('description') ? 'is-invalid' : ''}}">
                            <label>Description</label>
                            <textarea type="text" class="form-control" disabled readonly>{{$description}}</textarea>
                            <input type="hidden" name="description" value="{{$description}}">                           
                        </div>

                    </div>

                    @php
                    $sum = 0;
                    @endphp

                    <table class="table table-striped table-sm table-bordered">
                        <thead>
                            <tr>
                                <th>Member</th>
                                <th>Amount</th>
                                <th>Current Balance</th>
                                <th>Balance after Transaction</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user_transactions as $row)
                            <tr>
                                <td style="width:30%">{{$row->name}}</td>
                                <td>{{_c($row->amount)}}</td>
                                <td>{{_c($row->old_balance)}}</td>
                                <td>
                                    {{_c($row->old_balance + $row->amount)}}
                                    <input type="hidden" class="form-control" name="amounts[{{$row->user_id}}]" value="{{$row->amount}}">
                                </td>
                            </tr>

                            @php
                            $sum+= $row->amount;
                            @endphp

                            @endforeach

                            <tr>
                                <td style="width:30%"><h5>Total Amount</h5></td>
                                <td><h5>{{_c($sum)}}</h5></td>
                                <td colspan="2"></td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="row">

                        <div class="form-group col-md-12 {{$errors->has('password') ? 'is-invalid' : ''}}">
                            <label>Your Account Password</label>
                            <input type="password" class="form-control" name="password" required>

                            @if($errors->has('password'))
                            <div class="invalid-feedback">{{$errors->first('password')}}</div>
                            @endif
                        </div>

                    </div>

                    
                    <div class="form-group row">
                        <div class="col-md-9">
                            <button type="submit" class="btn btn-alt-primary" onclick="return confirm('Are you sure?')">Confirm Transactions</button>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>

    </div>
</div>
@endsection