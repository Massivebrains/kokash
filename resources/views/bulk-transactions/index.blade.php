@extends('app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Bulk New Transactions</h3>
                </div>
                <div class="block-content">
                    @isset($upload_errors)
                        @foreach ($upload_errors as $row)
                            <div class="alert alert-danger">
                                {{ $row }}
                            </div>
                        @endforeach
                    @endisset

                    <form action="{{ url('review-bulk-transactions') }}" method="post">

                        {{ csrf_field() }}

                        <div class="row">

                            <div class="form-group col-md-12 {{ $errors->has('description') ? 'is-invalid' : '' }}">
                                <label>Description</label>
                                <textarea type="text" class="form-control" name="description" required>{{ old('description') }}</textarea>

                                @if ($errors->has('description'))
                                    <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                                @endif
                            </div>

                        </div>

                        <table class="table table-striped table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th>Member</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $row)
                                    <tr>
                                        <td style="width:30%">
                                            <h4 style="margin-bottom:0">{{ $row->name }}</h4>
                                            Balance: {{ _c($row->savings_account->balance) }}
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" name="amounts[{{ $row->id }}]" style="width:200px" placeholder="0.00" value="{{ old('amount') }}" step="0.01">
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="form-group row">
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-alt-primary">Submit for Review</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
