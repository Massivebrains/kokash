@extends('app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Bulk New Transactions</h3>
                </div>
                <div class="block-content">
                    @isset($upload_errors)
                        @foreach ($upload_errors as $row)
                            <div class="alert alert-danger">
                                {{ $row }}
                            </div>
                        @endforeach
                    @endisset


                    <form action="/upload-bulk-transactions" method="post" enctype=multipart/form-data>

                        {{ csrf_field() }}

                        <div class="row">

                            <div class="form-group col-md-12 {{ $errors->has('description') ? 'is-invalid' : '' }}">
                                <label>Description</label>
                                <textarea type="text" class="form-control" name="description" required>{{ old('description') }}</textarea>

                                @if ($errors->has('description'))
                                    <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                                @endif
                            </div>

                        </div>

                        <div class="alert alert-info">See sample .csv file below. (Include the headers)</div>
                        <table class="table table-striped table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th>Member ID</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>500</td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="form-group col-md-12 {{ $errors->has('description') ? 'is-invalid' : '' }}">
                            <label>Select File</label>
                            <input type="file" name="transactions" required class="form-control" />
                            @if ($errors->has('transactions'))
                                <div class="invalid-feedback">{{ $errors->first('transactions') }}</div>
                            @endif
                        </div>

                        <div class="form-group row">
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-alt-primary" onclick="return confirm('Are you sure? This action cannot be reversed.')">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
