@extends('app')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Notification</h3>
                <div class="block-options">

                </div>
            </div>
            <div class="block-content">

                <form action="{{url('save-notification/'.$notification->id)}}" method="post">

                    {{csrf_field()}}

                    <div class="row">

                       <div class="form-group col-md-12 {{$errors->has('title') ? 'is-invalid' : ''}}">

                           <label>Title</label>
                           <input type="text" class="form-control" name="title" value="{{old('title', $notification->title)}}" required placeholder="Notification Title">

                           @if($errors->has('title'))
                           <div class="invalid-feedback">{{$errors->first('title')}}</div>
                           @endif
                       </div>

                       <div class="form-group col-md-12 {{$errors->has('notification') ? 'is-invalid' : ''}}">

                        <label>Notification</label>
                        <textarea type="text" class="form-control" name="notification" required placeholder="Notification">{{old('notification', $notification->notification)}}</textarea>

                        @if($errors->has('notification'))
                        <div class="invalid-feedback">{{$errors->first('notification')}}</div>
                        @endif
                    </div>

                    <div class="form-group col-md-12">

                        <label>Send as SMS</label>
                        <select name="sms"  class="form-control" required>
                            <option value="no">Don't Send as SMS</option>
                            <option value="yes">Also Send as SMS</option>
                        </select>

                    </div>

                    <div class="form-group col-md-12">

                       <label>Test SMS Phone Number</label>
                       <input type="text" class="form-control" name="phone" placeholder="Test SMS phone number">

                   </div>

                   <div class="form-group col-md-12">
                    @if(Gate::allows('update-notifications'))
                    <div class="form-group row">
                        <div class="col-md-9">
                            <button type="submit" class="btn btn-alt-primary">Submit</button>
                        </div>
                    </div>
                    @endif
                </div>

            </div>

        </div>


    </form>
</div>
</div>

</div>
</div>
@endsection