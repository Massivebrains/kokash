@extends('app')

@section('content')

    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Notifications <strong>[NOTIFICATION IS CURRENTLY {{ strtoupper(Auth::user()->company->notifications) }}]</strong></h3>

            @if (Gate::allows('update-notifications'))
                <div class="block-options">
                    <div class="block-options-item">
                        <a href="{{ url('notification') }}" class="btn btn-alt-primary btn-sm"><i class="si si-plus"></i> New Notification</a>
                        @if (Auth::user()->company->notifications == 'on')
                            <a href="/notification-switch" class="btn btn-alt-success btn-sm"><i class="si si-bell"></i> Turn notifications Off</a>
                        @else
                            <a href="/notification-switch" class="btn btn-alt-danger btn-sm"><i class="si si-bell"></i> Turn notifications On</a>
                        @endif
                    </div>
                </div>
            @endif

        </div>
        <div class="block-content">

            <table class="table table-striped table-vcenter">
                <thead>
                    <tr>
                        <th>User</th>
                        <th>Title</th>
                        <th>Notificatoin</th>
                        <th style="width:10%">Date</th>
                        <th class="text-center" style="width: 100px;">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($notifications as $row)
                        <tr>
                            <td><a href="{{ url('member-info/' . $row->user->id) }}">{{ $row->user->name }}</a></td>
                            <td>{{ $row->title }}</td>
                            <td>{{ $row->title == 'SMS' ? $row->notification : 'Email Body' }}</td>
                            <td>{{ $row->scheduled_at }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ url('notification/' . $row->id) }}" class="btn btn-sm btn-secondary js-tooltip-enabled">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection
