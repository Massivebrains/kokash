
<!doctype html>


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>KoKash</title>
    <meta name="description" content="Cooperative">
    <meta name="author" content="Olaiya Segun">
    <meta name="robots" content="noindex, nofollow">
    <link rel="shortcut icon" href="{{asset('/img/favicons/favicon.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('/img/favicons/favicon-192x192.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('/img/favicons/apple-touch-icon-180x180.png')}}">
    <link rel="stylesheet" id="css-main" href="{{asset('/css/codebase.min.css')}}">
    <style type="text/css">
</style>
</head>
<body>

    <div id="page-container" class="main-content-boxed">

        <main id="main-container">

            <div class="bg-gd-dusk">
                <div class="hero-static content content-full bg-white invisible" data-toggle="appear">

                    <div class="py-30 px-5 text-center">
                        <a class="link-effect font-w700" href="{{url('/')}}">
                            <span class="font-size-xl text-primary-dark">Ko</span><span class="font-size-xl">Kash</span>
                        </a>
                        <h1 class="h2 font-w700 mt-50 mb-10">The KoKash Portal</h1>
                        <h2 class="h4 font-w400 text-muted mb-0">{{isset($title) ? $title : 'Please sign in'}}</h2>
                    </div>


                    <div class="row justify-content-center px-5">
                        <div class="col-sm-8 col-md-6 col-lg-4">

                            @yield('body')

                        </div>
                    </div>

                </div>
            </div>

        </main>

    </div>



    <script src="{{asset('/js/core/jquery.min.js')}}"></script>
    <script src="{{asset('/js/core/popper.min.js')}}"></script>
    <script src="{{asset('/js/core/bootstrap.min.js')}}"></script>
    <script src="{{asset('/js/core/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('/js/core/jquery.scrollLock.min.js')}}"></script>
    <script src="{{asset('/js/core/jquery.appear.min.js')}}"></script>
    <script src="{{asset('/js/core/jquery.countTo.min.js')}}"></script>
    <script src="{{asset('/js/core/js.cookie.min.js')}}"></script>
    <script src="{{asset('/js/codebase.js')}}"></script>
    <script src="{{asset('/js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('/js/pages/op_auth_signin.js')}}"></script>
</body>
</html>