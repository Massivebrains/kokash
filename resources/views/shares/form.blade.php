@extends('app')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">{{$share->id > 0 ? 'Update '.$share->user->name.'\'s Shares' : 'New Shares'}}</h3>
                <div class="block-options">

                </div>
            </div>
            <div class="block-content">

                <form action="{{url('save-shares')}}" method="post">

                    {{csrf_field()}}

                    <div class="row">

                        <div class="form-group col-md-12">

                           <label>Member</label>

                           @if($share->id > 0)
                           <input type="text" class="form-control" value="{{$share->user->name}}" disabled readonly>
                           <input type="hidden" class="form-control" name="user_id" value="{{$share->user_id}}">
                           @else
                           <select class="select2 form-control country" name="user_id" required>
                               <option>--select--</option>
                               @foreach($users as $row)
                               <option value="{{$row->id}}">{{$row->name}}</option>
                               @endforeach
                           </select>
                           @endif


                           @if($errors->has('user_id'))
                           <div class="invalid-feedback">{{$errors->first('user_id')}}</div>
                           @endif
                       </div>

                       <div class="form-group col-md-6 {{$errors->has('units') ? 'is-invalid' : ''}}">
                        <label>Units</label>
                        <input type="number" class="form-control" name="units" value="{{old('units')}}" step="0.01" required>

                        @if($errors->has('units'))
                        <div class="invalid-feedback">{{$errors->first('units')}}</div>
                        @endif
                    </div>

                    <div class="form-group col-md-6 {{$errors->has('currency_value') ? 'is-invalid' : ''}}">
                        <label>Currency Equivalent</label>
                        <input type="number" class="form-control" name="currency_value" value="{{old('currency_value')}}" required>

                        @if($errors->has('currency_value'))
                        <div class="invalid-feedback">{{$errors->first('currency_value')}}</div>
                        @endif
                    </div>

                </div>

                <div class="row">

                    <div class="form-group col-md-12 {{$errors->has('password') ? 'is-invalid' : ''}}">
                        <label>Your Account Password</label>
                        <input type="password" class="form-control" name="password" required>

                        @if($errors->has('password'))
                        <div class="invalid-feedback">{{$errors->first('password')}}</div>
                        @endif
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-alt-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
</div>
@endsection