@extends('app')

@section('content')

<div class="block">
	<div class="block-header block-header-default">
		<h3 class="block-title">Shares</h3>
		<div class="block-options">

			@can('update', \App\Share::class)
			<div class="block-options-item">
				<a href="{{url('share/0')}}" class="btn btn-alt-primary btn-sm"><i class="si si-plus"></i> New Shares</a>
			</div>
			@endcan

		</div>
	</div>
	<div class="block-content">

		@component('components.search', ['placeholder' => 'Search shares by member name', 'url' => 'shares', 'count' => $count ?? 0, 'query' => $query ?? null]) @endcomponent

		<table class="table table-striped table-vcenter">
			<thead>
				<tr>
					<th>Member</th>
					<th>Total Units Bought</th>
					<th>Amount in Currency</th>
					<th>Last Updated</th>
					<th class="text-center" style="width: 100px;">Actions</th>
				</tr>
			</thead>
			<tbody>

				@foreach($shares as $row)
				<tr>
					<td>{{$row->user->name}}</td>
					<td>{{number_format($row->units, 2)}}</td>
					<td>{{_c($row->currency_value)}}</td>
					<td>{{_d($row->updated_at)}}</td>
					<td class="text-center">
						<div class="btn-group">
							<a href="{{url('share-history/'.$row->id)}}" class="btn btn-sm btn-secondary js-tooltip-enabled">
								<i class="fa fa-eye"></i>
							</a>
						</div>
					</td>
				</tr>
				@endforeach

			</tbody>
		</table>

		{{$shares->links()}}
	</div>
</div>

@endsection