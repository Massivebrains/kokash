@extends('app')

@section('content')

<div class="block">
	<div class="block-header block-header-default">
		<h3 class="block-title">{{$share->user->name}}'s Share history</h3>
		<div class="block-options">

			@can('update', \App\Share::class)
			<div class="block-options-item">
				<a href="{{url('update-share/'.$share->id)}}" class="btn btn-alt-primary btn-sm"><i class="si si-plus"></i> Update Share</a>
			</div>
			@endcan

		</div>
	</div>
	<div class="block-content">

		<table class="table table-striped table-vcenter">
			<thead>
				<tr>
					<th>History</th>
					<th>Date</th>
				</tr>
			</thead>
			<tbody>
				
				@foreach($activities as $row)
				<tr>
					<td>{{$row->description}}</td>
					<td>{{_d($row->created_at)}}</td>
				</tr>
				@endforeach

			</tbody>
		</table>
	</div>
</div>

@endsection