<!doctype html>
<html lang="en" class="no-focus">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>KoKash {{ $title ?? '' }}</title>

    <meta name="description" content="Cooperative Banking Platform">
    <meta name="author" content="olaiya segun">
    <meta name="robots" content="noindex, nofollow">
    <link rel="shortcut icon" href="{{ asset('img/kokash.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('img/kokash.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/kokash.png') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('css/codebase.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/select2/select2-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
    <script src="/js/core/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>

    <style type="text/css">
        button {

            cursor: pointer;
        }

    </style>

    @yield('style')
</head>

<body>
    <div id="page-container" class="sidebar-o side-overlay-hover side-scroll page-header-{{ Auth::user()->impersonating == 1 ? 'inverse' : 'classic' }} main-content-boxed {{ Auth::user()->impersonating == 1 ? 'sidebar-inverse' : '' }}">

        @include('components.nav')


        <main id="main-container">

            @if (Auth::user()->impersonating == 1)
                <div class="alert alert-danger">
                    <marquee><strong style="">You are currently impersonating {{ Auth::user()->company->name }}</strong></marquee>
                </div>
            @endif

            <div class="content">
                <h2 class="content-heading">{{ isset($title) ? strtoupper(implode(' ', explode('_', $title))) : 'KoKash' }}</h2>

                @include('components.alert')

                @yield('content')

            </div>

        </main>



        <footer id="page-footer" class="opacity-0">
            <div class="content py-20 font-size-xs clearfix">
                <div class="float-right">
                    Crafted with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="mailto:vadeshayo@gmail.com" target="_blank">Olaiya Segun</a>
                </div>
                <div class="float-left">
                    <a class="font-w600" href="#" target="_blank">KoKash 1.1.0</a> &copy; <span class="js-year-copy">{{ date('Y') }}</span>
                </div>
            </div>
        </footer>

    </div>


    <script src="{{ asset('/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('/js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/js/core/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('/js/core/jquery.scrollLock.min.js') }}"></script>
    <script src="{{ asset('/js/core/jquery.appear.min.js') }}"></script>
    <script src="{{ asset('/js/core/jquery.countTo.min.js') }}"></script>
    <script src="{{ asset('/js/core/js.cookie.min.js') }}"></script>
    <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('/js/codebase.js') }}"></script>
    <script type="text/javascript">
        $('.select2').select2();
        $('.date').datepicker({
            format: 'dd-mm-yyyy'
        });
        $('.month').datepicker({
            minViewMode: 'months',
            viewMode: 'months',
            format: 'mm-yyyy'
        });


        $('.country').change(() => {

            $.get('{{ url('utils/states') }}/' + $('.country').val(), response => {

                $('.state').html(response)
            })
        })
    </script>

    @yield('script')
</body>

</html>
