
<form action="{{url($url)}}" method="POST">

  {{csrf_field()}}

  <div class="input-group">

    <input type="text" name="query" required class="form-control" placeholder="{{$placeholder ?? 'Search'}}" required>
    
    <span class="input-group-btn">
      <button type="submit" class="btn btn-secondary">
        <i class="fa fa-search"></i> Search
      </button>
    </span>

  </div>

</form>

@if(isset($query) && $query != null)

  @if($count > 0)

    <div class="font-size-h5 font-w600 py-30 mb-20 text-center border-b">
      <span class="text-primary">{{$count}}</span> {{str_plural('result', $count)}} found for <mark class="text-danger">{{$query}}</mark>
    </div>

  @else

    <div class="font-size-h5 font-w600 py-30 mb-20 text-center border-b">
      <span class="text-primary">No results found for </span> <mark class="text-danger">{{$query}}</mark>
    </div>

  @endif

@endif