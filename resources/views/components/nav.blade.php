@php
$user = Auth::user();
@endphp

<nav id="sidebar">

    <div id="sidebar-scroll">

        <div class="sidebar-content">

            <div class="content-header content-header-fullrow px-15">

                <div class="content-header-section sidebar-mini-visible-b">

                    <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                        <span class="text-dual-primary-dark">K</span><span class="text-primary">K</span>
                    </span>

                </div>

                <div class="content-header-section text-center align-parent sidebar-mini-hidden">

                    <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                        <i class="fa fa-times text-danger"></i>
                    </button>

                    <div class="content-header-item">
                        <a class="link-effect font-w700" href="#">
                            <span class="font-size-xl text-dual-primary-dark">{{ Auth::check() ? Auth::user()->company->name : 'KOKASH ADMIN' }}</span></span>
                        </a>
                    </div>

                </div>

            </div>


            <div class="content-side content-side-full content-side-user px-10 align-parent">

                <div class="sidebar-mini-visible-b align-v animated fadeIn">
                    <img class="img-avatar img-avatar32" src="/img/avatars/avatar15.jpg" alt="">
                </div>



                <div class="sidebar-mini-hidden-b text-center">
                    <a class="img-link" href="#">
                        <img class="img-avatar" src="/img/avatars/avatar15.jpg" alt="">
                    </a>
                    <ul class="list-inline mt-10">
                        <li class="list-inline-item">
                            <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase" href="#">{{ Auth::user()->name }}</a>
                        </li>
                        <li class="list-inline-item">

                            <a class="link-effect text-dual-primary-dark" data-toggle="layout" data-action="sidebar_style_inverse_toggle" href="javascript:void(0)">
                                <i class="si si-drop"></i>
                            </a>
                        </li>
                        @if (Auth::user()->impersonating == 1)
                            <li class="list-inline-item">
                                <a class="link-effect text-dual-primary-dark" href="{{ url('auth/undo-impersonation') }}">
                                    <i class="si si-user"></i>
                                </a>
                            </li>
                        @endif
                        <li class="list-inline-item">
                            <a class="link-effect text-dual-primary-dark" href="{{ url('logout') }}">
                                <i class="si si-logout"></i>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>



            <div class="content-side content-side-full">

                <ul class="nav-main">

                    @if ($user->type == 'admin')

                        <li>
                            <a href="{{ url('dashboard') }}"><i class="si si-home"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                        </li>

                        <li>
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-users"></i><span class="sidebar-mini-hide">Members</span></a>
                            <ul>
                                <li>
                                    <a href="{{ url('members') }}">All Members</a>

                                    @can('update', \App\User::class)
                                        <a href="{{ url('member/') }}">New Member</a>
                                    @endcan

                                </li>
                            </ul>
                        </li>

                        @can('view', \App\Transaction::class)
                            <li>
                                <a href="{{ url('transactions') }}"><i class="si si-calculator"></i>Transactions</span></a>
                            </li>
                        @endcan

                        @can('view', \App\BankStatement::class)

                            <li>
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-calendar"></i><span class="sidebar-mini-hide">Bank Statements</span></a>
                                <ul>
                                    <li>
                                        <a href="{{ url('bank-statement/matched') }}">Matched Statements</a>
                                        <a href="{{ url('bank-statement/unmatched') }}">Unmatched Statements</a>
                                        <a href="{{ url('bank-statement/discarded') }}">Discarded Statements</a>
                                        <a href="{{ url('bank-statement/upload') }}">Upload Statement</a>
                                    </li>
                                </ul>
                            </li>

                        @endcan

                        @can('view', \App\Loan::class)

                            <li>
                                <a href="/loan-categories"><i class="si si-docs"></i>Loan Categories</span></a>
                            </li>

                            <li>
                                <a href="{{ url('loans') }}"><i class="si si-bar-chart"></i>Loans</span></a>
                            </li>

                            <li>
                                <a href="{{ url('shares') }}"><i class="si si-share"></i>Shares</span></a>
                            </li>

                        @endcan

                        <li>
                            <a href="{{ url('notifications') }}"><i class="si si-bell"></i>Notifications</span></a>
                        </li>

                        @can('view', \App\Statement::class)

                            {{-- <li>
                        <a href="{{url('statements')}}"><i class="si si-paper-clip"></i>Statements</span></a>
                    </li> --}}

                        @endcan

                        <li>
                            <a href="{{ url('faqs') }}"><i class="si si-question"></i>FAQS</span></a>
                        </li>

                        @if (Gate::allows('view-report'))

                            <li>
                                <a href="{{ url('report') }}"><i class="si si-pie-chart"></i><span class="sidebar-mini-hide">Reports</span></a>
                            </li>

                        @endif

                        @can('update', \App\User::class)

                            <li>
                                <a href="{{ url('admins') }}"><i class="si si-people"></i>Administrators</span></a>
                            </li>

                        @endcan
                    @endif

                    @if (Auth::user()->type == 'zeus')

                        <li>
                            <a href="{{ url('zeus/dashboard') }}"><i class="fa fa-dashboard"></i>Dashboard</span></a>
                        </li>

                        <li>
                            <a href="{{ url('zeus/companies') }}"><i class="fa fa-building-o"></i>Companies</span></a>
                        </li>

                        <li>
                            <a href="{{ url('zeus/payment-sources') }}"><i class="fa fa-clone"></i>Payment Sources</span></a>
                        </li>

                        <li>
                            <a href="{{ url('zeus/transaction-types') }}"><i class="fa fa-cubes"></i>Transaction Types</span></a>
                        </li>

                        <li>
                            <a href="{{ url('zeus/migration') }}"><i class="fa fa-cloud-upload"></i>Data Migration</span></a>
                        </li>

                        <li>
                            <a href="{{ url('zeus/dashboard/app-logs') }}"><i class="fa fa-file"></i>Support App - Logs</span></a>
                        </li>

                    @endif

                </ul>
            </div>

        </div>

    </div>

</nav>



<header id="page-header">

    <div class="content-header">

        <div class="content-header-section">


            <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="sidebar_toggle">
                <i class="fa fa-navicon"></i>
            </button>

            <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="header_search_on">
                <i class="fa fa-search"></i>
            </button>


        </div>


        <div class="content-header-section">

            <div class="btn-group" role="group">
                <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ Auth::user()->name }}<i class="fa fa-angle-down ml-5"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right min-width-150" aria-labelledby="page-header-user-dropdown">
                    <a class="dropdown-item" href="#">
                        <i class="si si-user mr-5"></i> Profile
                    </a>

                    <div class="dropdown-divider"></div>



                    <a class="dropdown-item" href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_toggle">
                        <i class="si si-wrench mr-5"></i> Settings
                    </a>

                    <div class="dropdown-divider"></div>

                    @if (Auth::user()->impersonating == 1)
                        <a class="dropdown-item" href="{{ url('undo-impersonation') }}">
                            <i class="si si-user mr-5"></i> Undo Impersonation
                        </a>
                    @endif

                    <a class="dropdown-item" href="{{ url('logout') }}">
                        <i class="si si-logout mr-5"></i> Sign Out
                    </a>
                </div>
            </div>

            <a href="{{ url('logout') }}" class="btn btn-circle btn-dual-secondary">
                <i class="si si-logout"></i>
            </a>

        </div>

    </div>

    <div id="page-header-search" class="overlay-header">
        <div class="content-header content-header-fullrow">
            <form action="$" method="post">
                <div class="input-group">
                    <span class="input-group-btn">

                        <button type="button" class="btn btn-secondary" data-toggle="layout" data-action="header_search_off">
                            <i class="fa fa-times"></i>
                        </button>

                    </span>
                    <input type="text" class="form-control" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-secondary">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>


    <div id="page-header-loader" class="overlay-header bg-primary">
        <div class="content-header content-header-fullrow text-center">
            <div class="content-header-item">
                <i class="fa fa-sun-o fa-spin text-white"></i>
            </div>
        </div>
    </div>

</header>
