@extends('app')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Loan Category</h3>
                <div class="block-options">

                </div>
            </div>
            <div class="block-content">

                <form action="/loan-category" method="POST">

                    @csrf

                    <div class="row">

                     <div class="form-group col-md-6 {{$errors->has('name') ? 'is-invalid' : ''}}">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" value="{{old('name', $loan_category->name)}}" required>
                        @if($errors->has('name'))
                        <div class="invalid-feedback">{{$errors->first('name')}}</div>
                        @endif
                    </div>

                </div>               

                <input type="hidden" name="id" value="{{$loan_category->id}}">
                <div class="form-group row">
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-alt-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
</div>
@endsection