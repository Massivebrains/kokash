@extends('app')

@section('content')

<div class="block">
	<div class="block-header block-header-default">
		<h3 class="block-title">Loan Categories</h3>
		<div class="block-options">
			<div class="block-options-item">
				<a href="/loan-category/0" class="btn btn-primary btn-sm">New Loan Category</a>
			</div>
		</div>
	</div>
	<div class="block-content">

		<table class="table table-striped table-vcenter">
			<thead>
				<tr>
					<th>Name</th>
					<th>Running Loans</th>
					<th>Completed Loans</th>
					<th>Cancelled Loans</th>
					<th class="text-center" style="width: 100px;">Actions</th>
				</tr>
			</thead>
			<tbody>

				@foreach($loan_categories as $row)
				<tr>
					<td>{{$row->name}}</td>
					<td>{{$row->loans()->whereStatus('running')->count()}}</td>
					<td>{{$row->loans()->whereStatus('completed')->count()}}</td>
					<td>{{$row->loans()->whereStatus('cancelled')->count()}}</td>
					<td class="text-center">
						<div class="btn-group">
							<a href="/loan-category/{{$row->id}}" class="btn btn-sm btn-secondary js-tooltip-enabled">
								<i class="fa fa-pencil"></i>
							</a>
						</div>
					</td>
				</tr>
				@endforeach

			</tbody>
		</table>
	</div>
</div>

@endsection