@extends('app')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Statement</h3>
                <div class="block-options">

                </div>
            </div>
            <div class="block-content">

                <form action="{{url('save-deposit')}}" method="post">

                    {{csrf_field()}}

                    <div class="row">

                        <div class="form-group col-md-12">
                            <div class="form-material floating">
                                <select class="select2 form-control country" name="country_id" required>
                                 @foreach($users as $row)
                                 <option value="{{$row->id}}">{{$row->name}}</option>
                                 @endforeach
                             </select>
                             <label>Member</label>
                         </div>
                         @if($errors->has('user_id'))
                         <div class="invalid-feedback">{{$errors->first('user_id')}}</div>
                         @endif
                     </div>

                     
                </div>

            
                <div class="row">

                    <div class="form-group col-md-6 {{$errors->has('start_date') ? 'is-invalid' : ''}}">
                        <div class="form-material floating">
                            <input type="start_date" class="form-control" name="start_date" required>
                            <label>Start Date</label>
                        </div>
                        @if($errors->has('start_date'))
                        <div class="invalid-feedback">{{$errors->first('start_date')}}</div>
                        @endif
                    </div>

                    <div class="form-group col-md-6 {{$errors->has('end_date') ? 'is-invalid' : ''}}">
                        <div class="form-material floating">
                            <input type="end_date" class="form-control" name="end_date" required>
                            <label>End Date</label>
                        </div>
                        @if($errors->has('end_date'))
                        <div class="invalid-feedback">{{$errors->first('end_date')}}</div>
                        @endif
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-alt-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
</div>
@endsection