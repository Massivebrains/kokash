@extends('app')

@section('content')

<div class="block">
	<div class="block-header block-header-default">
		<h3 class="block-title">Statements</h3>
		<div class="block-options">
			<div class="block-options-item">
				<a href="{{url('statement')}}" class="btn btn-alt-primary btn-sm"><i class="si si-plus"></i> New Statement</a>
			</div>
		</div>
	</div>
	<div class="block-content">

		@component('components.search', ['placeholder' => 'Search statements', 'url' => 'savings']) @endcomponent

		<table class="table table-striped table-vcenter">
			<thead>
				<tr>
					<th>Member</th>
					<th>Statement Start Date</th>
					<th>Statement End Date</th>
					<th>Date Requested</th>
					<th>Service Charge</th>
					<th>Status</th>
					<th class="text-center" style="width: 100px;">Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($statements as $row)
				<tr>
					<td><a href="{{url('member-info/'.$row->user->id)}}">{{$row->user->name}}</a></td>
					<td>{{_d($row->start_date, false)}}</td>
					<td>{{_d($row->end_date, false)}}</td>
					<td>{{_d($row->created_at, true)}}</td>
					<td>{{_c(optional($row->transaction)->amount)}}</td>
					<td>{{_badge($row->status)}}</td>
					<td class="text-center">
						<div class="btn-group">
							<a href="{{url('members/'.$row->id)}}" class="btn btn-sm btn-secondary js-tooltip-enabled">
								<i class="fa fa-pencil"></i>
							</a>
							<button type="button" class="btn btn-sm btn-secondary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Delete">
								<i class="fa fa-eye"></i> Download
							</button>
						</div>
					</td>
				</tr>
				@endforeach

			</tbody>
		</table>
	</div>
</div>

@endsection