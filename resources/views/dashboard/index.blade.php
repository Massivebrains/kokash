@extends('app')

@section('content')


<div class="row gutters-tiny js-appear-enabled animated fadeIn" data-toggle="appear">

    <div class="col-6 col-xl-3">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-left mt-10 d-none d-sm-block">
                    <i class="si si-users fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h3 font-w600 js-count-to-enabled" data-toggle="countTo" data-speed="1000" data-to="{{$active_members}}">{{$active_members}}</div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">Active Members</div>
            </div>
        </a>
    </div>
    <div class="col-6 col-xl-3">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-left mt-10 d-none d-sm-block">
                    <i class="si si-users fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h3 font-w600"><span data-toggle="countTo" data-speed="1000" data-to="{{$inactive_members}}" class="js-count-to-enabled">{{$inactive_members}}</span></div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">Inactive Members</div>
            </div>
        </a>
    </div>

    @can('view', \App\Loan::class)
    <div class="col-6 col-xl-3">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-left mt-10 d-none d-sm-block">
                    <i class="si si-bar-chart fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h3 font-w600 js-count-to-enabled" data-toggle="countTo" data-speed="1000" data-to="{{$running_loans}}">{{$running_loans}}</div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">Running Loans</div>
            </div>
        </a>
    </div>

    <div class="col-6 col-xl-3">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-left mt-10 d-none d-sm-block">
                    <i class="si si-bar-chart fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h3 font-w600 js-count-to-enabled" data-toggle="countTo" data-speed="1000" data-to="{{$completed_loans}}">{{$completed_loans}}</div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">Completed Loans</div>
            </div>
        </a>
    </div>
    @endcan
    
</div>

<div class="row gutters-tiny js-appear-enabled animated fadeIn" data-toggle="appear">

    @can('view', \App\Transaction::class)

    <div class="col-md-4">
        <div class="block">
            <div class="block-content block-content-full">
                <div class="py-20 text-center">
                    <div class="mb-20">
                        <i class="fa fa-calculator fa-4x text-primary"></i>
                    </div>
                    <div class="font-size-h4 font-w600">NGN{{_to_k($total_transactions)}} Savings</div>
                    <div class="text-muted">Savings this month</div>
                    <div class="pt-20">
                        <a class="btn btn-rounded btn-alt-primary" href="{{url('transactions')}}">
                            <i class="fa fa-cog mr-5"></i> View all transactions
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endcan
    

    <div class="col-md-4">
        <div class="block">
            <div class="block-content block-content-full">
                <div class="py-20 text-center">
                    <div class="mb-20">
                        <i class="si si-users fa-4x text-info"></i>
                    </div>
                    <div class="font-size-h4 font-w600">+{{$new_users}} {{str_plural('Members', $new_users)}}</div>
                    <div class="text-muted">New members this month</div>
                    <div class="pt-20">
                        <a class="btn btn-rounded btn-alt-info" href="{{url('members')}}">
                            <i class="fa fa-users mr-5"></i> View all Members
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @can('view', \App\Loan::class)

    <div class="col-md-4">
        <div class="block">
            <div class="block-content block-content-full">
                <div class="py-20 text-center">
                    <div class="mb-20">
                        <i class="fa fa-check fa-4x text-success"></i>
                    </div>
                    <div class="font-size-h4 font-w600">Loans Health</div>
                    <div class="text-muted">Members response to loans repayment</div>
                    <div class="pt-20">
                        <a class="btn btn-rounded btn-alt-success" href="{{url('loans')}}">
                            <i class="fa fa-arrow-up mr-5"></i> View all loans
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endcan
    
</div>

<div class="row gutters-tiny js-appear-enabled animated fadeIn" data-toggle="appear">

    @can('view', \App\Transaction::class)

    <h3 class="content-heading">Recent Transactions</h3>
    
    <table class="table table-striped bg-white table-vcenter">
        <thead>
            <tr>
                <th>Date</th>
                <th>Amount</th>
                <th>Type</th>
                <th>Source</th>
                <th>Member</th>
                <th>Balance</th>
                <th>Reference</th>
                <th class="text-center" style="width: 100px;">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($transactions as $row)
            <tr>
                <td>{{_d($row->created_at)}}</td>
                <td>{{_c($row->amount)}}</td>
                <td>{{_badge($row->transaction_type->name)}}</td>
                <td>{{_badge($row->payment_source->name)}}</td>
                <td><a href="{{url('member-info/'.$row->user->id)}}">{{$row->user->name}}</a></td>
                <td>{{_c($row->running_balance)}}</td>
                <td>{{$row->reference}}</td>
                <td class="text-center">
                    <div class="btn-group">
                        <a href="{{url('transaction/'.$row->id)}}" class="btn btn-sm btn-secondary js-tooltip-enabled">
                            <i class="fa fa-eye"></i>
                        </a>
                    </div>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>

    @endcan

    @can('view', \App\Loan::class)

    <h3 class="content-heading">Recent Loan Transactions</h3>

    <table class="table table-striped bg-white table-bordered table-vcenter">
        <thead>
            <tr>
                <th>Loan</th>
                <th>Member</th>
                <th>Reference</th>
                <th>Month</th>
                <th>Amount</th>
                <th>Status</th>
                <th>Payment Source</th>
            </tr>
        </thead>

        <tbody>

            @foreach($loan_transactions as $row)
            <tr>
                <td><a href="{{url('loan/'.$row->loan_id)}}">{{$row->loan->name}}</a></td>
                <td><a href="{{url('member-info/'.$row->loan->user_id)}}">{{$row->loan->user->name}}</a></td>
                <td>{{$row->reference}}</td>
                <td>{{_month($row->date)}}</td>
                <td>{{_c($row->amount)}}</td>
                <td>{{_badge($row->status)}}</td>
                <td>{{_badge(optional($row->payment_source)->name)}}</td>
            </tr>
            @endforeach

        </tbody>
    </table>

    @endcan

</div>
@endsection
