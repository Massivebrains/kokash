<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Savings Transactions Migration</h3>
    </div>
    <div class="block-content">

        <blockquote>Ensure your .csv file is in the format is displayed below <strong>Including the Headings</strong></blockquote>

        <table class="table table-bordered table-responsive table-sm" style="font-size:0.8em !important">
            <thead>
                <tr>
                    <th>Member Phone</th>
                    <th>Member Name</th>
                    <th>Member Email</th>
                    <th>Amount</th>
                </tr>
                <tr>
                    <td>Required</td>
                    <td>Required</td>
                    <td>Optional</td>
                    <td>Required</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>


        <form method="post" action="{{ url('zeus/migration/transactions') }}" enctype="multipart/form-data" class="mb-10">

            {{ csrf_field() }}

            <div class="form-group row">

                <div class="col-12">

                    <label class="col-12">Select File (.csv or txt Only)</label>
                    <div class="col-12">
                        <input type="file" name="transactions" required>
                        @if ($errors->has('transactions'))
                            <div class="text-danger">{{ $errors->first('transactions') }}</div>
                        @endif
                    </div>
                    <div class="form-group col-12 mt-2">
                        <label class="mt-10">Description</label>
                        <input name="description" class="form-control" required />
                    </div>

                    <label class="col-12 mt-10">Select Company</label>
                    <div class="col-12">
                        <select name="company_id" class="form-control" required>
                            @foreach (\App\Company::get() as $row)
                                <option value="{{ $row->id }}">{{ $row->name }} - {{ $row->description }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-12">
                        <button type="submit" name="upload" class="btn btn-primary mt-10">
                            Upload
                        </button>
                    </div>

                </div>

            </div>

        </form>

    </div>
</div>
