<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Members Migration</h3>
    </div>
    <div class="block-content" style="width: 100%; overflow:scroll">

        <blockquote>Ensure your .csv file is in the format is displayed below <strong>Including the Headings</strong></blockquote>

        <table class="table table-bordered table-responsive table-sm" style="font-size:0.8em !important;margin-right:10px;">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Sex</th>
                    <th>Country</th>
                    <th>State</th>
                    <th>Marital Status</th>
                    <th>Address</th>
                    <th>Occupation</th>
                    <th>Employer</th>
                    <th>Employer Address</th>
                    <th>Next of Kin</th>
                    <th>Next of Kin Phone Number</th>
                    <th>Monthly Savings Amount</th>
                    <th>Bank ID</th>
                    <th>Bank Account Number</th>
                    <th>Bank Account Name</th>
                </tr>
                <tr>
                    <td>Required</td>
                    <td>Required</td>
                    <td>Required</td>
                    <td>Required</td>
                    <td>Required (Male/Female)</td>
                    <td>Optional</td>
                    <td>Optional</td>
                    <td>Single/Married</td>
                    <td>Required</td>
                    <td>Required</td>
                    <td>Optional</td>
                    <td>Optional</td>
                    <td>Optional</td>
                    <td>Optional</td>
                    <td>Optional</td>
                    <td>Optional</td>
                    <td>Optional</td>
                    <td>Optional</td>
                </tr>
            </thead>
            <tbody>


            </tbody>
        </table>


        <form method="post" action="{{url('zeus/migration/members')}}" enctype="multipart/form-data" class="mb-10">

            {{csrf_field()}}
            
            <div class="row">
                
                <div class="col-6">
                    <div class="form-group row">

                        <label class="col-12">Select File (.csv or txt Only)</label>
                        <div class="col-12">
                            <input type="file" name="members" required>
                            @if($errors->has('members'))
                            <div class="text-danger">{{$errors->first('members')}}</div>
                            @endif
                        </div>

                        <label class="col-12 mt-10">Select Company</label>
                        <div class="col-12">
                            <select name="company_id" class="form-control" required>
                                @foreach(\App\Company::get() as $row)
                                <option value="{{$row->id}}">{{$row->name}} - {{$row->description}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                </div>

                <div class="col-6" style="height:500px; overflow: scroll">
                    <label>Banks</label>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(\App\Bank::get() as $row)
                            <tr>
                                <td>{{$row->id}}</td>
                                <td>{{$row->name}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                
            </div>

            <button type="submit" name="upload" class="btn btn-primary">
                Upload
            </button>

        </form>

    </div>
</div>