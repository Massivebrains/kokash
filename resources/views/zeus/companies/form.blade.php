@extends('app')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Company</h3>
                <div class="block-options">

                </div>
            </div>
            <div class="block-content">

                <form action="{{url('zeus/save-company/'.$company->id)}}" method="post">

                    {{csrf_field()}}

                    <div class="row">

                     <div class="form-group col-md-6 {{$errors->has('name') ? 'is-invalid' : ''}}">
                        <div class="form-material floating">
                            <input type="text" class="form-control" name="name" value="{{old('name', $company->name)}}" required>
                            <label>Short Name</label>
                        </div>
                        @if($errors->has('name'))
                        <div class="invalid-feedback">{{$errors->first('name')}}</div>
                        @endif
                    </div>

                    <div class="form-group col-md-6 {{$errors->has('description') ? 'is-invalid' : ''}}">
                        <div class="form-material floating">
                            <input type="text" class="form-control" name="description" value="{{old('description', $company->description)}}" required>
                            <label>Long Name</label>
                        </div>
                        @if($errors->has('description'))
                        <div class="invalid-feedback">{{$errors->first('description')}}</div>
                        @endif
                    </div>

                </div>

                <div class="row">

                    <div class="form-group col-md-12 {{$errors->has('address') ? 'is-invalid' : ''}}">
                        <div class="form-material floating">
                            <textarea type="text" class="form-control" name="address" required>{{old('address', $company->address)}}</textarea>
                            <label>Address</label>
                        </div>

                        @if($errors->has('address'))
                        <div class="invalid-feedback">{{$errors->first('address')}}</div>
                        @endif
                    </div>

                </div>

                <div class="row">

                    <div class="form-group col-md-4 {{$errors->has('contact_person') ? 'is-invalid' : ''}}">
                        <div class="form-material floating">
                            <input type="text" class="form-control" name="contact_person" required value="{{old('contact_person', $company->contact_person)}}">
                            <label>Contact Person's Name</label>
                        </div>
                        @if($errors->has('contact_person'))
                        <div class="invalid-feedback">{{$errors->first('contact_person')}}</div>
                        @endif
                    </div>

                    <div class="form-group col-md-4 {{$errors->has('contact_person_phone') ? 'is-invalid' : ''}}">
                        <div class="form-material floating">
                            <input type="text" class="form-control" name="contact_person_phone" required value="{{old('contact_person_phone', $company->contact_person_phone)}}">
                            <label>Contact Person's Phone</label>
                        </div>
                        @if($errors->has('contact_person_phone'))
                        <div class="invalid-feedback">{{$errors->first('contact_person_phone')}}</div>
                        @endif
                    </div>

                    <div class="form-group col-md-4 {{$errors->has('contact_person_email') ? 'is-invalid' : ''}}">
                        <div class="form-material floating">
                            <input type="text" class="form-control" name="contact_person_email" required value="{{old('contact_person_email', $company->contact_person_email)}}">
                            <label>Contact Person's Email</label>
                        </div>
                        @if($errors->has('contact_person_email'))
                        <div class="invalid-feedback">{{$errors->first('contact_person_email')}}</div>
                        @endif
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-alt-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
</div>
@endsection