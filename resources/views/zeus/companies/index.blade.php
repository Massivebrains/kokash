@extends('app')

@section('content')

<div class="block">
	<div class="block-header block-header-default">
		<h3 class="block-title">Companies</h3>
		<div class="block-options">
			<div class="block-options-item">
				<a href="{{url('zeus/company/0')}}" class="btn btn-primary btn-sm">New Company</a>
			</div>
		</div>
	</div>
	<div class="block-content">

		<table class="table table-striped table-vcenter">
			<thead>
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Address</th>
					<th>Contact</th>
					<th>Phone</th>
					<th>Email</th>
					<th>Status</th>
					<th class="text-center" style="width: 100px;">Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($companies as $row)
				<tr>
					<td>{{$row->name}}</td>
					<td>{{$row->description}}</td>
					<td>{{$row->address}}</td>
					<td>{{$row->contact_person}}</td>
					<td>{{$row->contact_person_phone}}</td>
					<td>{{$row->contact_person_email}}</td>
					<td>{{_badge($row->status)}}</td>
					<td class="text-center">
						<div class="btn-group">
							<a href="{{url('zeus/company/'.$row->id)}}" class="btn btn-sm btn-secondary js-tooltip-enabled">
								<i class="fa fa-pencil"></i>
							</a>
							<a href="{{url('zeus/company/setting/'.$row->id)}}" class="btn btn-sm btn-secondary js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Delete">
								<i class="fa fa-cogs"></i>
							</a>
							<a href="{{url('zeus/company/impersonate/'.$row->id)}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" data-placement="top" title="Impersonate">
								<i class="si si-eyeglasses"></i>
							</a>
						</div>
					</td>
				</tr>
				@endforeach

			</tbody>
		</table>
	</div>
</div>

@endsection