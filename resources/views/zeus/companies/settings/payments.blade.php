 <div class="form-group row">
    <label class="col-lg-4 col-form-label" for="example-hf-email">
        Funds Disbursement<br>
        <small class="text-muted">
            Choose how you move money out of the system.
        </small>
    </label>
    
    <div class="col-lg-8">

     <div class="custom-controls-stacked">
        <label class="custom-control custom-checkbox mb-10">
            <input type="checkbox" class="custom-control-input" id="example-checkbox1" name="example-checkbox1" value="option1" checked="">
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">
                Disburse money offline<br>
                <small>Manually update transactions such as Loan disbursement as Disbursed after an offline transaction has taken place.</small>
            </span>
        </label>
        <label class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="example-checkbox2" name="example-checkbox2" value="option2">
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">
                Disburse money in System <small class="text-danger">(Coming Soon <a href="#">Learn more</a>)</small> <br>
                <small>Transfer funds to any Nigerian Bank account. Powered by <a href="https://paystack.com">Paystack</a></small></span>
            </label>
        </div>

        <div class="form-group col-md-6 {{$errors->has('paystack_sub_account_code') ? 'is-invalid' : ''}}">
            <div class="form-material">
                <input type="text" class="form-control" name="paystack_sub_account_code" value="{{old('paystack_sub_account_code', $setting->paystack_sub_account_code)}}">
                <label>Paystack Sub Account Code</label>
                <small>Sub Account code from paystack for daily disbursements</small>
            </div>
        </div>

    </div>
</div>