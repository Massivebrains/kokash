<div class="row">

	<div class="form-group col-md-6 {{$errors->has('savings_interest') ? 'is-invalid' : ''}}">
		<div class="form-material">
			<input type="number" class="form-control" name="savings_interest" value="{{old('savings_interest', $setting->savings_interest)}}" required>
			<label>Savings Monthly Interest ( % Percentage )</label>
		</div>
	</div>

</div>