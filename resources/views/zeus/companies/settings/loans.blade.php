<div class="row">

	<div class="form-group col-md-6 {{$errors->has('loan_fine_fee') ? 'is-invalid' : ''}}">
		<div class="form-material">
			<input type="number" class="form-control" name="loan_fine_fee" value="{{old('loan_fine_fee', $setting->loan_fine_fee)}}" required>
			<label>Loan Defaulting Fine amount.</label>
			<small>When members default their loan repayment. How much would you like to charge them as fine?</small>
		</div>
	</div>

</div>