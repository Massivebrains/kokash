@extends('app')

@section('content')

<form action="{{url('zeus/company/setting/'.$company->id)}}" method="post">


    <div class="row gutters-tiny mt-20">

        {{csrf_field()}}

        <div class="col-lg-12">

            <div class="block mb-20">

                <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">

                    <li class="nav-item">
                        <a class="nav-link active" href="#company">Company</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#savings">Savings</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#loans">Loans</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#payments">Payments</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#defaults">Defaults</a>
                    </li>


                </ul>

                <div class="block-content tab-content">

                    <div class="tab-pane active" id="company" role="tabpanel">
                        <h4 class="font-w400">Company</h4>
                        @include('zeus.companies.settings.company')
                    </div>

                    <div class="tab-pane" id="savings" role="tabpanel">
                        <h4 class="font-w400">Savings</h4>

                        @include('zeus.companies.settings.savings')

                    </div>

                    <div class="tab-pane" id="loans" role="tabpanel">
                        <h4 class="font-w400">Loans</h4>
                        
                        @include('zeus.companies.settings.loans')

                    </div>

                    <div class="tab-pane" id="payments" role="tabpanel">
                        <h4 class="font-w400">Payments</h4>

                        @include('zeus.companies.settings.payments')

                    </div>

                    <div class="tab-pane" id="defaults" role="tabpanel">
                        <h4 class="font-w400">Defaults</h4>
                        
                        @include('zeus.companies.settings.defaults')
                    </div>

                </div>


                <div class="container mt-20">
                    <div class="col-md-12">
                        <input type="hidden" name="id" value="{{$setting->company_id}}">
                        <button type="submit" class="btn btn-alt-primary">Save All</button>
                    </div>
                    <div class="col-12" style="height:30px;"></div>
                </div>

            </div>

        </div>

    </div>

</form>

@endsection