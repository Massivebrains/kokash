@extends('app')

@section('content')


    <div class="row gutters-tiny js-appear-enabled animated fadeIn" data-toggle="appear">

        <div class="col-6 col-xl-6">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <i class="si si-wallet fa-3x text-body-bg-dark"></i>
                    </div>
                    <div class="font-size-h3 font-w600 js-count-to-enabled">{{ _c($savings_month) }}</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">SAVINGS THIS MONTH</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-xl-6">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <i class="si si-wallet fa-3x text-body-bg-dark"></i>
                    </div>
                    <div class="font-size-h3 font-w600">{{ _c($savings) }}</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">SAVINGS EVER</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-xl-6">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <i class="si si-wallet fa-3x text-body-bg-dark"></i>
                    </div>
                    <div class="font-size-h3 font-w600 js-count-to-enabled">{{ _c($loans_month) }}</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">LOANS THIS MONTH</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-xl-6">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <i class="si si-wallet fa-3x text-body-bg-dark"></i>
                    </div>
                    <div class="font-size-h3 font-w600 js-count-to-enabled">{{ _c($loans) }}</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">LOANS EVER</div>
                </div>
            </a>
        </div>

    </div>

    <div class="row gutters-tiny js-appear-enabled animated fadeIn" data-toggle="appear">

        <h3 class="content-heading">Recent Transactions</h3>

        <table class="table table-striped bg-white table-vcenter">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Company</th>
                    <th>Amount</th>
                    <th>Type</th>
                    <th>Source</th>
                    <th>Member</th>
                    <th>Balance</th>
                    <th>Reference</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($transactions as $row)
                    <tr>
                        <td>{{ _d($row->created_at) }}</td>
                        <td>{{ $row->company->name }}</td>
                        <td>{{ _c($row->amount) }}</td>
                        <td>{{ _badge($row->transaction_type->name) }}</td>
                        <td>{{ _badge($row->payment_source->name) }}</td>
                        <td>{{ $row->user->name }}</td>
                        <td>{{ _c($row->running_balance) }}</td>
                        <td>{{ $row->reference }}</td>
                    </tr>
                @endforeach

            </tbody>
        </table>

    </div>

@endsection
