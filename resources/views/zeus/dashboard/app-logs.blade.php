@extends('app')

@section('content')

<div class="row">
	<div class="col-6 col-xl-6">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-left mt-10 d-none d-sm-block">
                    <i class="fa fa-commenting fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h3 font-w600 js-count-to-enabled">
                	{{\App\Job::where(['type' => 'sms', 'status' => 'pending'])->count()}}
                </div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">Pending Messages</div>
            </div>
        </a>
    </div>

    <div class="col-6 col-xl-6">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-left mt-10 d-none d-sm-block">
                    <i class="fa fa-comment fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h3 font-w600 js-count-to-enabled">
                	{{\App\Job::where(['type' => 'sms', 'status' => 'completed'])->count()}}
                </div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">Sent Messages</div>
            </div>
        </a>
    </div>

</div>

<div class="block">
	<div class="block-header block-header-default">
		<h3 class="block-title">Kokash Support App - Logs</h3>
	</div>
	<div class="block-content">

		<table class="table table-striped table-vcenter">
			<thead>
				<tr>
					<th>ID</th>
					<th>Log</th>
					<th>Created At</th>
				</tr>
			</thead>
			<tbody>

				@foreach($logs as $row)
				<tr>
					<td>{{$row->id}}</td>
					<td>
						@if(strpos($row->log, 'FAILED'))
						<span class='text-danger'>{{$row->log}}</span>
						@else
						<span>{{$row->log}}</span>
						@endif
					</td>
					<td style="width:180px;">{{_d($row->created_at)}}</td>
				</tr>
				@endforeach

			</tbody>
		</table>
	</div>
</div>

@endsection