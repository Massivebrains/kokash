@extends('app')

@section('content')

<div class="block">
	<div class="block-header block-header-default">
		<h3 class="block-title">Transaction Types</h3>
		<div class="block-options">
			<div class="block-options-item">
				<a href="{{url('zeus/transaction-type/0')}}" class="btn btn-primary btn-sm">New Transaction Type</a>
			</div>
		</div>
	</div>
	<div class="block-content">

		<table class="table table-striped table-vcenter">
			<thead>
				<tr>
					<th>Name</th>
					<th>Status</th>
					<th class="text-center" style="width: 100px;">Actions</th>
				</tr>
			</thead>
			<tbody>

				@foreach($transaction_types as $row)
				<tr>
					<td>{{$row->name}}</td>
					<td>{{_badge($row->status)}}</td>
					<td class="text-center">
						<div class="btn-group">
							<a href="{{url('zeus/transaction-type/'.$row->id)}}" class="btn btn-sm btn-secondary js-tooltip-enabled">
								<i class="fa fa-pencil"></i>
							</a>
						</div>
					</td>
				</tr>
				@endforeach

			</tbody>
		</table>
	</div>
</div>

@endsection