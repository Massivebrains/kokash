@extends('app')

@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Transaction Types</h3>
                <div class="block-options">

                </div>
            </div>
            <div class="block-content">

                <form action="{{url('zeus/save-transaction-type')}}" method="post">

                    {{csrf_field()}}

                    <div class="row">

                       <div class="form-group col-md-6 {{$errors->has('name') ? 'is-invalid' : ''}}">
                        <div class="form-material">
                            <input type="text" class="form-control" name="name" value="{{old('name', $transaction_type->name)}}" required>
                            <label>Name</label>
                        </div>
                        @if($errors->has('name'))
                        <div class="invalid-feedback">{{$errors->first('name')}}</div>
                        @endif
                    </div>

                    <div class="form-group col-md-6">
                        <div class="form-material ">
                            <select class="select2 form-control" name="status" required>
                                <option value="active" {{$transaction_type->status == 'active' ? 'selected' : ''}}>Active</option>
                                <option value="inactive" {{$transaction_type->status == 'inactive' ? 'selected' : ''}}>Inactive</option>
                            </select>
                            <label>Status</label>
                        </div>
                    </div>

                </div>               

                <input type="hidden" name="id" value="{{$transaction_type->id}}">
                <div class="form-group row">
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-alt-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
</div>
@endsection