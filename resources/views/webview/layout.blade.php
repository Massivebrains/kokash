<!DOCTYPE html>
<html>
<head>
    <title>Kokash WebView</title>
    <link rel="stylesheet" id="css-main" href="{{asset('/css/codebase.min.css')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    @yield('styles')

    <style type="text/css">

        .btn{

            margin-bottom:20px;
            cursor: pointer;
            background : #1c3d5a;
            color : #fff;
            font-weight: 'normal';
            border-color : #1c3d5a;
            height : 45px;
        }

        .btn-light{

            background:transparent; 
            color:#1c3d5a;
        }

        .form-group > label{

            font-size: 20px !important;
        }

        .form-group > small{

            font-size:70%;
        }

        h1, h2, h3, h4, h5, h6{

            color: #1c3d5a;
        }
    </style>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-12" style="margin-top:30px;">

                @yield('content')

            </div>
        </div>
    </div>

</body>

@yield('scripts')
</html>