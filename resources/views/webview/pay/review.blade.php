@extends('webview.layout')

@section('content')

<form action="{{url('webview/confirm')}}" method="POST" role="form">

	{{csrf_field()}}

	<table class="table table-bordered">
		<tbody>
			@foreach($order->order_details as $row)
			<tr>
				<td>
					@if($row->type == 'savings')
					{{ucwords($row->type).' '.$row->user->name}}
					@else
					{{$row->loan->name.' '.$row->user->name}}
					@endif
				</td>
				<td>{{_c($row->amount)}}</td>
			</tr>
			@endforeach
			<tr>
				<th><h4>Grand Total</h4></th>
				<th>
					<h4>{{_c($order->total)}}</h4>
				</th>
			</tr>
		</tbody>
	</table>

	@if($order->status == 'cart')
	<input type="hidden" name="order_id" value="{{$order->id}}">
	<button type="submit" id="submit" class="btn btn-block btn-lg">Confirm</button>
	<a href="{{url('webview/pay?api_token='.$order->user->api_token)}}" id="go_back" class="btn btn-block btn-lg btn-light" style="margin-top:40px">Go Back</a>
	@else
	<p>
		Your request has been saved. use the reference number below as a description while making payment via nay channel such as Bank Transfer <br>
		Once you make payment, we would use this reference number to complete your transaction. After that, you will get an SMS confirmation from us.
	</p>

	<h1 class="text-center">
		{{$order->order_reference}}
	</h1>
	
	<a href="{{url('webview/pay?api_token='.$order->user->api_token)}}" class="btn btn-block btn-lg btn-light">Ok Got it!</a>
	@endif

</form>

<script type="text/javascript">
	
	@if($order->status == 'cart')
	
	document.getElementById('submit').addEventListener('click', () => {

		document.getElementById('submit').innerHTML = 'Loading...';

		setTimeout(() => {

			document.getElementById('submit').innerHTML = 'Confirm';

		}, 1000 * 5)
	});

	document.getElementById('go_back').addEventListener('click', () => {

		document.getElementById('go_back').innerHTML = 'Loading...';
	});

	@endif

	function close(){

		window.postMessage(true, '*');
	}

</script>
@endsection