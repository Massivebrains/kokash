@extends('webview.layout')

@section('content')

<form action="{{url('webview/review')}}" method="POST" role="form">

    {{csrf_field()}}

    @foreach($products as $row)

    <div class="form-group">
        <label>{{$row->label}}</label>
        <input type="number" name="products[{{$row->id}}]" class="form-control form-control-lg" placeholder="Enter Amount">
        <small>{{$row->notes}}</small>
    </div>

    @endforeach

    <input type="hidden" name="order_id" value="{{collect($products)->first()->order_id}}">

    <button type="submit" class="btn btn-block btn-lg" id="submit">
        Continue to Review <i class="fa fa fa-chevron-circle-right"></i>
    </button>

</form>

<script type="text/javascript">

    document.getElementById('submit').addEventListener('click', () => {

        document.getElementById('submit').innerHTML = 'Loading...';

        setTimeout(() => {

            document.getElementById('submit').innerHTML = 'Continue to Review <i class="fa fa fa-chevron-circle-right"></i>';
            
        }, 1000 * 5)
        
    })
</script>
@endsection