<div class="modal fade" id="disburse-offline" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modal-slideright" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-slideright" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Confirm Disbursement of {{_c($loan->principal)}} to {{$loan->user->name}} - OFFLINE</h3>
                </div>
                <div class="block-content">
                    <div class="table-responsive">


                       <div class="form-group row">

                        <div class="col-lg-12">
                            <form action="{{url('disburse-offline')}}" method="post">

                                {{csrf_field()}}

                                <div class="form-group row {{$errors->has('disbursed_at') ? 'is-invalid' : ''}}">
                                    <label class="col-lg-3 col-form-label">Disbursement Date</label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control date" name="disbursed_at" placeholder="Date Disbursed" value="{{old('disbursed_at')}}" autocomplete="off" required>
                                        @if($errors->has('disbursed_at'))
                                        <div class="invalid-feedback">{{$errors->first('disbursed_at')}}</div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row {{$errors->has('disbursement_notes') ? 'is-invalid' : ''}}">
                                    <label class="col-lg-3 col-form-label" for="example-hf-password">Disbursement Notes</label>
                                    <div class="col-lg-7">
                                        <textarea name="disbursement_notes" class="form-control" rows="5">{{old('disbursement_notes', 'Disbursed')}}</textarea>
                                        @if($errors->has('disbursement_notes'))
                                        <div class="invalid-feedback">{{$errors->first('disbursement_notes')}}</div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row {{$errors->has('password') ? 'is-invalid' : ''}}">
                                    <label class="col-lg-3 col-form-label">Your Account Password</label>
                                    <div class="col-lg-7">
                                        <input type="password" class="form-control" name="password" placeholder="Your account Password" required>
                                        @if($errors->has('password'))
                                        <div class="invalid-feedback">{{$errors->first('password')}}</div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-9 ml-auto">
                                        <input type="hidden" name="id" value={{$loan->id}}>
                                        <button type="submit" class="btn btn-alt-primary">Mark Loan as Disbursed</button>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>

@section('script')
<script type="text/javascript">
    
    @if($errors->has('disbursed_at') || $errors->has('disbursement_notes') || $errors->has('password'))
        $('#disburse-offline').modal('toggle');
    @endif
</script>
@endsection