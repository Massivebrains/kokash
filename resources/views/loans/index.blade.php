@extends('app')

@section('content')

    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Loans</h3>

            @can('update', \App\Loan::class)
                <div class="block-options">
                    <div class="block-options-item">
                        <a href="{{ url('loan-form') }}" class="btn btn-alt-primary btn-sm"><i class="si si-plus"></i> New Loan</a>
                        <a href="{{ url('bulk-loans') }}" class="btn btn-alt-primary btn-sm"><i class="si si-plus"></i> Bulk Repayment</a>
                        <a href="/upload-bulk-loans" class="btn btn-alt-primary btn-sm"><i class="si si-plus"></i> Bulk Upload Repayment</a>
                    </div>
                </div>
                @endif

            </div>
            <div class="block-content">

                @component('components.search', ['placeholder' => 'Search Loans by Name, Reference, Principal and Status.', 'url' => 'loans', 'count' => $count ?? 0, 'query' => $query ?? null]) @endcomponent

                <table class="table table-striped table-vcenter">
                    <thead>
                        <tr>
                            <th>Member</th>
                            <th>Name</th>
                            <th>Principal</th>
                            <th>Paid</th>
                            <th>Outstanding</th>
                            <th>Status</th>
                            <th>Disbursed</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($loans as $row)
                            <tr>
                                <td><a href="{{ url('member-info/' . $row->user->id) }}">{{ $row->user->name }}</a></td>
                                <td>{{ $row->name }}<br><small>{{ $row->reference }}</small></td>
                                <td>{{ _c($row->principal) }}<br><small>{{ _percent($row->rate) }} / {{ _months($row->time) }}</small></td>
                                <td>{{ _c(
                                    $row->loan_transactions()->whereIn('status', ['completed', 'partly_paid'])->sum('paid'),
                                ) }}</td>
                                <td>{{ _c($row->outstanding) }}</td>
                                <td>{{ _badge($row->status) }}</td>
                                <td>{{ _d($row->disbursed_at, false) }}<br><small>Started {{ _month($row->start_at) }}</small></td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ url('loan/' . $row->id) }}" class="btn btn-sm btn-secondary js-tooltip-enabled">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>

                {{ $loans->links() }}
            </div>
        </div>

    @endsection
