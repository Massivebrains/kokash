@extends('app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">{{ $loan_category->name }} - {{ $user->name }} | <strong class="text-danger">{{ $repayment_type == 'deduct_interest' ? 'Deducting Interest from Principal' : 'Adding Interest to Principal' }}</strong></h3>
                    <div class="block-options">

                    </div>
                </div>
                <div class="block-content">

                    <form action="{{ url('save-loan') }}" method="post" id="form">

                        {{ csrf_field() }}

                        <div class="row">

                            <div class="col-8">
                                <table class="table table-striped table-sm table-bordered table-vcenter">
                                    <thead>
                                        <tr>
                                            <th>Month</th>
                                            <th>Principal</th>
                                            <th>Interest</th>
                                            <th>Payable</th>
                                            <th>Running Balance</th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                        @php
                                            $sum = $loan_transactions->sum('amount');
                                        @endphp

                                        @foreach ($loan_transactions->chunk(12) as $transactions)

                                            @foreach ($transactions as $row)

                                                @php
                                                    $sum -= $row->amount;
                                                @endphp

                                                <tr>
                                                    <th>{{ $row->date->format('M Y') }}</th>
                                                    <td>{{ _c($row->principal) }}</td>
                                                    <td>{{ _c($row->interest) }}</td>
                                                    <td>{{ _c($row->amount) }}</td>
                                                    <td>{{ _c($sum) }}</td>


                                                </tr>

                                            @endforeach

                                            <tr>
                                                <td colspan="5">&nbsp;</td>
                                            </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-4">

                                <a class="block block-link-pop block-rounded block-bordered text-center" href="javascript:void(0)">
                                    <div class="block-header">
                                        <h3 class="block-title"><strong>{{ $loan_category->name }}</strong> <small>- {{ $user->name }}</small></h3>
                                    </div>
                                    <div class="block-content bg-gray-lighter">
                                        <div class="h1 font-w700 mb-10">{{ _c($principal) }}</div>
                                        <div class="h5 text-muted">{{ $time * 12 }} Months at {{ _percent($rate) }}</div>
                                    </div>
                                    <div class="block-content">
                                        <h3 class="text-muted">INTEREST</h3>
                                        <h1 class="text-danger"><strong>{{ _c(collect($loan_transactions)->sum('interest')) }}</strong></h1>
                                    </div>

                                    <div class="block-content block-content-full bg-gray-lighter">
                                        <span class="btn btn-hero btn-lg btn-rounded btn-noborder btn-success" onclick="$('#form').submit()">CONFIRM LOAN</span>
                                        <span class="btn btn-hero btn-sm btn-rounded btn-noborder btn-default" onclick="window.location = '{{ url('loan-form') }}'">CANCEL</span>
                                    </div>
                                </a>

                            </div>
                        </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-9">
                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                        <input type="hidden" name="loan_category_id" value="{{ $loan_category->id }}">
                        <input type="hidden" name="principal" value="{{ $principal }}">
                        <input type="hidden" name="rate" value="{{ $rate }}">
                        <input type="hidden" name="time" value="{{ $time }}">
                        <input type="hidden" name="start_at" value="{{ $start_at }}">
                        <input type="hidden" name="repayment_type" value="{{ $repayment_type }}">
                        <input type="hidden" name="auto_disburse" value="{{ $auto_disburse }}">
                    </div>
                </div>
                </form>
            </div>
        </div>

    </div>
    </div>
@endsection
