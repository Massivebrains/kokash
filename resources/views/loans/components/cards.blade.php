<div class="row gutters-tiny mb-20">

    <div class="col-md-6 col-xl-3">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-left mt-10">
                    <i class="si si-calculator fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h5 font-w600">{{ _c($loan->principal) }}</div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">Principal</div>
            </div>
        </a>
    </div>
    <div class="col-md-6 col-xl-3">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-left mt-10">
                    <i class="si si-graph fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h5 font-w600">{{ _percent($loan->rate) }} ~ {{ number_format($loan->time, 0) }} Month(s)</div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">Rate/Time</div>
            </div>
        </a>
    </div>
    <div class="col-md-6 col-xl-3">
        <a class="block block-link-shadow text-left" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-right mt-10">
                    <i class="si si-calculator fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h5 font-w600">{{ _c($loan->disbursed) }}</div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">Amount Disbursed</div>
            </div>
        </a>
    </div>
    <div class="col-md-6 col-xl-3">
        <a class="block block-link-shadow text-left" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-right mt-10">
                    <i class="si si-wallet fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h5 font-w600">{{ _c($loan->interest) }}</div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">Interest</div>
            </div>
        </a>
    </div>

</div>

<div class="row gutters-tiny mb-20">

    <div class="col-md-6 col-xl-3">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-left mt-10">
                    <i class="si si-calendar fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h5 font-w600">{{ _d($loan->disbursed_at, false) }}</div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">Disbursement Date</div>
            </div>
        </a>
    </div>
    <div class="col-md-6 col-xl-3">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-left mt-10">
                    <i class="si si-power fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h5 font-w600">{{ _c($loan->paid) }}</div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">Paid So Far</div>
            </div>
        </a>
    </div>
    <div class="col-md-6 col-xl-3">
        <a class="block block-link-shadow text-left" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-right mt-10">
                    <i class="si si-wallet fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h5 font-w600">{{ _c($loan->outstanding) }}</div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">Outstanding</div>
            </div>
        </a>
    </div>
    <div class="col-md-6 col-xl-3">
        <a class="block block-link-shadow text-left" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-right mt-10">
                    <i class="si si-briefcase fa-3x text-body-bg-dark"></i>
                </div>
                <div class="font-size-h5 font-w600">{{ _d($loan->created_at, false) }}</div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">Date Created</div>
            </div>
        </a>
    </div>

</div>
