<div class="col-md-12">

    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Pay Loan {{ $loan->name }}</h3>
        </div>
        <div class="block-content">

            <form action="{{ url('complete-loan-transaction') }}" method="post">

                {{ csrf_field() }}

                <div class="row">

                    <div class="form-group col-md-12 {{ $errors->has('payment_source_id') ? 'is-invalid' : '' }}">
                        <label>Payment Source</label>
                        <select class="select2 form-control country" name="payment_source_id" required>
                            <option>--select--</option>
                            @foreach ($sources as $row)
                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('payment_source_id'))
                            <div class="invalid-feedback">{{ $errors->first('payment_source_id') }}</div>
                        @endif
                    </div>

                    <div class="form-group col-md-12">
                        <label>Amount Paid</label>
                        <input type="number" step="0.001" min="0.001" class="form-control" name="amount" value="{{ old('amount') }}" required placeholder="Maximum Amount is {{ $loan->outstanding }}">
                    </div>

                </div>

                <div class="row">

                    <div class="form-group col-md-12 {{ $errors->has('description') ? 'is-invalid' : '' }}">
                        <label>Description</label>
                        <textarea type="text" class="form-control" name="description">{{ old('description') }}</textarea>

                        @if ($errors->has('description'))
                            <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                        @endif
                    </div>

                </div>

                <div class="row">

                    <div class="form-group col-md-12 {{ $errors->has('password') ? 'is-invalid' : '' }}">
                        <label>Your Account Password</label>
                        <input type="password" class="form-control" name="password" required>
                        @if ($errors->has('password'))
                            <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                        @endif
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-md-9">
                        <input type="hidden" name="loan_id" value="{{ $loan->id }}">
                        <button type="submit" class="btn btn-alt-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
