<div class="col-md-12">

    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Loan - {{$loan->name}} (View <a href="{{url('member-info/'.$loan->user->id)}}">{{$loan->user->name}}</a>'s Profile)</h3>
            <div class="block-options">

                @if($loan->status == 'pending')
                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-alt-primary dropdown-toggle" id="btnGroupVerticalDrop3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Disburse Funds</button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop1" x-placement="top-start">
                        <a class="dropdown-item" data-toggle="modal" href='#disburse-offline'>
                           Mark as Disbursed
                       </a>
                       <a class="dropdown-item" href="javascript:void(0)">
                           Transfer Funds
                       </a>
                   </div>
               </div>
               @endif

               @can('update', \App\Transaction::class)
               @if($loan->status != 'completed')
               <a href="{{url('loan/'.$loan->id)}}?form=true" class="btn btn-alt-primary btn-sm"><i class="si si-wallet"></i> Pay Loan</a>
               @endif
               @endcan
           </div>
       </div>
       <div class="block-content">

        <div class="row">

            <div class="col-6">

            </div>
            <div class="col-12">

                <table class="table table-striped table-sm table-bordered table-vcenter">
                    <thead>
                        <tr>
                            <th>Reference</th>
                            <th>Month</th>
                            <th>Payable</th>
                            <th>Paid</th>
                            <th>Outstanding</th>
                            <th>Running-B.</th>
                            <th>Status</th>
                            <th>Payment-Date</th>
                            <th>Source</th>
                        </tr>
                    </thead>

                    <tbody>

                        @foreach($loan->loan_transactions as $row)
                        <tr>
                            <td>{{$row->reference}}</td>
                            <td>{{_month($row->date)}}</td>
                            <td>{{_c($row->amount)}}</td>
                            <td>{{_c($row->amount - $row->outstanding)}}</td>
                            <td>{{_c($row->outstanding)}}</td>
                            <td>{{_c($row->balance)}}</td>
                            <td>
                                @if($row->fine_transaction_id > 0)
                                {{_badge('defaulted')}}
                                @else
                                {{_badge($row->status)}}
                                @endif
                            </td>
                            <td>{{_d($row->payment_date)}}</td>
                            <td>{{_badge(optional($row->payment_source)->name)}}</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
</div>