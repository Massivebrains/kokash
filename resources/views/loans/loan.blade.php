@extends('app')

@section('content')

@include('loans.components.cards')

<div class="row">

    @if($form)

    @include('loans.components.pay')

    @else

    @include('loans.components.details')

    @endif

</div>

@if($loan_transaction->id < 1 && $loan->status == 'pending')
@include('loans.disburse-offline-modal')
@endif

@endsection