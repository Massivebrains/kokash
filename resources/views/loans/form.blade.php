@extends('app')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">New Loan</h3>
                    <div class="block-options">

                    </div>
                </div>
                <div class="block-content">

                    <form action="{{ url('review-loan') }}" method="post" class="form" autocomplete="off">

                        {{ csrf_field() }}

                        <div class="row">

                            <div class="form-group col-md-6">

                                <label>Member</label>
                                <select class="select2 form-control country" name="user_id" required>
                                    <option>--select--</option>
                                    @foreach ($users as $row)
                                        @if (old('user_id') == $row->id)
                                            <option value="{{ $row->id }}" selected>{{ $row->name }}</option>
                                        @else
                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                        @endif
                                    @endforeach
                                </select>

                                @if ($errors->has('user_id'))
                                    <div class="invalid-feedback">{{ $errors->first('user_id') }}</div>
                                @endif
                            </div>

                            <div class="form-group col-md-6 {{ $errors->has('name') ? 'is-invalid' : '' }}">
                                <label>Loan Category</label>
                                <select name="loan_category_id" class="form-control select2" required>
                                    @foreach ($loan_categories as $row)
                                        <option value="{{ $row->id }}" {{ $row->id == 4 ? 'selected' : '' }}>{{ $row->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('loan_category_id'))
                                    <div class="invalid-feedback">{{ $errors->first('loan_category_id') }}</div>
                                @endif
                            </div>

                            <div class="form-group col-md-6">

                                <label>Repayment Type</label>
                                <select class="select2 form-control" name="repayment_type" required>
                                    <option value="deduct_interest" selected>Deduct Interest from Principal</option>
                                    <option value="add_interest">Add Interest to Principal</option>
                                </select>

                                @if ($errors->has('user_id'))
                                    <div class="invalid-feedback">{{ $errors->first('user_id') }}</div>
                                @endif
                            </div>

                            <div class="form-group col-md-6 {{ $errors->has('principal') ? 'is-invalid' : '' }}">

                                <label>Principal (Amount)</label>
                                <input type="number" class="form-control" name="principal" value="{{ old('principal') }}" required>

                                @if ($errors->has('principal'))
                                    <div class="invalid-feedback">{{ $errors->first('principal') }}</div>
                                @endif
                            </div>

                        </div>

                        <div class="row">

                            <div class="form-group col-md-6 {{ $errors->has('rate') ? 'is-invalid' : '' }}">

                                <label>Rate (%)</label>
                                <input type="number" class="form-control" name="rate" value="{{ old('rate', 10) }}" step="0.01" required>

                                @if ($errors->has('rate'))
                                    <div class="invalid-feedback">{{ $errors->first('rate') }}</div>
                                @endif
                            </div>

                            <div class="form-group col-md-6 {{ $errors->has('time') ? 'is-invalid' : '' }}">

                                <label>Time (Months)</label>
                                <input type="number" class="form-control" name="time" value="{{ old('time', 6) }}" required>

                                @if ($errors->has('time'))
                                    <div class="invalid-feedback">{{ $errors->first('time') }}</div>
                                @endif
                            </div>



                        </div>

                        <div class="row">

                            <div class="form-group col-md-6 {{ $errors->has('start_at') ? 'is-invalid' : '' }}">

                                <label>Start Month</label>
                                <input type="text" class="form-control month" name="start_at" value="{{ old('start_at', date('m-Y')) }}" autocomplete="nope" required>

                                @if ($errors->has('start_at'))
                                    <div class="invalid-feedback">{{ $errors->first('start_at') }}</div>
                                @endif
                            </div>

                            <div class="form-group col-md-6 {{ $errors->has('password') ? 'is-invalid' : '' }}">

                                <label>Your Account Password</label>
                                <input type="password" class="form-control" name="password" autocomplete="new-password" required>

                                @if ($errors->has('password'))
                                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                                @endif
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-alt-primary">Continue</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection
