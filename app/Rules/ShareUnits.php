<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\User;
use App\Share;

class ShareUnits implements Rule
{
    public $total_units = 0;

    public function __construct()
    {
        //
    }

    public function passes($attribute, $value)
    {
        $user = User::find(request('user_id'));

        if(!$user)
            return false;

        $this->total_units = (int)request('units');

        $share =   Share::whereUserId($user->id)->first();

        if($share)
            $this->total_units += $share->units;

        return $this->total_units <= 50000;
    }

    public function message()
    {
        return 'Total Units per Member must not be more than 50,000. Total units cannot be '.number_format($this->total_units);
    }
}
