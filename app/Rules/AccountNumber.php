<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Bank;

class AccountNumber implements Rule
{

    public function __construct()
    {

    }


    public function passes($attribute, $value)
    {
        $bank = Bank::find(request('bank_id'));

        if(!$bank)
            return false;

        if(!$this->isNubanValid($bank->code.$value)){

            session()->flash('error', 'Invalid Account Number.');
            return false;
        }

        return true;
    }

    public function message()
    {
        return 'Invalid Account Number';
    }

    public function isNubanValid($input = '')
    {

        $nuban = str_split($input);

        $total = ($nuban[0] * 3) + ($nuban[1] * 7) + ($nuban[2] * 3) + ($nuban[3] * 3) + ($nuban[4] * 7) + ($nuban[5] * 3) + ($nuban[6] * 3) + ($nuban[7] * 7) + ($nuban[8] * 3) + ($nuban[9] * 3) + ($nuban[10] * 7) + ($nuban[11] * 3);

        $check_digit = (10 - ($total % 10)) == 10 ? 0 : (10 - ($total % 10));

        if($check_digit != $nuban[12]) {
            return false;
        }

        return true;
    }
}
