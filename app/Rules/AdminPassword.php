<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Auth;

class AdminPassword implements Rule
{

    public function __construct()
    {
        //
    }

 
    public function passes($attribute, $value)
    {
        return app('hash')->check(request('password'), Auth::user()->password);
    }


    public function message()
    {
        return 'Your password is not correct.';
    }
}
