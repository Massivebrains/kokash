<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Events\UserCreated;
use App\DebitCard;
use App\Traits\Scopes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use Scopes;

    protected $hidden   = ['updated_at', 'deleted_at', 'password', 'remember_token'];
    protected $guarded  = ['updated_at'];
    protected $dates  	= ['created_at', 'updated_at'];
    protected $appends 	= ['name'];

    protected $dispatchesEvents = [

        'created' => UserCreated::class
    ];

    public function getNameAttribute()
    {
        return ucwords($this->first_name.' '.$this->last_name);
    }

    public function company()
    {
        return $this->belongsTo('App\Company')->withDefault(function () {
            return new \App\Company();
        });
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    public function beneficiaries()
    {
        return $this->hasMany('App\Beneficiary');
    }

    public function savings_account()
    {
        return $this->hasOne('App\SavingsAccount')->withDefault(function () {
            return new \App\SavingsAccount();
        });
    }

    public function loans()
    {
        return $this->hasMany('App\Loan');
    }

    public function debitcards()
    {
        return $this->hasMany('App\DebitCard');
    }

    public function share()
    {
        return $this->hasOne('App\Share')->withDefault();
    }

    public static function profile($user = null)
    {
        $user->first_name           = strtoupper($user->first_name);
        $user->last_name            = strtoupper($user->last_name);
        $user->status               = strtoupper($user->status);
        $user->company_name         = strtoupper($user->company->description);
        $user->bank_name            = Bank::firstOrNew(['id' => $user->savings_account->bank_id])->name;
        $user->bank_account_number  = $user->savings_account->bank_account_number;
        $user->bank_account_name    = strtoupper($user->savings_account->bank_account_name);
        $user->debitcards           = DebitCard::where(['user_id' => $user->id, 'reusable' => 1])->get();

        $partners = [];

        foreach ($user->beneficiaries as $row) {
            $partners[] = User::firstOrNew(['id' => $row->partner_id]);
        }

        $user->partners  = $partners;

        return $user;
    }

    public static function search($search = null, $company_id = 0, $type = 'user')
    {
        $phone = (int)$search > 1000 ? _to_phone($search) : $search;

        return self::where(['company_id' => $company_id])

        ->where('type', $type)

        ->where(function ($query) use ($search, $phone) {
            $query->where('first_name', 'like', "%$search%")
            ->orWhere('last_name', 'like', "%$search%")
            ->orWhere('email', 'like', "%$search%")
            ->orWhere('phone', 'like', "$phone")
            ->orWhere('sex', 'like', "$search")
            ->orWhere('marital_status', 'like', "$search")
            ->orWhere('address', 'like', "%$search%")
            ->orWhere('occupation', 'like', "%$search%")
            ->orWhere('employer', 'like', "%$search%");
        })

        ->paginate(50);
    }

    public static function getByName($company_id, $name)
    {
        $max_user = self::orderBy('reference', 'desc')->first();
        $query = User::where(['company_id' => $company_id]);

        $names = explode(' ', strtoupper($name));

        if (count($names) == 1) {
            $user = self::create([
                'company_id'        => $company_id,
                'reference'         => (int)$max_user->reference++,
                'first_name'		=> $names[0],
                'last_name'			=> $names[0],
                'email'				=> '',
                'phone'				=> null,
                'type'				=> 'user',
                'api_token'			=> str_random(16),
                'status'			=> 'active'
            ]);

            return $user;
        }

        $user = self::whereCompanyId($company_id)->where('first_name', $names[0])->where('last_name', $names[1])->first();
        if ($user) {
            return $user;
        }

        $user = self::whereCompanyId($company_id)->where('last_name', $names[0])->where('first_name', $names[1])->first();
        if ($user) {
            return $user;
        }

        $user = self::create([
            'company_id'        => $company_id,
            'reference'         => (int)$max_user->reference++,
            'first_name'		=> $names[0],
            'last_name'			=> $names[1],
            'email'				=> '',
            'phone'				=> null,
            'type'				=> 'user',
            'api_token'			=> str_random(16),
            'status'			=> 'inactive'
        ]);

        return $user;
    }
}
