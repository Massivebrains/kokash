<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\LoanTransaction;

class LoanTransactionSaved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $loan_transaction;

    public function __construct(LoanTransaction $loan_transaction)
    {
        $this->loan_transaction = $loan_transaction;
    }


    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
