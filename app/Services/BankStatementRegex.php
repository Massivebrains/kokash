<?php

namespace App\Services;

use Illuminate\Support\Str;
use App\User;
use Spatie\Regex\Regex;

class BankStatementRegex
{
	private $query = '';
	private $users;

	public function __construct($query = '', $users = 0)
	{
		$this->query = $query;
		$this->users = $users;
	}

	public function search()
	{
		foreach($this->users as $row){

			$names = $this->getNamesCombinations($row->name);

			foreach($names as $name){

				$pattern = "/\W*((?i)$name[0](?-i))\W*\W*((?i)$name[1](?-i))\W*/i";
				
				if(Regex::match($pattern, $this->query)->hasMatch())
					return User::find($row->id);
			}

		}

		return null;
	}

	private function getNamesCombinations($name)
	{
		$names 	= explode(' ', $name);
		$all 	= [];

		for($i = 0; $i < count($names); $i++){

			for($j = 0; $j < count($names); $j++){

				if($i != $j)
					$all[] = [$names[$i], $names[$j]];
			}
		}

		return $all;
	}
}