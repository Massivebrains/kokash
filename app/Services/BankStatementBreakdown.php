<?php

namespace App\Services;
use App\BankStatement;
use App\Transaction;
use App\User;

class BankStatementBreakdown
{
	public $statement;
	public $save;
	public $loan;
	public $loans;
	public $total;
	public $user;

	public function __construct(BankStatement $statement)
	{
		$this->statement = $statement;
		$this->user 	 = $statement->possible_user;
		$this->total 	 = $statement->amount;
		$this->loans 	 = $this->user->loans()->whereStatus('running')->get();
	}

	public function getBreakdown()
	{
		$this->save = $this->getSavingsAmount();
		$this->total -= $this->save;

		$this->loan = $this->getLoan();

		if($this->loan->amount == 0){

			$this->save += $this->total;
			$this->total = 0;
		}

		$loans = [];

		foreach($this->loans as $row){

			$loan = [

				'id'		=> $row->id,
				'name'		=> $row->name.' (Bal: '._c($row->outstanding).')',
				'amount'	=> 0
			];

			if($this->loan->loan_id == $row->id)
				$loan['amount'] = $this->loan->amount;

			$loans[] = $loan;
		}

		if($this->total > 0)
			$this->save += $this->total;

		return (object)[

			'save'			=> (float)$this->save,
			'share'			=> 0,
			'registration' 	=> 0,
			'loans'			=> $loans
		];
	}

	private function getSavingsAmount()
	{
		$transaction = Transaction::whereUserId($this->user->id)
		->where('amount', '>', 0)
		->orderBy('id', 'desc')
		->first();

		if($transaction && $this->total >= $transaction->amount)
			return $transaction->amount;

		return $this->total;
	}

	private function getLoan()
	{
		$loan = $this->loans->first();

		if($loan){

			if((float)$loan->outstanding > $this->total){

				$total 			=  $this->total;
				$this->total 	= 0;

				return (object)['loan_id' => $loan->id, 'name' => $loan->name, 'amount' => $total];
			}

			$this->total -= $loan->outstanding;

			return (object)['loan_id' => $loan->id, 'name' => $loan->name, 'amount' => $loan->outstanding];
		}

		return (object)['loan_id' => 0, 'name' => '', 'amount' => 0];
	}
}