<?php 

namespace App\Services;
use App\BankStatement;
use App\Transaction;
use App\User;
use App\LoanCategory;
use DB;

class BankStatementReport
{
	public $company_id = 0;
	public $year = 0;
	public $month = 0;
	public $report = [];

	public function __construct($company_id = 0, $year = 0, $month = 0)
	{
		$this->company_id 	= $company_id;
		$this->year 		= $year;
		$this->month 		= $month;
	}

	public function getReport()
	{
		$this->getReportWithoutLoans();
		$this->addLoansToReport();

		return collect($this->report);
	}

	private function getReportWithoutLoans()
	{
		$query = "
		select
		max(users.id) as id,
		concat(max(users.first_name), ' ', max(users.last_name)) as Member,
		ifnull(sum(statements.amount), 0) as BankStatementAmount,
		ifnull(sum(savings.amount), 0) as Savings,
		ifnull(sum(share.amount), 0) as Shares,
		ifnull(sum(registration.amount), 0) as Registration
		from bank_statements as statements
		join users on users.id = statements.user_id
		left join bank_statement_breakdowns as savings on savings.bank_statement_id = statements.id and savings.type = 'transaction'
		left join bank_statement_breakdowns as share on share.bank_statement_id = statements.id and share.type = 'share'
		left join bank_statement_breakdowns as registration on registration.bank_statement_id = statements.id and registration.type = 'registration'
		where year(statements.business_month) = ?
		and month(statements.business_month) = ?
		and statements.company_id = ?
		and statements.status = 'matched'
		group by statements.user_id
		order by max(users.first_name)
		";

		$this->report = collect(DB::select($query, [$this->year, $this->month,  $this->company_id]));
	}

	private function addLoansToReport()
	{
		$loan_categories = LoanCategory::whereCompanyId($this->company_id)->get();

		foreach($this->report as $row){

			foreach($loan_categories as $category)
				$row->{$category->name} = $this->getLoanCategoryAmountForUser($row->id, $category->id);
		}
	}

	private function getLoanCategoryAmountForUser($user_id = 0, $loan_category_id = 0)
	{
		$query = "
		select
		ifnull(sum(breakdowns.amount), 0) as amount
		from bank_statement_breakdowns breakdowns
		join bank_statements statements on statements.id = breakdowns.bank_statement_id
		where year(statements.business_month) = ?
		and month(statements.business_month) = ?
		and statements.company_id = ?
		and statements.status = 'matched'
		and statements.user_id = ?
		and breakdowns.type = 'loan'
		and breakdowns.loan_category_id = ?
		";

		return collect(DB::select($query, [$this->year, $this->month,  $this->company_id, $user_id, $loan_category_id]))->first()->amount;
	}
}