<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentSource extends Model
{
	use SoftDeletes;
	
    protected $hidden   = ['deleted_at'];
    protected $guarded  = ['updated_at'];
}
