<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $guarded  = ['updated_at'];
	protected $appends 	= ['kobo'];
}
