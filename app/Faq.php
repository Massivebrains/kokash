<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $hidden   = ['deleted_at'];
    protected $guarded  = ['updated_at'];
}
