<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\LoanSaved;
use App\Jobs\SendEmailJob;
use App\Jobs\SendLoanNotificationJob;
use App\Jobs\SendSMSJob;
use Illuminate\Database\Eloquent\SoftDeletes;

class Loan extends Model
{
    use SoftDeletes;

    protected $guarded  = ['updated_at'];
    protected $appends = ['paid'];

    protected $dispatchesEvents = [

        'saved' => LoanSaved::class
    ];

    public function getPaidAttribute()
    {
        $paid       = $this->loan_transactions()->whereIn('status', ['completed', 'partly_paid'])->sum('paid');
        $reveresed  = $this->loan_transactions()->whereDescription('REVERSAL')->sum('amount');

        return $paid - $reveresed;
    }

    public function user()
    {
        return $this->belongsTo('App\User')->withDefault();
    }

    public function company()
    {
        return $this->belongsTo('App\Company')->withDefault();
    }

    public function loan_category()
    {
        return $this->belongsTo('App\LoanCategory')->withDefault();
    }

    public function loan_transactions()
    {
        return $this->hasMany('App\LoanTransaction');
    }

    public function scopeCompany($query, $company_id = 0)
    {
        return $query->where(['company_id' => $company_id]);
    }

    public function scopePending($query)
    {
        return $query->where(['status' => 'pending']);
    }

    public function scopeRunning($query)
    {
        return $query->where(['status' => 'running']);
    }

    public function scopeCompleted($query)
    {
        return $query->where(['status' => 'completed']);
    }

    public function scopeCancelled($query)
    {
        return $query->where(['status' => 'cancelled']);
    }

    public function scopeBy($query, \App\User $user)
    {
        return $query->where(['user_id' => $user->id]);
    }

    public static function search($search = '', $company_id = 0)
    {
        return self::where(['company_id' => $company_id])

        ->where(function ($query) use ($search) {
            $query->where('name', 'like', "%$search%")
            ->orWhere('reference', $search)
            ->orWhere('principal', $search)
            ->orWhere('status', $search)
            ->orWhereHas('user', function ($userQuery) use ($search) {
                $userQuery->where('first_name', 'like', "%$search%")
                ->orWhere('last_name', 'like', "%$search%")
                ->orWhere('email', 'like', "%$search%")
                ->orWhere('phone', 'like', "$search");
            });
        })

        ->paginate(50);
    }

    public static function transform($loans = [])
    {
        collect($loans)->map(function ($row) {
            $paid               = LoanTransaction::where(['loan_id' => $row->id])->completed()->sum('amount');
            $last_transaction   = LoanTransaction::where(['loan_id' => $row->id])->completed()->latest()->first();

            $row->key                   = str_random(10);
            $row->principal             = _c($row->principal);
            $row->disbursed             = _c($row->disbursed);
            $row->amount                = _c($row->amount);
            $row->interest              = _c($row->interest);
            $row->paid                  = _c($paid);
            $row->outstanding           = _c(ceil($row->loan_transactions->sum('amount') - $paid));
            $row->date                  = _api_date($row->start_at);
            $row->last_payment_date     = $last_transaction ? _api_date($last_transaction->created_at) : '';
            $row->status                = ucfirst($row->status);

            unset($row->loan_transactions);
        });

        return $loans;
    }

    public static function pay(Loan $loan, $amount = 0, $description = '', $payment_source_id = 1)
    {
        if (!$loan || $loan->status != 'running') {
            return (object)['status' => false, 'message' => 'Only running loans can be updated'];
        }

        $amount_to_pay_now = $amount;

        if ($amount > $loan->outstanding) {
            return (object)['status' => false, 'message' => 'Amount is more than the outstanding on this loan'];
        }

        $source = PaymentSource::find($payment_source_id);

        if (!$source) {
            return (object)['status' => false, 'message' => 'Payment Source is Invalid'];
        }


        _log('Paid '.$amount.' on '.$loan->name, $loan->user);

        while ($amount > 0) {
            $loan_transaction = LoanTransaction::whereLoanId($loan->id)->whereIn('status', ['pending', 'partly_paid'])->first();

            if (!$loan_transaction) {
                break;
            }

            $outstanding = round($loan_transaction->outstanding, 2);
            $paid = 0;

            if ($amount >= $outstanding) {
                $paid = round($outstanding, 2);

                $loan_transaction->update([

                    'description'           => $description,
                    'payment_source_id'     => $source->id,
                    'paid'                  => $loan_transaction->amount,
                    'outstanding'           => 0,
                    'status'                => 'completed',
                    'payment_date'          => date('Y-m-d H:i:s')
                ]);

                _log('Loan Repayment with reference '.$loan_transaction->reference.' completed.', $loan->user);
            } else {
                $paid = round($amount, 2);

                $outstanding = $loan_transaction->outstanding - $paid;

                $loan_transaction->update([

                    'description'           => $description,
                    'payment_source_id'     => $source->id,
                    'paid'                  => $paid,
                    'outstanding'           => $outstanding,
                    'status'                => 'partly_paid',
                    'payment_date'          => date('Y-m-d H:i:s')
                ]);

                _log('Loan Repayment with reference '.$loan_transaction->reference.' partly paid.', $loan->user);
            }

            $amount -= $paid;
            $amount = round($amount, 2);
        }

        if ($amount_to_pay_now > 0) {
            SendLoanNotificationJob::dispatch($loan, $amount_to_pay_now, $description)->delay(now()->addMinutes(2));
        }

        return (object)['status' => true, 'message' => 'Loan has been successfully updated.'];
    }

    public static function unpayLoan(Loan $loan, $amount = 0)
    {
        if (!$loan || $loan->status != 'running') {
            return (object)['status' => false, 'message' => 'Only running loans can be updated'];
        }

        if ($amount < 1) {
            return (object)['status' => false, 'message' => 'Invalid Amount'];
        }

        if ($loan->loan_transactions()->sum('paid') < $amount) {
            return (object)['status' => false, 'message' => 'Total amount paid on loan is less than value'];
        }

        _log('Updating Loan... '.$amount.' on '.$loan->name, $loan->user);

        LoanTransaction::create([
            'loan_id'           => $loan->id,
            'user_id'           => $loan->user_id,
            'company_id'        => $loan->company_id,
            'reference'         => _reference(),
            'principal'         => $amount,
            'interest'          => 0,
            'amount'            => $amount,
            'paid'              => 0,
            'outstanding'       => $amount,
            'balance'           => $amount,
            'date'              => date('Y-m-d H:i:s'),
            'description'       => 'REVERSAL',
            'status'            => 'pending'
        ]);

        $outstanding = $loan->loan_transactions()->sum('outstanding');
        $loan->update(['outstanding' => $outstanding ]);

        return (object)['status' => true, 'message' => 'Loan has been successfully updated.'];
    }
}
