<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationRead extends Model
{
    protected $hidden   = ['deleted_at'];
    protected $guarded  = ['updated_at'];

    public function notification()
    {
    	return $this->belongsTo('App\Notification');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
