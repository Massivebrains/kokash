<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Scopes;

class Share extends Model
{
    protected $hidden   = ['updated_at', 'deleted_at', 'password', 'remember_token'];
    protected $guarded  = ['updated_at'];

    use Scopes;

    public function company()
    {
        return $this->belongsTo('App\Company')->withDefault(function(){

            return new Company();
        });
    }

    public function user()
    {
        return $this->belongsTo('App\User')->withDefault(function(){

            return new User();
        });
    }

    public static function search($search = '', $company_id = 0)
    {
        return self::where(['company_id' => $company_id])
        ->whereHas('user', function($query) use ($search) {

        	$query->where('first_name', 'like', "%$search%")
        	->orWhere('last_name', 'like', "%$search%");
        })
        ->paginate(50);
    }
}
