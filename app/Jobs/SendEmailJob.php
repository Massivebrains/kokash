<?php

namespace App\Jobs;

use App\Notification;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;
    public $subject;
    public $body;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $subject, $body)
    {
        $this->user = $user;
        $this->subject = $subject;
        $this->body = $body;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $notification = Notification::create([
            'company_id'	=> $this->user->company_id,
            'user_id'		=> $this->user->id,
            'title'			=> 'Email: '.$this->subject,
            'scheduled_at'  => date('Y-m-d H:i:s'),
            'notification'	=> $this->body
        ]);

        if ($this->user->company->notifications == 'on') {
            $response = _email($this->user->email, $this->subject, $this->body);
            $notification->update(['status' => 'sent', 'response' => json_encode($response)]);
        } else {
            $notification->update(['status' => 'sent', 'response' => json_encode(['status' => 'Notification Off'])]);
        }
    }
}
