<?php

namespace App\Jobs;

use App\Loan;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendLoanNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $loan;
    public $amount;
    public $description;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Loan $loan, $amount, $description)
    {
        $this->loan = $loan;
        $this->amount = $amount;
        $this->description = $description;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->loan = Loan::find($this->loan->id);
        $sms = _loan_gross_repayment_sms($this->loan, $this->amount, $this->loan->outstanding - $this->amount, $this->description);
        SendSMSJob::dispatch($this->loan->user, $sms);
        $amount_text     = number_format($this->amount, 2);
        $subject    = 'Loan Repayment Transaction ['.$amount_text.' ]';
        $body       = view('emails.loan-transaction-alert', ['loan' => $this->loan, 'amount' => $amount_text])->render();
        SendEmailJob::dispatch($this->loan->user, $subject, $body);
    }
}
