<?php

namespace App\Jobs;

use App\Notification;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendSMSJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;
    public $sms;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $sms)
    {
        $this->user = $user;
        $this->sms = $sms;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $notification = Notification::create([
            'company_id'	=> $this->user->company_id,
            'user_id'		=> $this->user->id,
            'title'			=> 'SMS',
            'scheduled_at'  => date('Y-m-d H:i:s'),
            'notification'	=> $this->sms
        ]);
        if ($this->user->company->notifications == 'on') {
            $response = _send_sms($this->user->phone, $this->sms);
            $notification->update(['status' => 'sent', 'response' => json_encode($response)]);
        } else {
            $notification->update(['status' => 'sent', 'response' => json_encode(['status' => 'Notification Off'])]);
        }
    }
}
