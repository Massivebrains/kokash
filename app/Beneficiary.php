<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beneficiary extends Model
{
    protected $hidden   = ['deleted_at'];
    protected $guarded  = ['updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function partner()
    {
        return $this->belongsTo('App\User', 'partner_id');
    }
}
