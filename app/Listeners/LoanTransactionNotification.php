<?php

namespace App\Listeners;

use App\Events\LoanTransactionSaved;
use App\LoanTransaction;
use App\Loan;
use App\Jobs\SendEmailJob;

class LoanTransactionNotification
{
    public function handle(LoanTransactionSaved $event)
    {
        $loan_transaction   = $event->loan_transaction;
        $loan               = Loan::find($loan_transaction->loan_id);
        $outstanding        = LoanTransaction::where(['loan_id' => $loan_transaction->loan_id])->where('status', '!=', 'completed')->sum('outstanding');
        $loan->update(['outstanding' => $outstanding]);

        if ($loan_transaction->status != 'completed') {
            return;
        }

        if ($outstanding == 0) {
            $loan->update(['status' => 'completed']);
            $subject    = 'Loan Repayment Complete!';
            $body       = view('emails.loan-complete', ['loan' => $loan])->render();
            SendEmailJob::dispatch($loan->user, $subject, $body);
        }
    }
}
