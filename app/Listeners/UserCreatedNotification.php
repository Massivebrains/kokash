<?php

namespace App\Listeners;

use App\Events\UserCreated;
use App\Jobs\SendEmailJob;
use App\Jobs\SendSMSJob;

class UserCreatedNotification
{
    public function __construct()
    {
    }

    public function handle(UserCreated $event)
    {
        $user = $event->user;

        $password = substr((string)rand() * time(), 0, 6);

        $user->update(['password' => bcrypt($password)]);

        if ($user->type == 'admin') {
            $subject    = 'An Admin account has been created for you on kokash.ng.';
            $body       = view('emails.new-registration-admin', ['user' => $user, 'password' => $password])->render();
        } else {
            $subject    = 'An account has been created for you on kokash!';
            $body       = view('emails.new-registration', ['user' => $user, 'password' => $password])->render();
        }


        if ($user->email) {
            SendEmailJob::dispatch($user, $subject, $body);
        }

        SendSMSJob::dispatch($user, _password_sms($password, $user->phone, $user->company->name));
    }
}
