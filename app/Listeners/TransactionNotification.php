<?php

namespace App\Listeners;

use App\Events\TransactionSaved;
use App\Transaction;
use App\SavingsAccount;
use App\Jobs\SendEmailJob;
use App\Jobs\SendSMSJob;

class TransactionNotification
{
    public function handle(TransactionSaved $event)
    {
        $transaction 	= $event->transaction;
        $user 			= $transaction->user;
        $balance 		= $user->savings_account->balance + $transaction->amount;

        Transaction::where(['id' => $transaction->id])->update([
            'running_balance' => $balance
        ]);

        $savingsAccount = SavingsAccount::where(['user_id' => $user->id])->first();

        if (!$savingsAccount) {
            $savingsAccount = SavingsAccount::create(['user_id' => $user->id, 'company_id' => $user->company_id, 'monthly_savings' => 10000]);
        }

        $savingsAccount->update([ 'balance'	=> $balance ]);

        $transaction 	= Transaction::find($transaction->id);
        $amount 		= number_format($transaction->amount, 2);

        $type = $transaction->amount > 0 ? 'Credit' : 'Debit';

        $subject    = 'Transaction Alert ['.$type.': '.$amount.' ]';
        $body       = view('emails.transaction-alert', ['transaction' => $transaction])->render();
        SendEmailJob::dispatch($user, $subject, $body);
        SendSMSJob::dispatch($user, _transaction_sms($transaction));
    }
}
