<?php

namespace App\Listeners;

use App\Events\LoanDisbursed;
use App\Jobs\SendEmailJob;
use App\Jobs\SendSMSJob;

class LoanDisbursedNotification
{
    public function __construct()
    {
    }

    public function handle(LoanDisbursed $event)
    {
        $loan           = $event->loan;
        $user           = $loan->user;

        $subject    = 'Loan Disbursement Alert ['.$loan->principal.' ]';
        $body       = view('emails.loan-disbursed', ['loan' => $loan])->render();
        SendEmailJob::dispatch($user, $subject, $body);
        SendSMSJob::dispatch($user, _loan_disbursement_sms($loan));
    }
}
