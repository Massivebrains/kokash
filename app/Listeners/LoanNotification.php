<?php

namespace App\Listeners;

use App\Jobs\SendEmailJob;

class LoanNotification
{
    public function handle($event)
    {
        $loan           = $event->loan;

        if ($loan->status != 'pending') {
            return;
        }

        $user           = $loan->user;

        $subject    = 'Loan Alert ['.$loan->principal.' ]';
        $body       = view('emails.new-loan', ['loan' => $loan])->render();
        SendEmailJob::dispatch($user, $subject, $body);
    }
}
