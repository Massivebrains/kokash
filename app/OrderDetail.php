<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use SoftDeletes;
    
	protected $guarded  = ['updated_at'];

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function order()
	{
		return $this->belongsTo('App\Order')->withDefault(function(){

			return new \App\Order();
		});
	}

	public function loan()
	{
		return $this->belongsTo('App\Loan')->withDefault(function(){

			return new \App\Loan();
		});
	}

	public function transaction()
	{
		return $this->belongsTo('App\Transaction')->withDefault(function(){

			return new \App\Transaction();
		});
	}
}
