<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
	use SoftDeletes;

    protected $hidden   = ['deleted_at'];
    protected $guarded  = ['updated_at'];

    public function users()
    {
    	return $this->hasMany('App\User');
    }

    public function setting()
    {
    	return $this->hasOne('App\Setting');
    }
}
