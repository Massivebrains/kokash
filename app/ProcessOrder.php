<?php

namespace App;

use \App\Order;
use \App\OrderDetail;
use \App\Transaction;
use \App\Loan;
use \App\LoanTransaction;
use \App\User;

class ProcessOrder
{
    public static function process(Order $order)
    {
        if($order->status != 'paid') return false;

        foreach($order->order_details as $orderDetail){

            $user  = User::firstOrNew(['id' => $orderDetail->user_id]);

            if($orderDetail->type == 'savings'){

                $amount         = _c($orderDetail->amount);
                $description    = 'Payment of '.$amount.' via #'.$order->order_reference;

                $response = Transaction::pay($user, $orderDetail->amount, $description, $order->payment_source_id);

                if($response->status == true)
                    $orderDetail->update(['transaction_id' => $response->transaction_id, 'status' => 'completed']);
            }

            if($orderDetail->type == 'loan'){

                $loan = Loan::find($orderDetail->loan_id);

                $amount        = $orderDetail->amount;
                $description   = 'Loan Repayment Via #'.$order->order_reference;

                $response   = Loan::pay($loan, $amount, $description, $order->payment_source_id);

                if($response->status == true)
                    $orderDetail->update(['status' => 'completed']);
            }
        }

        return $order->update(['status' => 'completed']);
    }

}
