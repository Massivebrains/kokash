<?php

namespace App\Traits;

trait Scopes
{
	public static function scopeCompany($query, $company_id = 0)
    {
        return $query->where(['company_id' => $company_id]);
    }

    public function scopeUser($query)
    {
        return $query->where(['type' => 'user']);
    }

    public function scopeAdmin($query)
    {
        return $query->where(['type' => 'admin']);
    }

    public function scopeZeus($query)
    {
        return $query->where(['type' => 'zeus']);
    }

    public function scopeActive($query)
    {
        return $query->where(['status' => 'active']);
    }

    public function scopeInactive($query)
    {
        return $query->where(['status' => 'inactive']);
    }

}