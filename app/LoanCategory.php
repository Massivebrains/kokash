<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanCategory extends Model
{
    protected $hidden   = ['deleted_at'];
    protected $guarded  = ['updated_at'];

    public function loans()
    {
    	return $this->hasMany('App\Loan');
    }
}
