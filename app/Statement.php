<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Statement extends Model
{
	use SoftDeletes;
	
	protected $hidden   = ['deleted_at'];
    protected $guarded  = ['updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function getStatusAttribute($value)
    {
    	return ucfirst($value);
    }

    public function getStartDateAttribute($value)
    {
    	return _api_date($value);
    }

    public function getEndDateAttribute($value)
    {
    	return _api_date($value);
    }

    public function getCreatedAtAttribute($value)
    {
    	return _api_date($value);
    }
}
