<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        //
    ];

    protected function schedule(Schedule $schedule)
    {
        //$schedule->call('App\Http\Controllers\Cron\UserProductsController@truncate')->daily();
        $schedule->call('App\Http\Controllers\Cron\JobsController@email')->everyMinute();
        //$schedule->call('App\Http\Controllers\Cron\JobsController@sms')->everyMinute();

        //$schedule->command('backup:clean')->daily()->at('01:00');
        //$schedule->command('backup:run')->daily()->at('02:00');

        $schedule->call(App\Http\Controllers\Cron\DBDumpController::class)->daily();
    }

    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
