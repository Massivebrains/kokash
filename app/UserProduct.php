<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProduct extends Model
{
    protected $hidden   = ['updated_at'];
    protected $guarded  = ['updated_at'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function for()
    {
    	return $this->belongsTo('App\User', 'for_user_id');
    }

    public static function getProducts(User $user = null)
    {
        if(!$user) return null;

        self::where(['user_id' => $user->id])->delete();

        $amount             = $user->savings_account->monthly_savings;
        $formatted_amount   = _c($amount);
        $notes              = "Your monthly savings is $formatted_amount.";

        $most_recent_transaction = Transaction::where([

            'user_id' => $user->id, 
            'status' => 'completed'

        ])
        ->orderBy('updated_at', 'desc')
        ->first();


        if($most_recent_transaction){

            $date = _api_date($most_recent_transaction->updated_at);

            $notes .=" The last time you saved was $date";
        }

        self::create([

            'user_id'       => $user->id,
            'for_user_id'   => $user->id,
            'label'         => 'Amount to save now',
            'description'   => 'Personal Savings Account',
            'notes'         => $notes,
            'amount'        => $amount
        ]);

        $loans = Loan::by($user)->running()->get();

        foreach($loans as $loan){

            $next_loan_transaction = LoanTransaction::where([

                'loan_id'   => $loan->id, 
                'status'    => 'pending'
            ])
            ->orderBy('date', 'asc')
            ->first();

            $amount             = $loan->monthly_payment;
            $formatted_amount   = _c($amount);  
            $label              = ucwords(strtolower($loan->name));
            $notes              = "You pay $formatted_amount monthly.";

            if($next_loan_transaction){

                $date = _month($next_loan_transaction->date);
                $notes .= " Your next repayment is $date.";
            }

            $double_amount = _c($amount * 2);
            $notes .= " You can pay for multiple months at once. e.g $double_amount will cover for two months.";

            self::create([

                'user_id'       => $user->id,
                'type'          => 'loan',
                'for_user_id'   => $user->id,
                'loan_id'       => $loan->id,
                'label'         => $label,
                'description'   => "$label (SELF)",
                'notes'         => $notes,
                'amount'        => $amount,
            ]);
        }

        foreach($user->beneficiaries as $beneficiary){

            $partner_name       = ucwords($beneficiary->partner->name);
            $amount             = $beneficiary->partner->savings_account->monthly_savings;
            $formatted_amount   = _c($amount);

            self::create([

                'user_id'       => $user->id,
                'for'           => 'beneficiary',
                'for_user_id'   => $beneficiary->partner->id,
                'label'         => "Save for $partner_name",
                'description'   => "Savings for $partner_name",
                'notes'         => "$partner_name saves $formatted_amount monthly.",
                'amount'        => $amount
            ]);

            $loans = Loan::by($beneficiary->partner)->running()->get();

            foreach($loans as $loan){

                $amount             = $loan->monthly_payment;
                $formatted_amount   = _c($amount);
                $loan_name          = ucwords(strtolower($loan->name));
                $partner_name       = ucwords($beneficiary->partner->name);
                $label              = "$loan_name for $partner_name";

                self::create([

                    'user_id'       => $user->id,
                    'for'           => 'beneficiary',
                    'for_user_id'   => $beneficiary->partner->id,
                    'type'          => 'loan',
                    'loan_id'       => $loan->id,
                    'label'         => $label,
                    'description'   => $label,
                    'notes'         => "$partner_name pays $formatted_amount monthly.",
                    'amount'        => $amount,
                ]);
            }
        }

        return self::where(['user_id' => $user->id])->get();
    }

}
