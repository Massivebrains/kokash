<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    
	protected $guarded  = ['updated_at'];
	protected $appends 	= ['kobo'];

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function company()
	{
		return $this->belongsTo('App\Company');
	}

	public function transaction()
	{
		return $this->hasOne('App\Transaction')->withDefault(function(){

			return new \App\Transaction();
		});
	}

	public function payment_source()
	{
		return $this->belongsTo('App\PaymentSource')->withDefault(function(){

			return new \App\PaymentSource();
		});
	}

	public function order_details()
	{
		return $this->hasMany('App\OrderDetail');
	}

	public function scopeCompany($query, $company_id = 0)
    {
        return $query->where(['company_id' => $company_id]);
    }
    
	public function scopeBy($query, $user){

		return $query->where(['user_id' => $user->id]);
	}

	public function scopePending($query)
	{
		return $query->where(['status' => 'pending']);
	}

	public function scopeCompleted($query)
	{
		return $query->where(['status' => 'completed']);
	}

	public function scopeCancelled($query)
	{
		return $query->where(['status' => 'cancelled']);
	}

	public function getKoboAttribute()
	{
		return $this->total * 100;
	}

	public static function search($search = '', $company_id = 0)
    {
        return self::where(['company_id' => $company_id])

        ->where(function($query) use($search){

            $query->where('order_reference', $search)
            ->orWhere('total', $search);
        })

        ->paginate(50);
    }
}
