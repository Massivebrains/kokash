<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\LoanTransactionSaved;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Carbon\Carbon;
use Auth;

class LoanTransaction extends Model
{
    use SoftDeletes;

    protected $guarded  = ['updated_at'];
    protected $dates   = ['created_at', 'updated_at'];

    protected $dispatchesEvents = [

        'updated' => LoanTransactionSaved::class
    ];

    public function loan()
    {
        return $this->belongsTo('App\Loan');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function payment_source()
    {
        return $this->belongsTo('App\PaymentSource');
    }

    public function fine()
    {
        return $this->belongsTo('App\Transaction', 'fine_transaction_id')->withDefault(function () {
            return new \App\Transaction();
        });
    }

    public function scopeCompany($query, $company_id = 0)
    {
        return $query->where(['company_id' => $company_id]);
    }

    public function scopePending($query)
    {
        return $query->where(['status' => 'pending']);
    }

    public function scopePartlyPaid($query)
    {
        return $query->where(['status' => 'partly_paid']);
    }

    public function scopeCompleted($query)
    {
        return $query->where(['status' => 'completed']);
    }

    public function scopeCancelled($query)
    {
        return $query->where(['status' => 'cancelled']);
    }

    public function scopeBy($query, \App\User $user)
    {
        return $query->where(['user_id' => $user->id]);
    }

    public static function preCalculate($principal = 0, $rate = 0, $time = 0, $start_at = '', $repayment_type = 'deduct_interest')
    {
        $transactions = [];
        $months       = $time;
        $amount       = round($principal / $months, 2);
        $interest     = $principal * ($rate / 100) / $months;

        for ($i = 0; $i < $months; $i++) {
            $transaction = new LoanTransaction();

            if ($repayment_type == 'deduct_interest') {
                $transaction->principal = $amount - $interest;
                $transaction->interest  = $interest;
                $transaction->amount    = $amount;
            } else {
                $transaction->principal = $amount;
                $transaction->interest  = $amount * ($rate / 100);
                $transaction->amount    = $amount * ($rate / 100) + $amount;
            }

            $transaction->date      = Carbon::createFromFormat('d-m-Y', "01-$start_at")->addMonths($i);
            $transaction->balance   = 0;

            $transactions[] = $transaction;
        }

        return collect($transactions);
    }

    public static function outstanding(\App\User $user)
    {
        $outstanding = self::whereHas('loan', function ($query) {
            $query->where(['status' => 'running']);
        })->by($user)->where('status', '!=', 'completed')->sum('outstanding');

        return $outstanding;
    }

    public static function transform($transactions = [])
    {
        collect($transactions)->map(function ($row) {
            $month          = _month($row->date);
            $outstanding    = _c($row->outstanding);
            $payment_date   = _api_date($row->payment_date);
            $due_date       = _api_date($row->date);

            $text = $month.' Loan Repayment Completed on '.$payment_date;

            if ($row->status == 'pending') {
                $text = $month.' Pending Loan Repayment of '._c($row->outstanding).' Due on '.$due_date;
            }

            if ($row->status == 'partly_paid') {
                $text = $month.' Part Payment of Loan Repayment done on '. $payment_date .' Balance is '.$outstanding;
            }

            $row->key                 = str_random(10);
            $row->amount              = $outstanding;
            $row->due_date            = $due_date;
            $row->payment_date        = $payment_date;
            $row->month               = $month;
            $row->description         = $text;
            $row->status              = strtoupper(implode('-', explode('_', $row->status)));
        });

        return $transactions;
    }
}
