<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Services\BankStatementRegex;

class BankStatement extends Model
{
    protected $hidden   = ['deleted_at'];
    protected $guarded  = ['updated_at'];

    public function user()
    {
    	return $this->belongsTo('App\User')->withDefault();
    }

    public function possible_user()
    {
    	return $this->belongsTo('App\User', 'possible_user_id')->withDefault();
    }

    public function breakdowns()
    {
        return $this->hasMany('App\BankStatementBreakdown');
    }

    public static function getSearchableNames($company_id = 0)
    {
        $_users = [];
        $users  = User::select(['id', 'first_name', 'last_name', 'phone'])->whereCompanyId($company_id)->get();

        foreach($users as $row){

            $_users[] = (object)[

                'id'    => $row->id,
                'name'  => $row->first_name.' '.$row->last_name
            ];
        }

        return $_users;
    }

    public static function search($search = null, $company_id = 0, $status = 'matched')
    {
        return self::where(['company_id' => $company_id])

        ->where('status', $status)

        ->where(function($query) use($search){

            $query->where('id', $search)
            ->orWhere('reference', 'like', "%$search%")
            ->orWhere('bank', 'like', "%$search%")
            ->orWhere('account_number', 'like', "%$search%");
        })
        ->orderBy('reference', 'desc')
        ->paginate(50);

    }

    public static function updatePossibleUsers()
    {
        $statements = self::whereNull('possible_user_id')->get();
        $users      = [];

        foreach($statements as $row){

            if(!isset($users[$row->company_id]))
                $users[$row->company_id] = self::getSearchableNames($row->company_id);

            $regex = new BankStatementRegex($row->reference, $users[$row->company_id]);
            $user = $regex->search();
            $row->update(['possible_user_id' => $user ? $user->id : 0]);
        }
    }
}
