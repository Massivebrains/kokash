<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [

        'App\Events\TransactionSaved' => [

            'App\Listeners\TransactionNotification',
        ],
        'App\Events\LoanSaved' => [
            'App\Listeners\LoanNotification',
        ],
        'App\Events\LoanTransactionSaved' => [

            'App\Listeners\LoanTransactionNotification',
        ],
        'App\Events\LoanDisbursed' => [

            'App\Listeners\LoanDisbursedNotification',
        ],
        'App\Events\UserCreated' => [

            'App\Listeners\UserCreatedNotification',
        ],
    ];


    public function boot()
    {
        parent::boot();

        //
    }
}
