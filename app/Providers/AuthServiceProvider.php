<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{

    protected $policies = [
        
        'App\User'              => 'App\Policies\UserPolicy',
        'App\Loan'				=> 'App\Policies\LoanPolicy',
        'App\Transaction'		=> 'App\Policies\TransactionPolicy',
        'App\LoanTransaction'	=> 'App\Policies\LoanTransactionPolicy',
        'App\Statement'         => 'App\Policies\StatementPolicy',
        'App\Share'             => 'App\Policies\SharePolicy',
        'App\BankStatement'	    => 'App\Policies\BankStatementPolicy',
    ];


    public function boot()
    {
        $this->registerPolicies();

        Gate::define('view-report', function($user, $model = null){

            return !is_null($user->access_level) && $user->access_level  > 1; 
        });

        Gate::define('update-notifications', function($user, $model = null){

            return !is_null($user->access_level) && $user->access_level  > 0; 
        });

        Gate::define('update-faq', function($user, $model = null){

            return !is_null($user->access_level) && $user->access_level  > 0; 
        });

        Gate::define('view-member-details', function($user, $model = null){

            return !is_null($user->access_level) && $user->access_level  > 0; 
        });

        Gate::define('delete-member', function($user, $model = null){

            return $user->id == 1;
        });

    }
}
