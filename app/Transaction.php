<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\TransactionSaved;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;
    
    protected $hidden   = ['deleted_at'];
    protected $guarded  = ['updated_at'];

    protected $dispatchesEvents = [

        'saved' => TransactionSaved::class
    ];

    public function user()
    {
        return $this->belongsTo('App\User')->withDefault();
    }

    public function company()
    {
        return $this->belongsTo('App\Company')->withDefault();
    }

    public function transaction_type()
    {
        return $this->belongsTo('App\TransactionType')->withDefault();
    }

    public function payment_source()
    {
        return $this->belongsTo('App\PaymentSource')->withDefault();
    }

    public function order_detail()
    {
        return $this->hasOne('App\OrderDetail')->withDefault();
    }

    public function scopeCompany($query, $company_id = 0)
    {
        return $query->where(['company_id' => $company_id]);
    }

    public function scopeOwnedByCompany($query, $company_id = 0)
    {
        return $query->where(['company_id' => $company_id]);
    }

    public function scopePending($query)
    {
        return $query->where(['status' => 'pending']);
    }

    public function scopeCompleted($query)
    {
        return $query->where(['status' => 'completed']);
    }

    public function scopeCancelled($query)
    {
        return $query->where(['status' => 'cancelled']);
    }

    public function scopeBy($query, \App\User $user)
    {
        return $query->where(['user_id' => $user->id]);
    }

    public static function search($search = '', $company_id = 0)
    {
        return self::where(['company_id' => $company_id])

        ->where(function ($query) use ($search) {
            $query->where('reference', $search)
            ->orWhere('amount', $search)
            ->orWhere('description', 'like', "%$search%")
            ->orWhere('status', $search);
        })

        ->paginate(50);
    }

    public static function transform($transactions = [])
    {
        $transactions->map(function ($row) {
            $row->key                        = str_random(10);
            $row->amount_formatted           = number_format($row->amount, 2);
            $row->running_balance_formatted  = number_format($row->running_balance, 2);
        });

        return $transactions;
    }

    public static function savingsGraph(User $user = null)
    {
        $start = date('n');
        
        $labels = [];
        $data   = [];

        for ($i = 0; $i<=6; $i++) {
            $month = date('n') - $i;
            $year  = date('Y');

            if ($month <= 0) {
                $year = date('Y') - 1;
                $month = $month + 12;
            }

            $sum = Transaction::by($user)
            ->completed()
            ->whereYear('created_at', $year)
            ->whereMonth('created_at', $month)
            ->sum('amount');

            $labels[]   = date('M', mktime(0, 0, 0, $month, 10));
            $data[]     = (float)$sum;
        }

        return ['labels' => array_reverse($labels), 'data' => array_reverse($data)];
    }

    public static function pay(User $user, $amount = 0, $description = '', $payment_source_id = 1)
    {
        if ($amount == 0) {
            return (object)['status' => false, 'message' => 'Amount cannot be zero.'];
        }

        $transaction = self::create([

            'user_id'                   => $user->id,
            'company_id'                => _cid(),
            'transaction_type_id'       => TRANSACTION_TYPE_SAVINGS,
            'reference'                 => _reference(),
            'amount'                    => $amount,
            'description'               => $description,
            'payment_source_id'         => $payment_source_id
        ]);

        $amount = _c($amount);
        
        _log('Transaction of '.$amount.' created for '.$user->name, $user);

        return (object)['status' => true, 'transaction_id' => $transaction->id, 'message' => 'Member account has been successfully credited'];
    }
}
