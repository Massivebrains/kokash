<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Zeus
{
   
    public function handle($request, Closure $next)
    {
        if(!Auth::check() || Auth::user()->type != 'zeus'){

            return redirect('logout');
        }

        return $next($request);
    }
}
