<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check() && Auth::user()->type == 'admin';
    }


    public function rules()
    {
        $id = request('id');

        return [

            'first_name'        => 'required',
            'last_name'         => 'required',
            'email'             => '',
            'phone'             => 'required|max:14|unique:users,phone,'.$id,
            'sex'               => 'required|in:male,female',
            'marital_status'    => 'required|in:single,married',
            'country_id'        => 'required|numeric',
            'state_id'          => 'required|numeric',
            'address'           => 'required',
            'occupation'        => '',
            'employer'          => '',
            'employer_address'  => '',
            'next_of_kin'       => '',
            'next_of_kin_phone' => ''
        ];
    }
}
