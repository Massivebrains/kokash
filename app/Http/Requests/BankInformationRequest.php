<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use App\Rules\AccountNumber;

class BankInformationRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check() && Auth::user()->type == 'admin';
    }


    public function rules()
    {
        return [
            
            'bank_id'               => 'required|numeric',
            'bank_account_number'   => ['required', 'numeric', new AccountNumber],
            'bank_account_name'     => 'required'
        ];
    }
}
