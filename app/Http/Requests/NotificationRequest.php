<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class NotificationRequest extends FormRequest
{
    
    public function authorize()
    {
        return Auth::check() && Auth::user()->type == 'admin';
    }

    public function rules()
    {
        return [
            
            'title'         => '',
            'notification'  => 'required'
        ];
    }
}
