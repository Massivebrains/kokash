<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use App\Rules\AdminPassword;
use App\Rules\ShareUnits;

class ShareRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check() && Auth::user()->type == 'admin';
    }

    public function rules()
    {
        return [
            
            'user_id'   => 'required|numeric',
            'units'     => ['required', new ShareUnits, 'min:0'],
            'password'  => ['required', new AdminPassword]
        ];
    }
}
