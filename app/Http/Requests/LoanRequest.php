<?php

namespace App\Http\Requests;
use Auth;
use App\Rules\AdminPassword;
use Illuminate\Foundation\Http\FormRequest;

class LoanRequest extends FormRequest
{
    public function authorize()
    {
       return Auth::check() && Auth::user()->type == 'admin';
    }

    public function rules()
    {
        return [

            'loan_category_id'  => 'required|numeric|exists:loan_categories,id',
            'user_id'           => 'required|numeric|exists:users,id',
            'principal'         => 'required|numeric|gt:0',
            'rate'              => 'required|numeric|gt:0',
            'time'              => 'required|numeric|gt:0',
            'start_at'          => 'required',
            'password'          => ['required', new AdminPassword],
            'repayment_type'    => 'required|in:deduct_interest,add_interest'
        ];
    }
}
