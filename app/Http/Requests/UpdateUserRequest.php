<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = request('id');

        return [

            'first_name'        => 'required',
            'last_name'         => 'required',
            'phone'             => 'required|max:14'
        ];
    }
}
