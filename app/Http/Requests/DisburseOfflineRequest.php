<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\AdminPassword;
use Auth;

class DisburseOfflineRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check() && Auth::user()->type == 'admin';
    }


    public function rules()
    {
        return [
            
            'disbursed_at'          => 'required',
            'disbursement_notes'    => '',
            'password'              => ['required', new AdminPassword]
        ];
    }
}
