<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class StatementRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check() && Auth::user()->type == 'user';
    }

    public function rules()
    {
        return [

            'start_date'  => 'required',
            'end_date'    => 'required'
        ];
    }
}
