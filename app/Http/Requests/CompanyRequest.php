<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CompanyRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check() && Auth::user()->type == 'zeus';
    }

    public function rules()
    {
        return [
            
            'name'                  => 'required',
            'description'           => '',
            'address'               => 'required',
            'contact_person'        => 'required',
            'contact_person_phone'  => 'required',
            'contact_person_email'  => 'required'
        ];
    }
}
