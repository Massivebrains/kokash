<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class TransactionTypeRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check() && Auth::user()->type == 'zeus';
    }

  
    public function rules()
    {
        return [

            'name'      => 'required',
            'status'    => 'required'
        ];
    }
}
