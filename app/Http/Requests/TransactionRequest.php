<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use App\Rules\AdminPassword;

class TransactionRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check() && Auth::user()->type == 'admin';
    }


    public function rules()
    {
        return [

            'user_id'       => 'required|numeric',
            'amount'        => 'required|numeric',
            'description'   => 'required',
            'password'      => ['required', new AdminPassword]
        ];
    }
}
