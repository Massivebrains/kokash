<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class FaqRequest extends FormRequest
{
    
    public function authorize()
    {
        return Auth::check() && Auth::user()->type == 'admin';
    }


    public function rules()
    {
        return [

            'question'   => 'required',
            'answer'     => 'required'
        ];
    }
}
