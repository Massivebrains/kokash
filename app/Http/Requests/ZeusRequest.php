<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class ZeusRequest extends FormRequest
{
   
    public function authorize()
    {
        return Auth::check() && Auth::user()->type == 'zeus';
    }

    public function rules()
    {
        return [
            
            
        ];
    }
}
