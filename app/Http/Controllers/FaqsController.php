<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FaqRequest;
use App\Faq;
use Auth;
use App\Company;


class FaqsController extends Controller
{
    public function index()
    {
        $data['faqs']  	= Faq::where(['company_id' => Auth::user()->company_id])->latest()->get();
        $data['title']  = 'faqs';

    	return view('faqs.index', $data);
    }

    public function form($id = 0)
    {
    	$data['faq']		= Faq::firstOrNew(['id' => $id]);
    	$data['company']	= Company::get();
    	$data['title']		= 'faq';

    	return view('faqs.form', $data);
    }

    public function save(FaqRequest $request)
    {
    	$faq = Faq::updateOrCreate(['id' => request('id')],[

    		'company_id'	=> Auth::user()->company_id,
            'question'   	=> request('question'),
            'answer' 		=> request('answer')

    	]);

        _log('Saved FAQ');
        return redirect('faqs')->with('message', 'FAQ saved succesfully.');
    }
}
