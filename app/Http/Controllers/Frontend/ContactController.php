<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function index()
    {
    	$data['name']		= request('name');
    	$data['phone']		= request('phone');
    	$data['email']		= request('email');
    	$data['message']	= request('message');

    	$subject 			= 'New Contact Message';
    	$body 				= view('emails.contact', $data)->render();
    	
    	//_email('vadeshayo@gmail.com', $subject, $body);

    	return redirect('/#contact')->with('contact-message', 'Thank You for contacting us. We would reach out to you soon.');

    }
}
