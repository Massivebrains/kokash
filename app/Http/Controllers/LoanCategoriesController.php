<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LoanCategory;
use Auth;

class LoanCategoriesController extends Controller
{
    public function index()
    {
        $data['loan_categories'] 	= LoanCategory::whereCompanyId(Auth::user()->company_id)->get();
        $data['title']       		= 'loan categories';

    	return view('loan-categories.index', $data);
    }

    public function form($id = 0)
    {
    	$data['loan_category']	= LoanCategory::firstOrNew(['id' => $id]);
    	$data['title']			= 'Loan Category';
    	
    	return view('loan-categories.form', $data);
    }

    public function save(Request $request)
    {
    	$this->validate($request, ['name' => 'required']);

    	$loan_cateogry = LoanCategory::updateOrCreate(['id' => request('id')],[

    		'company_id' 	=> Auth::user()->company_id,
            'name'   		=> request('name')
    	]);

        _log('Saved loan category '.request('name'));
        session()->flash('message', 'Loan Category saved succesfully.');

        return redirect('loan-categories');
    }
}
