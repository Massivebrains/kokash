<?php

namespace App\Http\Controllers\BankStatement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BankStatement;
use Carbon\Carbon;
use Auth;
use DB;

class UploadController extends Controller
{
	public function index()
	{        
		$data['title']	= 'upload statement';

		return view('bank-statements.upload', $data);
	}

	public function upload(Request $request)
	{
		if(!$request->has('statement'))
			return redirect()->back()->with('error', 'You did not select a file to upload');

		$this->validate($request, [

			'bank'				=> 'required|in:GTBank,StanbicBank',
			'account_number'	=> 'numeric',
			'business_month'	=> 'required',
			'statement' 		=> 'required|mimes:csv,txt'
		]);

		$path 			= $request->file('statement')->store('statements');
		$bank 			= request('bank');
		$account_number	= request('account_number');

		$file    		= fopen(public_path('uploads/'.$path), 'r');
		$statements   	= [];

		while(!feof($file)) $statements[] = fgetcsv($file); 

		$errorCount = $successCount = $existingCount = 0;
		$errors 	= [];

		for($i = 1; $i <= count($statements)-1; $i++){

			if(!$statements[$i])
				continue;

			$row = $this->{$bank}($statements[$i]);

			if((double)$row->amount < 1)
				continue;

			$payload = [

				'bank'				=> $bank,
				'account_number'	=> $account_number,
				'reference'			=> $row->reference,
				'amount'			=> $row->amount
			];

			if(BankStatement::where($payload)->count() > 0){

				$existingCount++;
				continue;
			}

			$payload['company_id'] 			= Auth::user()->company_id;
			$payload['transaction_date'] 	= $row->date;
			$payload['business_month']		= Carbon::createFromFormat('m-Y', request('business_month'))->format('Y-m-d');

			BankStatement::create($payload);

			$successCount++;
		}

		$data['title']			= 'upload statement';

		if(count($errors) > 0)
			$data['upload_errors']	= $errors;
			
		BankStatement::updatePossibleUsers();
		session()->flash('message', $successCount.' unique Statement record(s) Uploaded Successfully. '.$existingCount.' existing statement records found.');

		if(count($errors) == 0)
			return redirect('/bank-statement/unmatched');
		
		return view('bank-statements.upload', $data);
	}

	private function GTBank($row = [])
	{
		if(count($row) < 7){

			return (object)[

				'date'	 		=> date('Y-m-d'),
				'amount' 		=> 0,
				'reference'		=> ''
			];
		}

		$date 			= $row[0] ?? 0;
		$reference 		= $row[1] ?? 0;
		$value_date 	= $row[2] ?? 0;
		$debit 			= (float)$row[3] ?? 0;
		$credit 		= (float)$row[4] ?? 0;
		$balance 		= $row[5] ?? 0;
		$remarks 		= $row[6] ?? 0;

		return (object)[

			'date'	 		=> date('Y-m-d', strtotime($date)),
			'amount' 		=> $debit > 0 ? 0 : $credit,
			'reference'	=> $remarks
		];
	}

	private function StanbicBank($row = [])
	{
		if(count($row) < 6){

			return (object)[

				'date'	 		=> date('Y-m-d'),
				'amount' 		=> 0,
				'reference'	=> ''
			];
		}

		$serial 		= $row[0] ?? 0;
		$date 			= $row[1] ?? 0;
		$description 	= $row[2] ?? 0;
		$debit 			= (float)$row[3] ?? 0;
		$credit 		= (float)$row[4] ?? 0;
		$balance 		= $row[5] ?? 0;

		return (object)[

			'date'	 		=> Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d'),
			'amount' 		=> $debit > 0 ? 0 : $credit,
			'reference'	=> $description
		];
	}

}
