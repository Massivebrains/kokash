<?php

namespace App\Http\Controllers\BankStatement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BankStatement;
use App\User;
use Auth;

class SplitController extends Controller
{
    public function index($id = 0)
    {
    	$statement = BankStatement::where([

    		'company_id'	=> Auth::user()->company_id,
    		'id'			=> $id,
    		'status'		=> 'pending'

    	])->first();

    	if(!$statement)
    		return redirect()->back()->with('error', 'Invalid Statement Record.');

    	$data['statement']     = $statement;
    	$data['users']		   = User::where(['company_id' => Auth::user()->company_id, 'status' => 'active'])->orderBy('first_name')->get();
		$data['title']         = 'split '.$statement->reference;

		return view('bank-statements.split', $data);
    }

    public function split(Request $request)
    {
    	$this->validate($request, [

    		'user_id'	=> 'required',
    		'amount'	=> 'required',
    		'id'		=> 'required'
    	]);

    	$statement = BankStatement::where([

    		'company_id'	=> Auth::user()->company_id,
    		'id'			=> request('id'),
    		'status'		=> 'pending'

    	])->first();

    	if(!$statement)
    		return redirect()->back()->with('error', 'Invalid Statement Record.');

    	if(request('amount') > $statement->amount)
    		return redirect()->back()->with('error', 'Split amount is more than remaining balance');

    	$user = User::where([

    		'id'			=> request('user_id'),
    		'company_id'	=> Auth::user()->company_id,
    		'status'		=> 'active'

    	])->first();

    	$amount 							= (float)request('amount');
    	$new_statement 						= $statement->replicate();
    	$new_statement->amount 				= $amount;
    	$new_statement->user_id 			= $user->id;
    	$new_statement->possible_user_id 	= $user->id;
    	$new_statement->reference 			= $statement->reference.' (!! SPLIT FOR '.$user->name.' !!)';
    	$new_statement->notes 				= 'Split of '._c($amount).' from '._c($statement->amount).' '.$statement->reference;
    	$new_statement->save();

    	$notes = $statement->notes;

    	if($notes == null)
    		$notes = 'OriginalAmount:'._c($statement->amount);

    	$notes .= ' / Split:'._c($amount).'=>'.$user->reference;

    	$statement->update([ 'notes' => $notes, 'amount' => $statement->amount - $amount ]);

    	return redirect()->back()->with('message', 'Split Successful. Remaining Amount: '._c($statement->amount));
    }
}
