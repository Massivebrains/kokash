<?php

namespace App\Http\Controllers\BankStatement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BankStatement;
use App\Transaction;
use App\Loan;
use Auth;

class ReversalController extends Controller
{
    public function index($id = 0)
    {
    	$statement = BankStatement::where([

    		'company_id'	=> Auth::user()->company_id,
    		'id'			=> $id,
    		'status'		=> 'matched'

    	])->first();

    	if(!$statement)
    		return redirect()->back()->with('error', 'Invalid Bank Statement record selected.');

    	$user = $statement->user;

    	foreach($statement->breakdowns as $row){

    		if($row->type == 'transaction')
    			Transaction::pay($user, -$row->amount, 'Reversal/Savings/'.$statement->id.'/'.$statement->bank, PAYMENT_SOURCE_BANK_TRANSFER);

            if($row->type == 'loan')
                Loan::unpayLoan(Loan::find($row->loan_id), $row->amount);

    		$row->delete();
    	}

    	$statement->update(['user_id' => null, 'status' => 'pending']);

    	return redirect('/bank-statement/unmatched')->with('message', 'Statment has been successfully reversed.');
    }
}
