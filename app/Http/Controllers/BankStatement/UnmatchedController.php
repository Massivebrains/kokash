<?php

namespace App\Http\Controllers\BankStatement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BankStatement;
use App\User;
use App\Transaction;
use App\Services\BankStatementBreakdown;
use Auth;

class UnmatchedController extends Controller
{
    public function index()
    {        
        $data['title'] = 'unmatched statements';

        return view('bank-statements.unmatched', $data);
    }

    public function statements()
    {
        $company_id = Auth::user()->company_id;

        $statements = BankStatement::where(['company_id' => $company_id])
        ->with('possible_user')
        ->whereStatus('pending')
        ->orderBy('id', 'desc')
        ->take(10)
        ->get();

        $count = BankStatement::where(['company_id' => $company_id, 'status' => 'pending'])->count();

        foreach($statements as $row){

            $row->transaction_date = _d($row->transaction_date, false);
            $row->amount_text = _c($row->amount);
        }

        $users = User::whereCompanyId($company_id)
        ->select(['id', 'first_name', 'last_name', 'reference'])
        ->orderBy('first_name', 'asc')
        ->orderBy('last_name', 'asc')
        ->get();

        $_users = [];

        foreach($users as $row)
            $_users[] = ['id' => $row->id, 'name' => $row->name.' ('.$row->reference.')'];

        return response()->json([

            'status'    => true,
            'data'      => [

                'statements'    => $statements,
                'count'         => $count,
                'users'         => $_users
            ]
        ]);
    }

    public function statement($id = 0, $new_possible_user_id = 0)
    {
        $statement = BankStatement::where([

            'company_id' =>  Auth::user()->company_id,
            'status'     => 'pending',
            'id'        => $id

        ])->first();

        if(!$statement)
            return response()->json(['status' => false, 'message' => 'Invalid Statement Record']);

        if($new_possible_user_id > 0)
            $statement->update(['possible_user_id' => $new_possible_user_id]);

        if($statement->possible_user_id > 0){

            $statement->breakdown = (new BankStatementBreakdown($statement))->getBreakdown();
        
        }else{

            $statement->breakdown = (object)['save' => 0, 'loans' => [] ];
        }

        $statement->amount_text = _c($statement->amount);

        return response()->json([

            'status'    => true,
            'message'   => 'Statement retrieved successfully.',
            'data'      => $statement
        ]);
    }

    public function discard($id = 0)
    {
        $statement = BankStatement::where([

            'company_id'    => Auth::user()->company_id,
            'status'        => 'pending',
            'id'            => $id

        ])->first();

        if(!$statement)
            return response()->json(['status' => false]);

        $statement->update(['status' => 'discarded']);

        return response()->json(['status' => true]);

    }
}
