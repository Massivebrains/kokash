<?php

namespace App\Http\Controllers\BankStatement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BankStatement;
use App\BankStatementBreakdown;
use App\Services\BankStatementReport;
use App\LoanCategory;
use Auth;
use Carbon\Carbon;

class ReportController extends Controller
{
	public function index()
	{   
		$data['title']	= 'bank statement report';

		return view('bank-statements.report', $data);
	}

	public function report(Request $request)
	{
		$company_id 	= Auth::user()->company_id;
		$month 			= Carbon::createFromFormat('m-Y', request('business_month'));

		$pending_statements = BankStatement::where([

			'status' 			=> 'pending',
			'company_id'		=> $company_id

		])
		->whereYear('business_month', $month->year)
		->whereMonth('business_month', $month->month)
		->count();

		if($pending_statements > 0)
			return redirect()->back()->with('error', 'There are unmatched statement records in this month. Please match all statements before downloading a report.');

		$report = (new BankStatementReport($company_id, $month->year, $month->month))->getReport();     

		if($report->count() < 1)
			return redirect()->back()->with('error', 'No matched statements in '.$month->format('m-Y'));

		$loan_categories = LoanCategory::whereCompanyId(Auth::user()->company_id)->get();

		return $this->exportAsCSV($report, $loan_categories, $month->format('m-Y'));
	}

	private function exportAsCSV($report, $loan_categories, $business_month)
	{		
		$filename = Auth::user()->company->name.'-Bank-Statement-Report-'.$business_month.'.csv';
		$columns = ['ID', 'MemberName', 'BankStatementAmount', 'Savings', 'Shares', 'Registration'];

		foreach($loan_categories as $row)
			$columns[] = $row->name;

		$callback = function() use ($report, $columns) {

			$file = fopen('php://output', 'w');
			fputcsv($file, $columns);

			foreach($report as $row)
				fputcsv($file, (array)$row);

			fclose($file);
		};

		return response()->streamDownload($callback, $filename);
	}


}
