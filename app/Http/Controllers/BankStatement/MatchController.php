<?php

namespace App\Http\Controllers\BankStatement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BankStatement;
use App\BankStatementBreakdown;
use App\Transaction;
use App\User;
use App\Loan;
use Auth;

class MatchController extends Controller
{
    public function index(Request $request)
    {
        $company_id = Auth::user()->company_id;

        $statement = BankStatement::where([

            'company_id'    => $company_id,
            'status'        => 'pending',
            'id'            => request('id')

        ])->first();

        if(!$statement)
            return response()->json(['status' => false, 'message' => 'Statement record not found.']);

        $user = User::where([

            'company_id'    => $company_id,
            'id'            => request('possible_user_id')

        ])->first();

        if(!$user)
            return response()->json(['status' => false, 'message' => 'Invalid Member selected']);

        $breakdown      = request('breakdown');
        $save           = isset($breakdown['save']) ? $breakdown['save'] : 0;
        $share          = isset($breakdown['share']) ? $breakdown['share'] : 0;
        $registration   = isset($breakdown['registration']) ? $breakdown['registration'] : 0;
        $loans          = isset($breakdown['loans']) ? $breakdown['loans'] : [];

        $total = $save + $share + $registration;

        foreach($loans as $row)
            $total += $row['amount'];

        if($total != $statement->amount)
            return response()->json(['status' => false, 'message' => 'Total amount of savings, shares, registration and loans does not match statement amount.']);

        $valid_loans = [];

        foreach($loans as $row){

            $loan = Loan::where([

                'user_id'   => $user->id,
                'id'        => $row['id'],
                'status'   => 'running'

            ])->first();

            if($loan && $loan->outstanding >= $row['amount']){

                $valid_loans[] = ['loan' => $loan, 'amount' => $row['amount']];

            }else{

                return response()->json(['status' => false, 'message' => $row['name'].' outsanding is less than '.$row['amount']]);
            }
            
        }

        foreach($valid_loans as $row){

        	Loan::pay($row['loan'], $row['amount'], 'Loan Repayment/'.$statement->id.'/'.$statement->bank, PAYMENT_SOURCE_BANK_TRANSFER);

            BankStatementBreakdown::create([

                'bank_statement_id' => $statement->id,
                'type'              => 'loan',
                'loan_id'           => $row['loan']->id,
                'loan_category_id'  => $row['loan']->loan_category_id,
                'amount'            => $row['amount']
            ]);
        }

        $response = Transaction::pay($user, $save, 'Savings/'.$statement->id.'/'.$statement->bank, PAYMENT_SOURCE_BANK_TRANSFER);

        if($response->status == true && isset($response->transaction_id)){

            BankStatementBreakdown::create([

                'bank_statement_id' => $statement->id,
                'type'              => 'transaction',
                'transaction_id'    => $response->transaction_id,
                'amount'            => $save
            ]);
        }
        

        if($share > 0){

            BankStatementBreakdown::create([

                'bank_statement_id' => $statement->id,
                'type'              => 'share',
                'share_id'          => optional($user->share)->id,
                'amount'            => $share
            ]);
        }

        if($registration > 0){

            BankStatementBreakdown::create([

                'bank_statement_id' => $statement->id,
                'type'              => 'registration',
                'amount'            => $registration
            ]);
        }

        $statement->update([

            'user_id'   => $user->id,
            'status'    => 'matched'
        ]);

        return response()->json(['status' => true, 'message' => 'Match complete successfully.']);
    }
}
