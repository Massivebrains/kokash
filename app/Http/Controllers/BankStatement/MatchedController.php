<?php

namespace App\Http\Controllers\BankStatement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BankStatement;
use Auth;

class MatchedController extends Controller
{
	public function index()
	{
		$company_id = Auth::user()->company_id;

		if(request()->isMethod('post') && request('query')){

			$statements        	= BankStatement::search(request('query'), $company_id, 'matched');
			$data['query']  	= request('query');
			$data['count']  	= $statements->count();

		}else{

			$statements = BankStatement::where(['company_id' => $company_id])
			->whereStatus('matched')
			->orderBy('reference', 'desc')->paginate(20);
		}

		$data['statements']     = $statements;
		$data['title']      	= 'matched statements';

		return view('bank-statements.matched', $data);
	}

    public function discarded()
    {
        $company_id = Auth::user()->company_id;

        $statements = BankStatement::where(['company_id' => $company_id])
        ->whereStatus('discarded')
        ->orderBy('updated_at', 'desc')->paginate(20);

        $data['statements']     = $statements;
        $data['title']          = 'discarded statements';

        return view('bank-statements.discarded', $data);
    }

    public function restore($id = 0)
    {
    	$statement = BankStatement::where([

    		'company_id'	=> Auth::user()->company_id,
    		'status'		=> 'discarded',
    		'id'			=> $id

    	])->first();

    	if(!$statement)
    		return redirect()->back()->with('message', 'Statement record not found');

    	$statement->update(['status' => 'pending']);

    	return redirect()->back()->with('message', 'Statement has been restored.');

    }

    public function breakdown($statement_id = 0)
    {
        $statement             = BankStatement::firstOrNew(['id' => $statement_id, 'company_id' => Auth::user()->company_id]);
        $data['statement']     = $statement;
        $data['title']         = $statement->reference.' '._c($statement->amount);

        return view('bank-statements.breakdown', $data);
    }
}
