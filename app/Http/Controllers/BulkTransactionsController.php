<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Rules\AdminPassword;
use App\Transaction;

class BulkTransactionsController extends Controller
{
    public function index()
    {
        $data['title']          = 'New Bulk Transactions';
        $data['users']          = User::query()->user()->company(_cid())->active()->orderBy('first_name', 'asc')->get();

        return view('bulk-transactions.index', $data);
    }

    public function review(Request $request)
    {
        $this->validate($request, [

            'description'	=> 'required',
            'amounts'		=> 'required|array'
        ]);

        $user_transactions = [];

        foreach (request('amounts') as $key => $value) {
            $value = (float)$value;

            if ($value == 0) {
                continue;
            }

            $user = User::find($key);

            $user_transactions[] = (object)[

                'user_id'			=> $user->id,
                'name'				=> $user->name,
                'old_balance'		=> $user->savings_account->balance,
                'amount'			=> $value
            ];
        }

        $data['title']					= 'Review Bulk Transactions';
        $data['description']			= request('description');
        $data['user_transactions']		= $user_transactions;

        return view('bulk-transactions.review', $data);
    }

    public function save(Request $request)
    {
        $this->validate($request, [

            'description'	=> 'required',
            'password'		=> ['required', new AdminPassword],
            'amounts'		=> 'required|array'
        ]);

        $errors = [];
        $sum 	= 0;
        $count	= 0;

        foreach (request('amounts') as $key => $value) {
            $user 	= User::find($key);
            $value 	= (float)$value;

            if (!$user) {
                $errors[] = 'User With ID '.$key.' Does not Exist. No transaction was created.';
                continue;
            }

            $response = Transaction::pay($user, $value, request('description'), PAYMENT_SOURCE_CASH);

            if ($response->status == false) {
                $errors[] = 'Trasaction failed for '.$user->name. 'Reason: '.$response->message;
            }

            $sum+= $value;
            $count++;
        }

        $data['upload_errors']	= $errors;
        $message				= $count.' Transacations with a total of '._c($sum).' was created successfully.';

        return redirect('bulk-transactions')->with('message', $message);
    }

    public function uploadPage()
    {
        $data['title'] = 'Upload Bulk Transactions';
        return view('bulk-transactions.upload', $data);
    }

    public function upload(Request $request)
    {
        $this->validate($request, ['transactions' => 'required|mimes:csv,txt']);
        $path = $request->file('transactions')->store('transactions');
        $file = fopen(public_path('uploads/'.$path), 'r');
        $transactions   = [];

        while (!feof($file)) {
            $transactions[] = fgetcsv($file);
        }

        $validTransactions = [];

        for ($i = 1; $i <= count($transactions)-1; $i++) {
            $row           = $transactions[$i];
            $user_id      	= (int)($row[0] ?? 0);
            $amount    		= (float)($row[1] ?? 0);

            $user = User::where(['id' => $user_id, 'company_id' => _cid()])->first();
            if (!$user) {
                return redirect()->back()->with('error', "Member with ID {$user_id} is Invalid. No changes was made.");
            }

            if ($amount == 0) {
                return redirect()->back()->with('error', "Amount cannot be zero for Member ID {$user_id} ({$user->name}). No changes was made");
            }
            $validTransactions[] = ['user' => $user, 'amount' => $amount];
        }

        foreach ($validTransactions as $row) {
            $user = $row['user'];
            $amount = $row['amount'];

            Transaction::create([
                'user_id' => $user->id,
                'company_id' => $user->company_id,
                'transaction_type_id' => TRANSACTION_TYPE_SAVINGS,
                'reference' => _reference(),
                'amount' =>  $amount,
                'description' => request('description'),
                'payment_source_id' => PAYMENT_SOURCE_CASH
            ]);

            _log('Credit Transaction of '.$amount.' created for '.$user->name.' via upload', $user);
        }

        return redirect('transactions')->with('message', 'Transactions uploaded successfully.');
    }
}
