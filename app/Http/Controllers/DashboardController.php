<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests\AdminRequest;
use App\Loan;
use App\User;
use App\LoanTransaction;
use App\Transaction;

class DashboardController extends Controller
{
    public function index(AdminRequest $request)
    {        
    	$user          = Auth::user();
        $loan          = Loan::find(1);

        $company_id     = $user->company_id;

    	$data['title'] 					= "Welcome $user->name";
    	$data['active_members']			= User::where(['company_id' => $company_id])->user()->active()->count();
    	$data['inactive_members']		= User::where(['company_id' => $company_id])->user()->inactive()->count();
    	$data['transactions']			= Transaction::where(['company_id' => $company_id])->completed()->latest()->take(10)->get();
    	$data['running_loans']			= Loan::where(['company_id' => $company_id])->running()->count();
    	$data['completed_loans']		= Loan::where(['company_id' => $company_id])->completed()->count();
        $data['total_transactions']     = Transaction::where(['company_id' => $company_id])->where('amount', '>', 0)->whereYear('created_at', date('Y'))->whereMonth('created_at', date('m'))->sum('amount');
    	$data['loan_transactions']		= LoanTransaction::where(['company_id' => $user->company_id])->completed()->orderBy('payment_date', 'desc')->take(10)->get();
        $data['new_users']              = User::where(['company_id' => $company_id])->user()->whereYear('created_at', date('Y'))->whereMonth('created_at', date('m'))->count();

    	return view('dashboard.index', $data);
    }
}
