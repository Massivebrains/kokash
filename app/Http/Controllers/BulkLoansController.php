<?php

namespace App\Http\Controllers;

use App\Loan;
use App\LoanCategory;
use App\Rules\AdminPassword;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BulkLoansController extends Controller
{
    public function index()
    {
        $data['title'] = 'New Bulk Loans Repayment';
        $data['loan_categories'] = LoanCategory::whereCompanyId(_cid())->get();
        return view('bulk-loans.index', $data);
    }

    public function review(Request $request)
    {
        if (!request('amounts')) {
            return $this->getLoans();
        }

        $this->validate($request, [
            'loan_category_id' => 'required',
            'description' => 'required',
            'amounts' => 'required|array'
        ]);

        $values = [];
        $errors = [];

        foreach (request('amounts') as $loan_id => $amount) {
            $amount = (float)$amount;
            if ($amount < 1) {
                continue;
            }

            $loan = Loan::where(['company_id' => _cid(), 'loan_category_id' => request('loan_category_id'), 'id' => $loan_id, 'status' => 'running'])->first();
            if (!$loan) {
                $errors[] = 'Loan with ID '.$loan_id.' is invalid';
                continue;
            }

            $values[] = (object)['loan' => $loan, 'amount' => $amount];
        }

        if (!empty($errors)) {
            return redirect()->back()->with('error', implode(', ', $errors));
        }

        $data['title'] = 'Review Bulk Loan Repayments';
        $data['description'] = request('description');
        $data['loan_category'] = LoanCategory::find(request('loan_category_id'));
        $data['values'] = $values;

        return view('bulk-loans.review', $data);
    }

    public function getLoans()
    {
        $loans = Loan::where([
            'company_id' => _cid(),
            'loan_category_id' => request('loan_category_id'),
            'status' => 'running'
        ])
        ->get();

        $data['title'] = 'Review Bulk Loan Repayments';
        $data['loans'] = $loans;
        $data['loan_category_id'] = request('loan_category_id');
        $data['loan_categories'] = LoanCategory::whereCompanyId(_cid())->get();

        return view('bulk-loans.index', $data);
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'loan_category_id' => 'required',
            'description'	=> 'required',
            'password'		=> ['required', new AdminPassword],
            'amounts'		=> 'required|array'
        ]);

        $errors = [];
        $sum 	= 0;
        $count	= 0;


        foreach (request('amounts') as $loan_id => $amount) {
            $loan = Loan::where(['company_id' => _cid(), 'loan_category_id' => request('loan_category_id'), 'id' => $loan_id, 'status' => 'running'])->first();
            if (!$loan) {
                $errors[] = 'Loan with ID '.$loan_id.' is invalid';
                continue;
            }

            $response = Loan::pay($loan, $amount, request('description'), PAYMENT_SOURCE_CASH);
            if ($response->status) {
                $sum+= $amount;
                $count++;
            } else {
                $errors[] = 'Loan repyament failed for '.$loan->user->name. 'Reason: '.$response->message;
            }
        }

        $upload_errors	= implode(',', $errors);
        $sum = _c($sum);
        return redirect('loans')->with('message', "Loan repyament completed. Total amount repaid: $sum members affected: $count {$upload_errors}");
    }

    public function uploadPage()
    {
        $data['title'] = 'Upload Loan Repayments';
        $data['loan_categories'] = LoanCategory::whereCompanyId(_cid())->get();
        return view('bulk-loans.upload', $data);
    }

    public function upload(Request $request)
    {
        $this->validate($request, ['loans' => 'required|mimes:csv,txt', 'loan_category_id' => 'required']);
        $path = $request->file('loans')->store('loans');
        $file = fopen(public_path('uploads/'.$path), 'r');
        $loans   = [];

        while (!feof($file)) {
            $loans[] = fgetcsv($file);
        }

        $validloans = [];

        for ($i = 1; $i <= count($loans)-1; $i++) {
            $row           = $loans[$i];
            $user_id      	= (int)($row[0] ?? 0);
            $amount    		= (float)($row[1] ?? 0);

            $user = User::where(['id' => $user_id, 'company_id' => _cid()])->first();
            if (!$user) {
                return redirect()->back()->with('error', "Member with ID {$user_id} is Invalid. No changes was made.");
            }

            if ($amount < 1) {
                return redirect()->back()->with('error', "Loan repayment amount cannot be less than 1 for Member ID {$user_id} ({$user->name}). No changes was made");
            }

            $loan = Loan::where([
                'company_id' => _cid(),
                'loan_category_id' => request('loan_category_id'),
                'user_id' => $user->id,
                'status' => 'running'
            ])->first();

            if (!$loan) {
                return redirect()->back()->with('error', "Loan for Member ID {$user_id} ({$user->name}) is invalid. No changes was made");
            }

            if ($amount > (float)$loan->outstanding) {
                return redirect()->back()->with('error', "Amount is more than what member is oweing for loan. Member ID {$user_id} ({$user->name}) No changes was made.");
            }

            $validloans[] = ['loan' => $loan, 'amount' => $amount];
        }

        foreach ($validloans as $row) {
            $loan = $row['loan'];
            $amount = $row['amount'];
            Loan::pay($loan, $amount, request('description'), PAYMENT_SOURCE_CASH);
        }

        return redirect('loans')->with('message', 'Loan repayments uploaded successfully.');
    }
}
