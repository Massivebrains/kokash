<?php

namespace App\Http\Controllers;

require_once(base_path('app/Paystack/Paystack.php'));


use Illuminate\Http\Request;
use App\Order;
use App\OrderDetail;
use App\DebitCard;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class PaystackController extends Controller
{
    private $base_url = 'https://api.paystack.co/';

    private function apiPost($endpoint = '', $payload = [])
    {
        try{

            $client     = new Client();
            $url        = $this->base_url.$endpoint;

            $private_key = env('PAYSTACK_STATUS') == 'test' ? env('PAYSTACK_TEST_SECRET') : env('PAYSTACK_LIVE_SECRET');

            $response = $client->post($url, [

                'headers'       => [ 'Authorization' => 'Bearer '.$private_key],
                'content-type'  => 'application/json',
                'Accept'        => 'application/json',
                'form_params'   => $payload
            ]);

            if((int)$response->getStatusCode() == 200){

                return json_decode($response->getBody());
            }

            return (object)['status' => false, 'data' => 'Transaction was not successful'];
            

        }catch(RequestException $e){

            return (object)['status' => false, 'data' => $e->getMessage()];

        }catch(\Exception $e){

            return (object)['status' => false, 'data' => $e->getMessage()];
        }
                
    }

    public function pay(Request $request, $order_id = 0)
    {
        $order = Order::find($order_id);

        if(!$order) return view('paystack.error');

        $data['order']  = $order;

        return view('paystack.pay-manual', $data);
    }

    public function requery($order_id = 0, $transaction_reference = '')
    {
        $order = Order::find($order_id);

        if(!$order || !$transaction_reference) return response()->json(['status' => false, 'data' => 'Transaction Not found.']);

        if($order->status == 'completed'){

            return response()->json(['status' => true, 'data' => 'Payment Successful']);
        }

        $private_key = env('PAYSTACK_STATUS') == 'test' ? env('PAYSTACK_TEST_SECRET') : env('PAYSTACK_LIVE_SECRET');

        $paystack = new \Paystack($private_key);

        try{

            $paystackTransaction = $paystack->transaction->verify(['reference' => $transaction_reference]);

            if((bool)$paystackTransaction->status == true && $paystackTransaction->data->status == 'success'){

                $authorization = $paystackTransaction->data->authorization;
                $customer      = $paystackTransaction->data->customer;

                DebitCard::updateOrCreate(['user_id' => $order->user->id, 'bin' => $authorization->bin], [

                    'user_id'               => $order->user->id,
                    'company_id'            => $order->user->company_id,
                    'email'                 => $customer->email,
                    'authorization_code'    => $authorization->authorization_code,
                    'bin'                   => $authorization->bin,
                    'last4'                 => $authorization->last4,
                    'exp_month'             => $authorization->exp_month,
                    'exp_year'              => $authorization->exp_year,
                    'channel'               => $authorization->channel,
                    'card_type'             => $authorization->card_type,
                    'bank'                  => $authorization->bank,
                    'country_code'          => $authorization->country_code,
                    'brand'                 => $authorization->brand,
                    'reusable'              => (bool)$authorization->reusable,
                    'signature'             => $authorization->signature
                ]);
                

                $order->update([

                    'transaction_reference' => $paystackTransaction->data->reference,
                    'transaction_date'      => date('Y-m-d H:i:s'),
                    'status'                => 'paid'
                ]);

                OrderDetail::where(['order_id' => $order->id])->update(['status' => 'paid']);

                return response()->json([

                    'status' => true,
                    'data'   => $paystackTransaction
                ]);

            }

            return response()->json(['status' => false, 'data' => 'Payment Unsuccessful']);

        }catch(Exception $e){

            return response()->json(['status' => false, 'data' => 'Transaction was not successful']);
        }
        
    }

    public function charge(Request $request, $order_id = 0, $debit_card_id = 0)
    {

        try{

            $order   = Order::find($order_id);

            if(!$order) return response()->json(['status' => false, 'data' => 'Transaction Not found.']);

            if($order->status == 'completed'){

                return response()->json(['status' => true, 'data' => 'Payment Successful']);
            }

            $debitcard   = DebitCard::where(['user_id' => $order->user->id, 'id' => $debit_card_id, 'reusable' => 1])->first();

            if(!$debitcard) return response()->json(['status' => false, 'data' => 'Invalid Debit Card']);


            $payload = [

                'authorization_code'   => $debitcard->authorization_code,
                'email'                => $debitcard->email,
                'amount'               => $order->kobo,
                'subaccount'           => $order->sub_account
            ];

            $response = $this->apiPost('transaction/charge_authorization', $payload);

            if((bool)$response->status == true && isset($response->data->status) && $response->data->status == 'success'){

                $authorization = $response->data->authorization;
                $customer      = $response->data->customer;

                DebitCard::updateOrCreate(['user_id' => $order->user->id, 'bin' => $authorization->bin], [

                    'user_id'               => $order->user->id,
                    'company_id'            => $order->user->company_id,
                    'email'                 => $customer->email,
                    'authorization_code'    => $authorization->authorization_code,
                    'bin'                   => $authorization->bin,
                    'last4'                 => $authorization->last4,
                    'exp_month'             => $authorization->exp_month,
                    'exp_year'              => $authorization->exp_year,
                    'channel'               => $authorization->channel,
                    'card_type'             => $authorization->card_type,
                    'bank'                  => $authorization->bank,
                    'country_code'          => $authorization->country_code,
                    'brand'                 => $authorization->brand,
                    'reusable'              => (bool)$authorization->reusable,
                    'signature'             => $authorization->signature
                ]);
                

                $order->update([

                    'transaction_reference' => $response->data->reference,
                    'transaction_date'      => date('Y-m-d H:i:s'),
                    'status'                => 'paid'
                ]);


                OrderDetail::where(['order_id' => $order->id])->update(['status' => 'paid']);

                return response()->json([

                    'status' => true,
                    'data'   => 'Payment Successful'
                ]);

            }

            return response()->json(['status' => false, 'data' => 'Transaction was not Successful']);

        }catch(Exception $e){

            return response()->json(['status' => false, 'data' => 'Transction was not Successful']);
        }
        
    }


    public function cards(Request $request, $order_id = 0)
    {
        $order = Order::find($order_id);

        if(!$order) die;

        $data['cards']  = $order->user->debitcards;
        $data['order']  = $order;

        return view('paystack.pay-manual', $data);
        //return view('paystack.cards', $data);
    }
}
