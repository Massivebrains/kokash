<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Notification;
use App\NotificationRead;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    public function index()
    {
        $user           = Auth::user();
        $notifications = Notification::where([

            'company_id'    => $user->company_id,
            'user_id'       => $user->id

        ])->latest()->take(30)->get();

        foreach ($notifications as $row) {
            $row->read = 1;
            $where = ['notification_id' => $row->id, 'user_id' => $user->id];
            $count = NotificationRead::where($where)->count();

            if ($count == 0) {
                $row->read = 0;
                NotificationRead::create($where);
            }
        }

        return response()->json(['status' => true, 'data' => $notifications]);
    }
}
