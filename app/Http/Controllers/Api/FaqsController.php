<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faq;
use Auth;

class FaqsController extends Controller
{
    public function index()
    {
    	$faqs = Faq::where(['company_id' => Auth::user()->company_id])->get();

    	$response = [];

    	foreach($faqs as $row){

    		$response[] = ['title' => $row->question, 'content' => $row->answer];
    	}
    	
    	return response()->json(['status' => true, 'data' => $response]);
    }
}
