<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Transaction;
use App\Loan;
use App\LoanTransaction;

class TransactionsController extends Controller
{
	public function index()
	{
		$transactions = Transaction::by(Auth::user())->orderBy('id', 'desc')->get();

		return response()->json(['status' => true, 'data' => Transaction::transform($transactions)]);
	}
}
