<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Loan;
use App\LoanTransaction;

class LoansController extends Controller
{
    public function index()
    {
    	$loans = Loan::by(Auth::user())->latest()->get();

    	return response()->json(['status' => true, 'data' => Loan::transform($loans)]);
    }

    public function transactions($id = 0)
    {
    	$loan = Loan::by(Auth::user())->find($id);

    	if(!$loan) return response()->json(['status' => false, 'data' => 'We could not find this loan entry.']);

    	return response()->json(['status' => true, 'data' => LoanTransaction::transform($loan->loan_transactions)]);
    }
}
