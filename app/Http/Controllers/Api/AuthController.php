<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Bank;
use App\Company;
use App\Http\Requests\ApiLoginRequest;

class AuthController extends Controller
{
    public function login(ApiLoginRequest $request)
    {           
        $company = Company::where(['name' => request('company')])->first();
        if(!$company) return response()->json(['status' => false, 'data' => 'Invalid Cooperative Company Name.']);

        // $user = User::where(['phone' => _to_phone(request('phone')), 'company_id' => $company->id])->first();

        // if($user && $user->change_password_on_login == 0){

        //     Auth::loginUsingId($user->id);

        //     $user                   = User::profile($user);
        //     $user->last_login_at    = date('Y-m-d H:i:s');

        //     return response()->json(['status' => true, 'data' => User::profile($user)]);
        // }

        if(Auth::attempt(['company_id'  => $company->id, 'phone' => _to_phone(request('phone')), 'password' => request('password')])){

            $user = Auth::user();
        

            if($user->status == 'inactive'){

            	Auth::logout();
                return response()->json(['status' => false, 'data' => 'Your account is currently inactive. Contact your managment to activate your account.' ], 400);
                
            }

            $last_login_at = $user->last_login_at ? true : false;

            $user->update(['last_login_at' => date('Y-m-d H:i:s')]);
            _log('New user Login.');

            $user                   = User::profile($user);
            $user->last_login_at    = $last_login_at;

            return response()->json(['status' => true, 'data' => User::profile($user)]);
        }

        return response()->json(['status' => false, 'data' => 'Invalid Phone or Password']);
    }
}
