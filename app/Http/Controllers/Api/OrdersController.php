<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\UserProduct;
use App\OrderDetail;
use App\ProcessOrder;
use Auth;

class OrdersController extends Controller
{
    public function order(Request $request)
    {
    	$user = Auth::user();

    	$order = Order::create([

    		'user_id'			=> $user->id,
    		'company_id'		=> $user->company_id,
    		'order_reference'	=> 'K'.mt_rand(10000, 99999),
            'payment_source_id' => PAYMENT_SOURCE_CASH
        ]);

    	$sub_total = 0;

    	// foreach((array)request('products') as $row){

     //        $row = (object)$row;

     //        $product = UserProduct::find($row->id);

     //        if(!$product) break;

     //        OrderDetail::create([

     //         'order_id'				=> $order->id,
     //         'type'					=> $product->type,
     //         'for'					=> $product->for,
     //         'user_id'				=> $product->for_user_id,
     //         'loan_id'				=> $product->loan_id,
     //         'amount'				=> (float)$row->amount
     //     ]);

     //        $sub_total+= $row->amount;
     //    }

        //dd(request('products'));

        foreach((array)request('products') as $row){

           $row = (object)$row;

            OrderDetail::create([

                'order_id'              => $order->id,
                'type'                  => 'savings',
                'for'                   => 'self',
                'user_id'               => Auth::user()->id,
                'loan_id'               => 0,
                'amount'                => (float)$row->amount
            ]);

            $sub_total+= $row->amount;
        }

        if($sub_total == 0){

            $order->delete();

            return response()->json(['status' => false, 'data' => 'This transaction has expired. Please try again.']);

        }

        $transaction_charge = 0; //_paystack_transaction_charge($sub_total);
        
        $order->update([

            'sub_total'             => $sub_total,
            'transaction_charge'    => $transaction_charge,
            'total'                 => $sub_total + $transaction_charge,
            'sub_account'           => optional($order->company->setting)->paystack_sub_account_code
        ]);

        $order = Order::where(['id' => $order->id])->with('user')->first();

        return response()->json(['status' => true, 'data' => $order]);
    }

    public function process(Request $request, $order_id = 0)
    {

        $order = Order::find($order_id);

        if(!$order || $order->status != 'paid') 
            return response()->json(['status' => false, 'data' => $message]);

        ProcessOrder::process($order);

        return response()->json(['status' => true, 'data' => 'Transaction successful.']);
    }
}
