<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Statement;
use App\Http\Requests\StatementRequest;
use Auth;

class StatementsController extends Controller
{
    public function index()
    {
    	$statements = Statement::where(['user_id' => Auth::user()->id])->latest()->get();
    	
    	return response()->json(['status' => true, 'data' => $statements]);
    }

    public function save(StatementRequest $request)
    {
    	Statement::create([

    		'user_id'		=> Auth::user()->id,
    		'company_id'	=> Auth::user()->company_id,
    		'start_date'	=> request('start_date'),
    		'end_date'		=> request('end_date')
    	]);

        activity()->performedOn(Auth::user())->causedBy(Auth::user())->log('New Statement request');

    	return response()->json(['status' => true, 'data' => 'Statement scheduled succesfully. You would be notified when your statement is ready.']);
    }
}
