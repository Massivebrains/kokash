<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Bank;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function profile()
    {
        return response()->json([

            'status'    => true, 
            'data'      => User::profile(Auth::user())
        ]);

    }
    
    public function uploadPhoto(Request $request, $field = '')
    {
        if(!request('image')) return response()->json(['status' => false, 'data' => 'Upload Failed. No Image sent.']);

        $response = _cloudinary(request('image'), '', true);

        if($response->link == null){

            return response()->json(['status' => false, 'data' => $response->error]);

        }else{

            Auth::user()->update(['photo' => $response->link]);

            return response()->json(['status' => true, 'data' => User::profile(Auth::user())]);
        }
    }

    public function editProfile(Request $request)
    {	
    	$user = Auth::user();

    	$user->update([

    		'first_name'		=> request('first_name', $user->first_name),
    		'last_name'			=> request('last_name', $user->last_name),
    		'email'				=> request('email', $user->email),
    		'phone'				=> request('phone', $user->phone),
    		'sex'				=> request('sex', $user->sex),
    		'marital_status'	=> request('marital_status', $user->marital_status),
    		'address'			=> request('address', $user->address),
    		'occupation'		=> request('occupation', $user->occupation),
    		'employer'			=> request('employer', $user->employer),
    		'employer_address'	=> request('employer_address', $user->employer_address),
    		'next_of_kin'		=> request('next_of_kin', $user->next_of_kin)
    	]);

    	if(request('bank_id') && request('bank_account_name') && request('bank_account_number')){

            $bank = Bank::find(request('bank_id'));

            if($bank){

                if(!is_nuban_valid($bank->code.request('bank_account_number')))
                    return response()->json(['status' => false, 'data' => 'Invalid Bank Account Number']);
                
                $user->savings_account->update([

                    'bank_id'               => request('bank_id'),
                    'bank_account_number'   => request('bank_account_number'),
                    'bank_account_name'     => request('bank_account_name'),
                ]);
            }

        }

        return response()->json(['status' => true, 'data' => User::profile($user)]);
    }

    public function banks()
    {
    	return response()->json(['status' => true, 'data' => Bank::get()]);
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [

            'password'                  => 'required',
            'new_password'              => 'required|min:6',
            'password_confirmation'     => 'required'
        ]);

        if (!Hash::check(request('password'), Auth::user()->password)) {

            _log('Attempted to change password but current password is incorrect');
            return response()->json(['status' => false, 'data' => 'Current Password is incorrect.']);
        }

        if(request('new_password') != request('password_confirmation')){

           _log('Attempted to change password but new password does not match password confirmation');
           return response()->json(['status' => false, 'data' => 'New Password does not match Password Confirmation']);
       }

       if(Hash::check(request('new_password'), Auth::user()->password)){

            return response()->json(['status' => false, 'data' => 'Old Password must be different from new password']);
       }

       Auth::user()->update(['password' => bcrypt(request('new_password'))]);
       _log('Password has been successfully changed');

       return response()->json(['status' => true, 'data' => 'Password has been changed successfully.']);
   }
}
