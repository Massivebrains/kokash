<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Loan;
use App\LoanTransaction;
use App\Transaction;
use App\Bank;

class DashboardController extends Controller
{
    public function index()
    {
    	$user                      = Auth::user();
        $next_loan_transaction     = LoanTransaction::by($user)->pending()->orderBy('date', 'asc')->take(1)->first();
    	$transactions              = Transaction::by($user)->orderBy('id', 'desc')->take(20)->get();

        return response()->json([

    		'status'	=> true,
    		'data'		=> [

                'company'                       => $user->company->description,
    			'savings_account_number'        => optional($user->savings_account)->account_number,
                'savings_balance'               => _c($user->savings_account->balance),
                'active_loans_count'            => Loan::by($user)->running()->count(),
    			'loan_outstanding_balance'		=> _c(LoanTransaction::outstanding($user)),
    			'loan_next_payment_date'	    => $next_loan_transaction ? _d($next_loan_transaction->date, false) : '--',
    			'recent_transactions'	        => Transaction::transform($transactions),
                'savings_graph'                 => Transaction::savingsGraph($user)

    		]
    	]);
    }
}
