<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\UserProduct;
use App\Order;
use App\OrderDetail;
use App\Loan;
use App\Transaction;
use App\LoanTransaction;

class PayController extends Controller
{
	public function form()
	{
		$user = User::whereApiToken(request('api_token'))->first();

		if(!$user)
			return view('webview.error');

		Auth::loginUsingId($user->id);

		$products = UserProduct::getProducts($user);

		$data['user']		= $user;
		$data['products']	= $products;

		return view('webview.pay.form', $data);
	}

	public function review()
	{
		$user 		= Auth::user();
		$products 	= request('products');

		if(!$products)
			return view('webview.error');

		$order = Order::create([

			'user_id'			=> $user->id,
			'company_id'		=> $user->company_id,
			'order_reference'	=> 'K'.mt_rand(10000, 99999),
			'payment_source_id' => PAYMENT_SOURCE_CASH,
			'status'			=> 'cart'
		]);

		$sub_total = 0;

		foreach($products as $id => $amount){

			$product = UserProduct::find($id);

			if(!$product) continue;
			if($amount < 1) continue;

			OrderDetail::create([

				'order_id'	=> $order->id,
				'type'		=> $product->type,
				'for'		=> $product->for,
				'user_id'	=> $product->for_user_id,
				'loan_id'	=> $product->loan_id,
				'amount'	=> (float)$amount
			]);
			
			$sub_total+=(float)$amount;

		}

		if($sub_total == 0){

			$order->delete();
			return redirect()->back();
		}

        $transaction_charge = 0; //_paystack_transaction_charge($sub_total);
        
        $order->update([

        	'sub_total'             => $sub_total,
        	'transaction_charge'    => $transaction_charge,
        	'total'                 => $sub_total + $transaction_charge,
        	'sub_account'           => optional($order->company->setting)->paystack_sub_account_code
        ]);

        $data['order']	= $order;

        if($sub_total < 1)
        	return redirect()->back();
        
        return view('webview.pay.review', $data);
    }

    public function confirm()
    {
    	$order = Order::find(request('order_id'));

    	if(!$order)
    		return view('webview.error');

    	$order->update(['status' => 'pending']);

    	$data['order']	= $order;

    	return view('webview.pay.review', $data);
    }
}
