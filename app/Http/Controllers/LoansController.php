<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Loan;
use App\LoanTransaction;
use App\User;
use App\Http\Requests\AdminRequest;
use App\Http\Requests\LoanRequest;
use App\Http\Requests\DisburseOfflineRequest;
use \Carbon\Carbon;
use App\Events\LoanDisbursed;
use App\PaymentSource;
use App\LoanCategory;
use App\Rules\AdminPassword;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LoansController extends Controller
{
    public function index(AdminRequest $request)
    {
        if (request()->isMethod('post') && request('query')) {
            $loans          = Loan::search(request('query'), Auth::user()->company_id);
            $data['query']  = request('query');
            $data['count']  = $loans->count();
        } else {
            $loans = Loan::where(['company_id' => Auth::user()->company_id])->orderBy('id', 'desc')->where('status', '!=', 'cancelled')->paginate(50);
        }

        $data['title']  = 'loans';
        $data['active'] = 'loan';
        $data['loans']  = $loans;

        return view('loans.index', $data);
    }

    public function form()
    {
        $data['title']              = 'New Loan';
        $data['users']              = User::where(['type' => 'user', 'status' => 'active', 'company_id' => _cid()])->orderBy('first_name')->get();
        $data['loan_categories']    = LoanCategory::whereCompanyId(Auth::user()->company_id)->get();

        return view('loans.form', $data);
    }

    public function review(LoanRequest $request)
    {
        $loan_category_id    = request('loan_category_id');
        $principal           = request('principal');
        $rate                = request('rate');
        $time                = (int)request('time');
        $start_at            = request('start_at');
        $user                = User::find(request('user_id'));
        $repayment_type      = request('repayment_type');
        $auto_disburse       = request('auto_disburse');

        $loan_transactions = LoanTransaction::preCalculate($principal, $rate, $time, $start_at, $repayment_type);
        $data['loan_category']      = LoanCategory::find($loan_category_id);
        $data['principal']          = $principal;
        $data['rate']               = $rate;
        $data['time']               = $time;
        $data['start_at']           = $start_at;
        $data['user']               = $user;
        $data['loan_transactions']  = $loan_transactions;
        $data['repayment_type']     = $repayment_type;
        $data['auto_disburse']      = $auto_disburse;

        $data['title']      = 'Review Loan :: '.$user->name;
        $data['title']      = 'loans';

        return view('loans.review', $data);
    }

    public function save(AdminRequest $request)
    {
        DB::transaction(function () {
            $principal      = request('principal');
            $rate           = request('rate');
            $time           = request('time');
            $start_at       = request('start_at');
            $repayment_type = request('repayment_type');
            $user           = User::find(request('user_id'));

            $loan_transactions  = LoanTransaction::preCalculate($principal, $rate, $time, $start_at, $repayment_type);
            $total_interest     = $loan_transactions->sum('interest');
            $principal          = request('principal');
            $monthly_payment    = $loan_transactions->first();
            $disbursed          = $principal;
            $total_amount       = $principal + $total_interest;

            if ($repayment_type == 'deduct_interest') {
                $disbursed    = $principal - $total_interest;
                $total_amount = $principal;
            }

            $loan_category = LoanCategory::firstOrNew(['id' => request('loan_category_id')]);

            $loan = Loan::create([
                'user_id'           => $user->id,
                'company_id'        => $user->company_id,
                'loan_category_id'  => $loan_category->id,
                'name'              => $loan_category->name,
                'reference'         => _loan_reference(),
                'principal'         => $principal,
                'disbursed'         => $disbursed,
                'rate'              => request('rate'),
                'time'              => request('time'),
                'interest'          => $total_interest,
                'amount'            => $total_amount,
                'outstanding'       => $total_amount,
                'monthly_payment'   => $monthly_payment->amount,
                'type'              => 'simple',
                'start_at'          => Carbon::createFromFormat('m-Y', request('start_at'))->format('Y-m-d'),
                'disbursed_at'          => date('Y-m-d'),
                'disbursement_notes'    => 'Disbursed by '.Auth::user()->name,
                'status'                => 'running'
            ]);

            $sum = $loan_transactions->sum('amount');

            foreach ($loan_transactions as $row) {
                $sum-= $row->amount;
                LoanTransaction::create([
                    'loan_id'       => $loan->id,
                    'user_id'       => $user->id,
                    'company_id'    => $user->company_id,
                    'reference'     => _reference(),
                    'principal'     => $row->principal,
                    'interest'      => $row->interest,
                    'amount'        => $row->amount,
                    'paid'          => 0,
                    'outstanding'   => $row->amount,
                    'balance'       => $sum,
                    'date'          => Carbon::parse($row->date)->format('Y-m-d'),
                    'status'        => 'pending'
                ]);
            }
            event(new LoanDisbursed($loan));
        });

        $user = User::find(request('user_id'));
        _log('New Loan created for '.$user->name, $user);

        return redirect('loan-form')->with('message', 'Loan created succesfully.');
    }

    public function loan(AdminRequest $request, $id = 0, $loan_transaction_id = 0)
    {
        $loan = Loan::where(['id' => $id, 'company_id' => _cid()])->first();

        if (!$loan) {
            return redirect('loan');
        }

        $data['title']              = 'Loan :: '.$loan->user->name;
        $data['active']             = 'loans';
        $data['loan']               = $loan;
        $data['sources']            = PaymentSource::get();
        $data['form']               = request('form') ? true : false;
        $data['loan_transaction']   = LoanTransaction::firstOrNew(['id' => $loan_transaction_id]);

        return view('loans.loan', $data);
    }

    public function disburseOffline(DisburseOfflineRequest $request)
    {
        $loan = Loan::find(request('id'));

        if (!$loan || $loan->status != 'pending') {
            session()->flash('error', 'Only Pending loans can be disbursed');

            return redirect()->back();
        }

        $loan->update([

            'disbursed_at'          => date('Y-m-d', strtotime(request('disbursed_at'))),
            'disbursement_notes'    => request('disbursement_notes'),
            'status'                => 'running'
        ]);

        $loan->refresh();

        event(new LoanDisbursed($loan));
        _log('Loan Disbursed for '.$loan->user->name, $loan->user);

        return redirect('loans')->with('message', 'Loan has been marked as disbursed succesfully.');
    }

    public function completeLoanTransaction(Request $request)
    {
        $this->validate($request, [

            'loan_id'               => 'required|numeric',
            'payment_source_id'     => 'required|numeric',
            'amount'                => 'required|numeric',
            'password'              => ['required', new AdminPassword]
        ]);

        $loan       = Loan::find(request('loan_id'));
        $amount     = (float)request('amount');
        $response   = Loan::pay($loan, $amount, request('description'), request('payment_source_id'));

        if ($response->status == false) {
            return redirect()->back()->with('error', $response->message);
        }

        return redirect('loans')->with('message', $response->message);
    }
}
