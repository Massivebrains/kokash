<?php

namespace App\Http\Controllers\Cron;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\DbDumper\Databases\MySql;

class DBDumpController extends Controller
{
	public function __invoke()
	{
		MySql::create()
		->setDbName('staging')
		->setUserName('root')
		->setPassword('cynosure')
		->dumpToFile('/var/www/kokash-db-dumps/'.now()->format('Y-m-d H:i:s').'-dump.sql');
	}
}
