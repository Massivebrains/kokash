<?php

namespace App\Http\Controllers\Cron;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserProduct;

class UserProductsController extends Controller
{
    public function truncate()
    {
    	UserProduct::query()->truncate();
    }
}
