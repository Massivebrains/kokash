<?php

namespace App\Http\Controllers\Cron;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LoanTransaction;
use App\Transaction;
use App\Setting;

class LoansController extends Controller
{
    public function fine()
    {
    	$defaultedLoanTransactions = LoanTransaction::pending()->where(['fine_transaction_id' => null])->whereDate('date', '<', date('Y-m-d'))->get();

    	foreach($defaultedLoanTransactions as $row){

    		$setting 		= Setting::where(['company_id' => $row->company_id])->first();
    		$fine 			= $setting ? $setting->loan_fine_fee : 0;

    		$month 		= _month($row->date);
    		$loan_name	= $row->loan->name;

    		$description = "Fine on Default of $row->reference ($loan_name) for the month of $month";

    		$transaction = Transaction::create([

    			'user_id'				=> $row->user_id,
    			'company_id'			=> $row->company_id,
    			'transaction_type_id'	=> TRANSACTION_TYPE_LOAN_DEFAULTING_FEE,
    			'reference'				=> _reference(),
    			'amount'				=> -$fine,
    			'description'			=> $description,
    			'payment_source_id'		=> PAYMENT_SOURCE_FINE,
    			'status'				=> 'completed'
    		]);

    		LoanTransaction::where(['id' => $row->id])->update(['fine_transaction_id' => $transaction->id, 'fine_date' => date('Y-m-d H:i:s')]);
    	}
    }
}
