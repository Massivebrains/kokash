<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Transaction;
use App\Loan;
use App\LoanTransaction;
use App\Http\Requests\AdminRequest;
use App\Http\Requests\BankInformationRequest;
use Spatie\Activitylog\Models\Activity;
use App\Beneficiary;
use App\Jobs\SendEmailJob;
use App\Jobs\SendSMSJob;
use App\Notification;
use App\Order;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{
    public function index(AdminRequest $request, $id = 0)
    {
        $user = User::where(['id' => $id])->user()->first();

        if (!$user) {
            return redirect('members');
        }

        $data['active']			     = 'members';
        $data['title']			     = $user->name;
        $data['user']			     = $user;
        $data['users']               = User::user()->active()->where(['company_id' => Auth::user()->company_id])->where('id', '!=', $user->id)->get();
        $data['transactions']	     = Transaction::by($user)->orderBy('id', 'desc')->paginate(20);
        $data['outstanding_loan']    = LoanTransaction::outstanding($user);
        $data['activities']          = Activity::where(['subject_id' => $user->id])->orWhere(['causer_id' => $user->id])->orderBy('created_at', 'desc')->paginate(20);
        $data['loans']               = Loan::by($user)->orderBy('id', 'desc')->paginate(10);
        $data['orders']              = Order::by($user)->pending()->orderBy('id', 'desc')->paginate(10);
        $data['share_activities']    = Activity::where(['subject_id' => $user->share->id, 'subject_type' => 'App\Share'])->orderBy('created_at', 'desc')->get();
        $data['notifications']    = Notification::by($user)->orderBy('id', 'desc')->paginate(20);

        return view('member.index', $data);
    }

    public function bank(BankInformationRequest $request, $id = 0)
    {
        $user = User::where(['id' => $id, 'company_id' => _cid()])->first();

        if (!$user) {
            return redirect('members');
        }

        $user->savings_account->update([

            'bank_id'               => request('bank_id'),
            'bank_account_number'   => request('bank_account_number'),
            'bank_account_name'     => strtoupper(request('bank_account_name')),
        ]);

        _log('Updated Member bank information '.$user->name, $user);
        return redirect('member-info/'.$user->id)->with('message', 'Bank information modified succesfully.');
    }

    public function status(Request $request, $id = 0)
    {
        $user = User::find($id);

        if (!$user) {
            abort(404);
        }

        $subject = $body = '';

        if ($user->status == 'active') {
            $user->update(['status' => 'inactive']);

            $subject    = 'Suspension notice for '.$user->email;
            $body       = view('emails.member-inactive', ['user' => $user])->render();
        } else {
            $user->update(['status' => 'active']);

            $subject    = 'Account Activated';
            $body       = view('emails.member-active', ['user' => $user])->render();
        }

        SendEmailJob::dispatch($user, $subject, $body);
        return redirect('member-info/'.$user->id)->with('message', 'Account updated succesfully.');
    }

    public function saveBeneficiary(Request $request)
    {
        $this->validate($request, ['partner_id' => 'required']);

        $data = ['user_id' => request('user_id'), 'partner_id' => request('partner_id')];

        Beneficiary::updateOrCreate($data, $data);

        session()->flash('message', 'Beneficaries updated succesfully.');

        return redirect('member-info/'.request('user_id'));
    }

    public function removeBeneficiary($id = 0)
    {
        Beneficiary::destroy($id);

        return redirect()->back()->with('message', 'Beneficiary removed succesfully.');
    }

    public function saveMonthlySavings()
    {
        $user = User::find(request('user_id'));

        if (!$user) {
            return redirect()->back()->with('message', 'Member not found. No changes was made.');
        }

        $user->savings_account->update(['monthly_savings' => request('monthly_savings')]);

        return redirect()->back()->with('message', 'Monthly Savings updated succesfully.');
    }

    public function changePassword()
    {
        if (request('password') != request('password_confirmation')) {
            return redirect()->back()->with('error', 'New Password does not match Password confirmation');
        }

        $user = User::where(['company_id' => _cid(), 'id' => request('user_id')])->first();

        if (!$user) {
            return redirect()->back()->with('error', 'Member not found.');
        }

        $user->update(['api_token' => str_random(16), 'password' => bcrypt(request('password'))]);

        $subject    = 'Your password was changed';
        $body       = view('emails.password-changed', ['user' => $user])->render();

        _email($user->email, $subject, $body);
        $sms = _password_sms(request('password'), $user->phone, $user->company->name);
        SendSMSJob::dispatch($user, $sms);

        return redirect()->back()->with('message', 'Password changed successfully.');
    }

    public function deleteMember(Request $request, $id = 0)
    {
        if (!User::find($id) || Auth::user()->id != 1) {
            return redirect('logout');
        }

        User::find($id)->delete();
        Transaction::where(['user_id' => $id])->delete();
        Loan::where(['user_id' => $id])->delete();
        LoanTransaction::where(['user_id' => $id])->delete();

        return redirect()->back()->with('message', 'User and all related transactions has been succesfully deleted');
    }
}
