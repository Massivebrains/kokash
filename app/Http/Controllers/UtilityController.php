<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;

class UtilityController extends Controller
{
    public function states($country_id = 0)
    {
        foreach(State::where(['country_id' => $country_id])->get() as $row){

            echo "<option value='{$row->id}''>{$row->name}</option>";
        }
    }
}
