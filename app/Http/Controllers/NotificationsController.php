<?php

namespace App\Http\Controllers;

use App\Notification;
use App\Http\Requests\NotificationRequest;
use App\Http\Requests\AdminRequest;
use App\User;
use App\Jobs\SendSMSJob;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    public function index(AdminRequest $request, $id = 0)
    {
        $notifications = Notification::where(['company_id' => Auth::user()->company_id])->latest()->paginate(50);

        $data['title']			= 'notifications';
        $data['notifications']	= $notifications;

        return view('notifications.index', $data);
    }

    public function form($id = 0)
    {
        $data['notification']	= Notification::firstOrNew(['id' => $id]);
        $data['title']			= 'Notification';

        return view('notifications.form', $data);
    }

    public function save(NotificationRequest $request, $id = 0)
    {
        if (request('phone')) {
            $user = User::where(['company_id' => _cid(), 'phone' => request('phone')])->first();
            SendSMSJob::dispatch($user, request('notification'));
        }

        _log('Created new Notification - '.request('title'));
        return redirect('notifications')->with('message', 'Notification saved successfully.');
    }

    public function switchNotification()
    {
        $company = Auth::user()->company;
        $company->update(['notifications' => $company->notifications === 'on' ? 'off' : 'on']);
        return redirect('/notifications')->with('message', 'Notifications settings updated successfully');
    }
}
