<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Company;
use App\Jobs\SendSMSJob;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
        if (User::count() == 0) {
            $user = User::create([

                'company_id'    => 0,
                'first_name'    => 'Zeus',
                'last_name'     => 'Manager',
                'email'         => 'zeus@kokash.ng',
                'phone'         => '2349060051239',
                'sex'           => 'male',
                'status'        => 'active',
                'type'          => 'zeus',
                'access_level' => 2
            ]);

            $password = bcrypt('Adeshola@18');
            $user->update(['password' => $password]);
        }

        if (Auth::user() != null) {
            if (Auth::user()->type == 'zeus') {
                return redirect('zeus/dashboard');
            }

            if (Auth::user()->type == 'admin') {
                return redirect('dashboard');
            }

            if (Auth::user()->type == 'user') {
                return redirect('logout');
            }
        }

        return view('auth.login');
    }

    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')], request('remember'))) {
            $user = Auth::user();

            if ($user->impersonating == 1) {
                return redirect('undo-impersonation');
            }

            $user = User::find(Auth::user()->id);
            $user->update(['last_login_at' => date('Y-m-d H:i:s')]);

            if ($user->status == 'inactive') {
                $error = 'Your account is currently inactive.';

                if (request()->ajax()) {
                    return response()->json(['status' => false, 'data' => $error ]);
                }

                request()->session()->flash('error', $error);

                Auth::logout();

                return $this->index();
            }

            if (request()->ajax()) {
                return response()->json(['status' => true, 'data' => Auth::user()]);
            }

            if (Auth::user()->type == 'zeus') {
                return redirect('zeus/dashboard');
            }

            if (Auth::user()->type == 'admin') {
                return redirect('dashboard');
            }

            return redirect('user');
        }

        if (request()->ajax()) {
            return response()->json(['status' => false, 'data' => 'Invalid username or Password']);
        }

        request()->session()->flash('error', 'Invalid Email or Password');

        return redirect()->back();
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }

    public function undoImpersonate()
    {
        if (!Auth::check() || Auth::user()->impersonating != 1) {
            return redirect('logout');
        }

        User::where(['id' => Auth::user()->id])->update([

            'impersonating' => 0,
            'company_id'    => 0,
            'type'          => 'zeus'
        ]);

        _log('Undo Impersonation');

        return redirect('zeus/dashboard');
    }

    public function activate($token = null)
    {
        if ($token != null) {
            $user = User::where(['api_token' => $token])->first();

            if (!$user) {
                return redirect('login')->with('error', 'Email verification attempt failed');
            }

            $user->update(['email_verified_at' => date('Y-m-d H:i:s')]);

            return redirect('login')->with('message', 'Your email has been verified succesfully.');
        }

        return redirect('login');
    }

    public function magicLink($token = null)
    {
        if ($token != null) {
            $user = User::where(['api_token' => $token])->first();

            if (!$user) {
                return redirect('magic-link')->with('error', 'We do not have '.request('email').' in our records');
            }

            Auth::loginUsingId($user->id, true);

            return redirect('dashboard');
        }

        if (request()->isMethod('post')) {
            $user = User::where(['email' => request('email')])->first();

            if (!$user) {
                return redirect('magic-link')->with('error', 'We do not have '.request('email').' in our records');
            }

            if ($user->status != 'active') {
                return redirect('magic-link')->with('error', 'Your account is currently inactive.');
            }

            $link       = url('magic-link/'.$user->api_token);
            $subject    = 'One time Login Link';
            $body       = view('emails.magic', ['user' => $user, 'link' => $link])->render();

            _email($user->email, $subject, $body);

            return redirect('magic-link')->with('message', 'A one time login link has been sent to '.request('email'));
        }

        $data['title']  = 'Get Magic Link';

        return view('auth.magic', $data);
    }

    public function forgotPassword()
    {
        if (request()->isMethod('post')) {
            $user = User::where(['email' => request('email')])->first();

            if (!$user) {
                return redirect()->back()->with('error', 'We do not have '.request('email').' in our records');
            }

            if ($user->status != 'active') {
                return redirect()->back()->with('error', 'Your account is currently inactive.');
            }

            $link       = url('reset-password/'.$user->api_token);
            $subject    = 'Reset your Kokash Password';
            $body       = view('emails.forgot-password', ['user' => $user, 'link' => $link])->render();

            _email($user->email, $subject, $body);

            return redirect()->back()->with('message', 'A reset password email has been sent to '.request('email'));
        }

        $data['title']  = 'Forgot Password';

        return view('auth.forgot-password', $data);
    }

    public function resetPassword(Request $request, $token = null)
    {
        if ($token == null) {
            return redirect('forgot-password');
        }

        $user = User::where(['api_token' => $token])->first();

        if (!$user) {
            return redirect('forgot-password');
        }

        if ($user->status != 'active') {
            return redirect('forgot-password')->with('error', 'Your account is currently inactive.');
        }

        if (request()->isMethod('post')) {
            $this->validate($request, ['password' => 'required|confirmed']);

            $user->update(['api_token' => str_random(16), 'password' => bcrypt(request('password'))]);

            $subject    = 'Your password has changed';
            $body       = view('emails.password-changed', ['user' => $user])->render();

            _email($user->email, $subject, $body);

            Auth::loginUsingId($user->id, true);

            return redirect('dashboard');
        }

        $data['title']  = 'Reset Password';
        $data['user']   = $user;

        return view('auth.reset-password', $data);
    }

    public function webViewPasswordReset()
    {
        if (request()->isMethod('post')) {
            $company = Company::find(request('company_id'));

            if (!$company) {
                return redirect()->back()->with('error', 'Cooperative Not Found');
            }

            $phone = _to_phone(request('phone'));

            $user = User::where(['phone' => $phone, 'company_id' => $company->id])->first();

            if (!$user) {
                return redirect()->back()->with('error', 'We do not have '.request('phone').' in our records');
            }

            if ($user->status != 'active') {
                return redirect()->back()->with('error', 'Your account is currently inactive.');
            }

            $new_password = mt_rand(10000, 99999);

            $user->update(['api_token' => str_random(16), 'password' => bcrypt($new_password), 'last_login_at' => null]);

            $sms = _password_sms($new_password, $user->phone, $user->company->name);
            SendSMSJob::dispatch($user, $sms);

            $subject    = 'Your password was changed';
            $body       = view('emails.password-changed', ['user' => $user])->render();

            _email($user->email, $subject, $body);

            $message = 'Hello '.$user->name.', Your Password has been reset and your login information has been sent to '.$user->phone. '. You can now open the Kokash App and login';

            return redirect()->back()->with('message', $message);
        }

        $data['companies']      = Company::get();
        $data['title']          = 'Forgot Password';

        return view('auth.webview-forgot-password', $data);
    }

    public function resendLoginDetails()
    {
        $users = User::whereNull('last_login_at')->get();

        foreach ($users as $user) {
            $new_password = mt_rand(10000, 99999);

            $user->update(['api_token' => str_random(16), 'password' => bcrypt($new_password), 'last_login_at' => null]);
            $sms = _password_sms($new_password, $user->phone, $user->company->name);
            SendSMSJob::dispatch($user, $sms);
        }

        return response()->json(['users' => $users->count()]);
    }
}
