<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \Carbon\Carbon;
use App\LoanTransaction;
use App\Transaction;
use App\Loan;
use App\User;
use App\Order;

class ReportsController extends Controller
{
    public function index()
    {
    	$data['title']		= 'Reports';

    	return view('reports.index', $data);
    }

    public function report(Request $request)
    {
        $start_month    = Carbon::createFromFormat('m-Y', request('start_month'))->format('Y-m-d');
        $end_month      = Carbon::createFromFormat('m-Y', request('end_month'))->format('Y-m-d');
        $report         = request('report');
        $company_id     = Auth::user()->company_id;

        switch($report){

            case 'Loan Defaulters':
            return $this->defaulters($company_id, $start_month, $end_month);
            break;

            case 'Transactions':
            return $this->transactions($company_id, $start_month, $end_month);
            break;

            case 'Loans':
            return $this->loans($company_id, $start_month, $end_month);
            break;

            case 'Savings Accounts':
            return $this->savings_accounts($company_id, $start_month, $end_month);
            break;
        }

        return redirect()->back()->with('error', 'Report selected is invalid');
    }

    public function defaulters($company_id = 0, $start_month = null, $end_month = null)
    {
        $data['company']        = Auth::user()->company;
        $data['start_month']    = _month($start_month);
        $data['end_month']      = _month($end_month);

        $data['transactions'] = LoanTransaction::company($company_id)->pending()
        ->where('fine_transaction_id', '>', 0)
        ->whereDate('fine_date', '>=', $start_month)
        ->whereDate('fine_date', '<=', $end_month)
        ->orderBy('loan_id', 'asc')
        ->orderBy('user_id', 'desc')
        ->get();

        return \App::make('dompdf.wrapper')
        ->loadView('reports.exports.defaulters', $data)
        ->setPaper('a4', 'landscape')
        ->stream();
    }

    public function transactions($company_id = 0, $start_month = null, $end_month = null)
    {
        $data['company']        = Auth::user()->company;
        $data['start_month']    = _month($start_month);
        $data['end_month']      = _month($end_month);
        
        $data['transactions'] = Transaction::where(['company_id' => $company_id])
        ->whereDate('created_at', '>=', $start_month)
        ->whereDate('created_at', '<=', $end_month)
        ->orderBy('created_at', 'desc')
        ->get();

        return \App::make('dompdf.wrapper')
        ->loadView('reports.exports.transactions', $data)
        ->setPaper('a4', 'landscape')
        ->stream();
    }

    public function loans($company_id = 0, $start_month = null, $end_month = null)
    {
        $data['company']        = Auth::user()->company;
        $data['start_month']    = _month($start_month);
        $data['end_month']      = _month($end_month);
        
        $data['loans'] = Loan::where(['company_id' => $company_id])
        ->whereDate('created_at', '>=', $start_month)
        ->whereDate('created_at', '<=', $end_month)
        ->orderBy('created_at', 'desc')
        ->get();

        return \App::make('dompdf.wrapper')
        ->loadView('reports.exports.loans', $data)
        ->setPaper('a4', 'landscape')
        ->stream();
    }

    public function savings_accounts($company_id = 0, $start_month = null, $end_month = null)
    {
        $data['company']        = Auth::user()->company;
        $data['start_month']    = _month($start_month);
        $data['end_month']      = _month($end_month);
        
        $data['accounts'] = User::where(['company_id' => $company_id])->orderBy('last_name', 'asc')->user()->get();

        return \App::make('dompdf.wrapper')
        ->loadView('reports.exports.accounts', $data)
        ->setPaper('a4', 'landscape')
        ->stream();
    }

    public function dailyPayouts()
    {
        $date  = date('Y-m-d', strtotime(request('date')));

        $data['company']    = Auth::user()->company;
        $data['date']       = _d($date);
        
        $data['orders'] = Order::where([

            'company_id'            => Auth::user()->company_id, 
            'payment_source_id'     => PAYMENT_SOURCE_PAYSTACK,
            'status'                => 'completed'

        ])->whereDate('transaction_date', $date)->get();

        return \App::make('dompdf.wrapper')
        ->loadView('reports.exports.daily-payouts', $data)
        ->setPaper('a4', 'portrait')
        ->stream();
    }
}
