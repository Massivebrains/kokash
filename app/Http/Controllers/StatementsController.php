<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StatementRequest;
use App\Statement;
use App\User;
use App\Http\Requests\AdminRequest;

class StatementsController extends Controller
{
    public function index(AdminRequest $request)
    {
        if(request()->isMethod('post')){

            $statements = Statment::search(request('query'));

        }else{

            $statements = Statement::orderBy('id', 'desc')->paginate(20);
        }

        $data['title']          = 'statements';
        $data['statements']   	= $statements;

        return view('statements.index', $data);
    }

    public function statement()
    {
        $data['title']          = 'statements';
        $data['users']          = User::where(['type' => 'user', 'status' => 'active'])->get();

        return view('statements.statement', $data);
    }

    public function saveStatement(StatementRequest $request)
    {
        $user = User::where(['id' => request('user_id'), 'type' => 'user'])->first();

        if(!$user)
            return redirect('statements');

        $statmenet = Statement::create([

            'user_id'       => $user->id,
            'start_date'    => date('Y-m-d', strtotime(request('start_date'))),
            'end_date'      => date('Y-m-d', strtotime(request('start_date')))
        ]);

        _log('Created a Statement of Account for '.$user->name, $user);
        return redirect('statements')->with('message', 'Statement Request has been queued for execution.');
    }
}
