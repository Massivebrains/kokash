<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\SavingsAccount;
use App\Http\Requests\AdminRequest;
use App\Jobs\SendEmailJob;
use Illuminate\Support\Facades\Auth;

class MembersController extends Controller
{
    public function index(AdminRequest $request)
    {
        if (request()->isMethod('post') && request('query')) {
            $members        = User::search(request('query'), Auth::user()->company_id, 'user');
            $data['query']  = request('query');
            $data['count']  = $members->count();
        } else {
            $members = User::user()->where(['company_id' => Auth::user()->company_id])->orderBy('id')->paginate(20);
        }

        $data['users']      = $members;
        $data['title']      = 'members';

        return view('members.index', $data);
    }

    public function member($id = 0)
    {
        $data['user']	= User::firstOrNew(['id' => $id]);
        $data['title']	= 'members';

        return view('members.form', $data);
    }

    public function createMember(UserRequest $request)
    {
        $user = User::create([
            'company_id'        => _cid(),
            'first_name'		=> request('first_name'),
            'last_name'			=> request('last_name'),
            'email'				=> request('email'),
            'phone'				=> _to_phone(request('phone')),
            'sex'				=> request('sex'),
            'marital_status'	=> request('marital_status'),
            'country_id'		=> request('country_id'),
            'state_id'			=> request('state_id'),
            'address'			=> request('address'),
            'occupation'		=> request('occupation'),
            'employer'			=> request('employer'),
            'employer_address'	=> request('employer_address'),
            'next_of_kin'		=> request('next_of_kin'),
            'next_of_kin_phone'	=> request('next_of_kin_phone'),
            'type'				=> 'user',
            'api_token'			=> str_random(16),
            'status'			=> 'inactive'
        ]);

        SavingsAccount::create([

            'user_id'           => $user->id,
            'company_id'        => $user->company_id,
            'account_number'    => SavingsAccount::nextAccountNumber(),
            'balance'           => 0
        ]);

        _log('Created new member Account'.$user->name, $user);
        return redirect('members')->with('message', 'Account created succesfully. Member has been sent an email to verify.');
    }

    public function updateMember(UpdateUserRequest $request)
    {
        $user = User::where(['id' => request('id'), 'type' => 'user'])->first();

        if (!$user) {
            return redirect('members');
        }

        User::where(['id' => request('id')])->update([

            'first_name'        => request('first_name', $user->first_name),
            'last_name'         => request('last_name', $user->last_name),
            'email'             => request('email'),
            'phone'             => _to_phone(request('phone', $user->phone)),
            'sex'               => request('sex', $user->sex),
            'marital_status'    => request('marital_status', $user->marital_status),
            'country_id'        => (int)request('country_id', $user->country_id),
            'state_id'          => (int)request('state_id', $user->state_id),
            'address'           => request('address', $user->address),
            'occupation'        => request('occupation', $user->occupation),
            'employer'          => request('employer', $user->employer),
            'employer_address'  => request('employer_address', $user->employer_address),
            'next_of_kin'       => request('next_of_kin', $user->next_of_kin),
            'next_of_kin_phone' => request('next_of_kin_phone', $user->next_of_kin_phone)
        ]);

        _log('Updated Member Information - '.$user->name, $user);

        $subject    = 'Your information was modified!';
        $body       = view('emails.update-registration', ['user' => $user])->render();
        SendEmailJob::dispatch($user, $subject, $body);

        return redirect('members')->with('message', 'Account modified succesfully. Member has been sent an email to know about this change.');
    }
}
