<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AdminRequest;
use App\User;
use Auth;

class AdminsController extends Controller
{
    public function index(AdminRequest $request)
    {
        $data['users']      = User::admin()->where(['company_id' => Auth::user()->company_id])->orderBy('id', 'desc')->paginate(50);
        $data['title']      = 'admins';

    	return view('admins.index', $data);
    }

    public function admin($id = 0)
    {        
    	$data['user']	= User::firstOrNew(['id' => $id]);
    	$data['title']	= 'admins';

    	return view('admins.form', $data);
    }

    public function saveAdmin(AdminRequest $request, $id)
    {
    	if($id > 0){

    		if(User::firstOrNew(['id' => $id])->type != 'admin')
    			return redirect('admins');
    	}

    	$user = User::updateOrCreate(['id' => $id],[

            'company_id'        => _cid(),
    		'first_name'		=> request('first_name'),
    		'last_name'			=> request('last_name'),
    		'email'				=> request('email'),
    		'phone'				=> _to_phone(request('phone')),
    		'type'				=> 'admin',
    		'api_token'			=> str_random(16),
    		'status'			=> request('status'),
    		'access_level'		=> request('access_level')
    	]);
        
        _log('Created new Admin account - '.$user->name);
        return redirect('admins')->with('message', 'Admin Account created succesfully. Admin password has been sent to user.');
    }
}
