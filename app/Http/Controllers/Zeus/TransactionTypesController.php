<?php

namespace App\Http\Controllers\Zeus;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\TransactionTypeRequest;
use App\TransactionType;
use Auth;

class TransactionTypesController extends Controller
{
    public function index()
    {
        $data['transaction_types']  = TransactionType::get();
        $data['title']      		= 'transaction_types';

    	return view('zeus.transaction-types.index', $data);
    }

    public function transactionType($id = 0)
    {
    	$data['transaction_type']	= TransactionType::firstOrNew(['id' => $id]);
    	$data['title']				= 'transaction_type';

    	return view('zeus.transaction-types.form', $data);
    }

    public function saveTransactionType(TransactionTypeRequest $request)
    {
    	$transaction_type = TransactionType::updateOrCreate(['id' => request('id')],[

            'name'   => request('name'),
            'status' => request('status')
    	]);

        _log('Saved Transaction Type - '.request('name'));
        session()->flash('message', 'Transaction Type created succesfully.');

        return redirect('zeus/transaction-types');
    }
}
