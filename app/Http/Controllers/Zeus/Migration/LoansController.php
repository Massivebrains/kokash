<?php

namespace App\Http\Controllers\Zeus\Migration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Carbon\Carbon;
use App\Loan;
use App\LoanTransaction;
use App\LoanCategory;
use App\User;
use Illuminate\Support\Facades\DB;

class LoansController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->has('upload')) {
            return redirect()->back()->with('error', 'You did not select a file to upload');
        }

        $this->validate($request, ['loans' => 'required|mimes:csv,txt']);

        $path = $request->file('loans')->store('loans');
        $file    = fopen(public_path('uploads/'.$path), 'r');
        $loans   = [];

        while (!feof($file)) {
            $loans[] = fgetcsv($file);
        }

        $errorCount = $successCount = 0;

        DB::transaction(function () use ($loans, $errorCount, $successCount) {
            for ($i = 1; $i <= count($loans)-1; $i++) {
                $row = $loans[$i];

                $phone = trim((float)$row[0] ?? 0);
                $principal = trim(strtolower($row[1] ?? 0));
                $description = trim($row[2] ?? 0);
                $company_id = request('company_id');
                $rate = 0;
                $time = 1;

                $user = User::updateOrCreate(['company_id' => $company_id, 'phone' => $phone]);
                $loan_category = LoanCategory::firstOrNew(['id' => request('loan_category_id')]);
                $loan_transactions  = LoanTransaction::preCalculate($principal, $rate, $time, date('m-Y'));
                $interest           = $loan_transactions->sum('interest');
                $monthly_payment    = $loan_transactions->first();
                $start_at           = now()->format('Y-m-d');

                $loan = Loan::create([
                    'user_id'           => $user->id,
                    'company_id'        => $user->company_id,
                    'loan_category_id' => $loan_category->id,
                    'name'              => $loan_category->name. ' '.$description,
                    'reference'         => _loan_reference(),
                    'principal'         => $principal,
                    'disbursed'         => $principal - $interest,
                    'rate'              => $rate,
                    'time'              => $time,
                    'interest'          => $interest,
                    'amount'            => $principal,
                    'outstanding'       => $principal,
                    'monthly_payment'   => $monthly_payment->amount,
                    'type'              => 'simple',
                    'start_at'          => $start_at,
                    'disbursed_at'      => date('Y-m-d'),
                    'status'            => 'running'
                ]);

                foreach ($loan_transactions as $row) {
                    LoanTransaction::create([
                        'loan_id'           => $loan->id,
                        'user_id'           => $user->id,
                        'company_id'        => $user->company_id,
                        'reference'         => _reference(),
                        'principal'         => $row->principal,
                        'interest'          => $row->interest,
                        'amount'            => $row->amount,
                        'paid'              => 0,
                        'outstanding'       => $row->amount,
                        'balance'           => $row->amount,
                        'date'              => Carbon::parse($row->date)->format('Y-m-d'),
                        'status'            => 'pending',
                        'description'       => 'Loan Migration',
                        'payment_source_id' => PAYMENT_SOURCE_CASH,
                        'payment_date'      => null
                    ]);
                }

                $successCount++;
            }
        });

        _log('Migrated Loan');
        return redirect('zeus/migration')->with('message', 'Loans Uploaded Successfully.');
    }

    public function process()
    {
        $file    = fopen(public_path('uploads/personal.csv'), 'r');
        $loans   = [];

        while (!feof($file)) {
            $loans[] = fgetcsv($file);
        }

        $errors = [];
        $success = [];

        $errorCount = $successCount = 0;

        for ($i = 1; $i <= count($loans)-1; $i++) {
            $row                    = $loans[$i];

            $date                   = trim(strtolower($row[0] ?? 0));
            $name                   = trim(strtolower($row[1] ?? 0));
            $principal              = trim((float)str_replace(',', '', $row[2]) ?? 0);
            $disbursed              = trim((float)str_replace(',', '', $row[3]) ?? 0);
            $_interest              = trim((float)str_replace(',', '', $row[4]) ?? 0);
            $paid                   = trim(str_replace(',', '', $row[5]) ?? 0);

            if (empty($name)) {
                continue;
            }

            $user = $this->getUser($name);

            if (!$user) {
                $errors[] = $name.' Not Found';
                continue;
            }

            $start_at = $this->getDate($date);

            if (!$start_at) {
                $errors[] = $date.' is invalid. Name: '.$name;
                continue;
            }

            $interest = $principal - $disbursed;
            $rate = (($interest / $principal) * 100);
            $rate = round($rate, 2);
            $time = 2;

            $loan_transactions  = LoanTransaction::preCalculate($principal, $rate, $time, date('m-Y', strtotime($start_at)));
            $interest           = $loan_transactions->sum('interest');
            $monthly_payment    = $loan_transactions->first();
            $start_at           = date('Y-m-d', strtotime($start_at));

            if ($start_at == '1970-01-01') {
                $errors[] = $date.' is invalid. Name: '.$name. 'Disbursed: '.$disbursed;
                continue;
            }

            $loan = Loan::create([

            'user_id'           => $user->id,
            'company_id'        => $user->company_id,
            'name'              => 'Personal Loan',
            'reference'         => _loan_reference(),
            'principal'         => $principal,
            'disbursed'         => $principal - $interest,
            'rate'              => $rate,
            'time'              => $time,
            'interest'          => $interest,
            'amount'            => $principal,
            'outstanding'       => $principal,
            'monthly_payment'   => $monthly_payment->amount,
            'type'              => 'simple',
            'start_at'          => $start_at,
            'disbursed_at'      => $start_at,
            'status'            => 'running'
        ]);

            $sum = $loan_transactions->sum('amount');

            foreach ($loan_transactions as $row) {
                $sum-= $row->amount;

                LoanTransaction::create([

                'loan_id'           => $loan->id,
                'user_id'           => $user->id,
                'company_id'        => $user->company_id,
                'reference'         => _reference(),
                'principal'         => $row->principal,
                'interest'          => $row->interest,
                'amount'            => $row->amount,
                'paid'              => 0,
                'outstanding'       => $row->amount,
                'balance'           => $sum,
                'date'              => Carbon::parse($row->date)->format('Y-m-d'),
                'status'            => 'pending'
            ]);
            }

            Loan::pay($loan, $paid, 'Dec 2019', 1);

            $successCount++;
        }

        dd(['errors' => $errors, 'success' => $success, 'successful' => $successCount]);
    }

    private function getUser($name)
    {
        $names = explode(' ', $name);

        if (count($names) < 2) {
            return null;
        }

        $user = User::where('first_name', 'like', '%'.$names[0].'%')->where('last_name', 'like', '%'.$names[1].'%')->first();

        if (!$user) {
            $user = User::where('last_name', 'like', '%'.$names[0].'%')->where('first_name', 'like', '%'.$names[1].'%')->first();
        }

        if (!$user) {
            if (User::whereFirstName($names[0])->count() == 1) {
                $user = User::whereFirstName($names[0])->first();
            }
        }

        if (!$user) {
            if (User::whereLastName($names[0])->count() == 1) {
                $user = User::whereLastName($names[0])->first();
            }
        }

        if (!$user) {
            if (User::whereFirstName($names[1])->count() == 1) {
                $user = User::whereFirstName($names[1])->first();
            }
        }

        if (!$user) {
            if (User::whereLastName($names[1])->count() == 1) {
                $user = User::whereLastName($names[1])->first();
            }
        }

        return $user;
    }

    private function getDate($date = '')
    {
        $date = explode('\'', strtolower($date));

        if (count($date) < 2) {
            return  null;
        }

        $month = 0;

        switch ($month) {

        case 'jan':
        $month = 1;
        break;

        case 'feb':
        $month = 2;
        break;

        case 'march':
        $month = 3;
        break;

        case 'april':
        $month = 4;
        break;

        case 'may':
        $month = 5;
        break;

        case 'june':
        $month = 6;
        break;

        case 'july':
        $month = 7;
        break;

        case 'august':
        $month = 8;
        break;

        case 'aug':
        $month = 8;
        break;

        case 'sept':
        $month = 9;
        break;

        case 'oct':
        $month = 10;
        break;

        case 'nov':
        $month = 11;
        break;

        case 'dec':
        $month = 12;
        break;
    }

        $year  = '20'.$date[1];

        $format = '01'.'/'.$month.'/'.$year;

        $loan_date = date('d-m-Y', strtotime($format));

        return $loan_date;
    }
}
