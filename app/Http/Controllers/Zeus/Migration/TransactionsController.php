<?php

namespace App\Http\Controllers\Zeus\Migration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SavingsAccount;
use App\User;
use App\Bank;
use App\Transaction;
use App\Share;
use Auth;

class TransactionsController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->has('upload')) {
            return redirect()->back()->with('error', 'You did not select a file to upload');
        }

        $this->validate($request, ['transactions' => 'required|mimes:csv,txt']);

        $path = $request->file('transactions')->store('transactions');

        $file       	= fopen(public_path('uploads/'.$path), 'r');
        $transactions   = [];

        while (!feof($file)) {
            $transactions[] = fgetcsv($file);
        }

        $errorCount = $successCount = 0;

        for ($i = 1; $i <= count($transactions)-1; $i++) {
            $row           = $transactions[$i];

            $phone    = trim(strtolower($row[0] ?? ''));
            $name      	= trim(strtolower($row[1] ?? 0));
            $email    = trim(strtolower($row[2] ?? ''));
            $amount    		= trim((float)$row[3] ?? 0);

            $company_id = request('company_id');

            $names = explode(' ', $name);
            $first_name = $names[0];
            unset($names[0]);
            $last_name = implode(' ', $names);
            $user = User::updateOrCreate(['company_id' => $company_id, 'phone' => $phone], [
                'company_id'        => $company_id,
                'first_name'		=> $first_name,
                'last_name'			=> $last_name,
                'email'				=> $email,
                'phone'				=> $phone,
                'type'				=> 'user',
                'api_token'			=> str_random(16),
                'status'			=> 'active'
            ]);

            Transaction::create([
                'user_id'                   => $user->id,
                'company_id'                => $user->company_id,
                'transaction_type_id'       => TRANSACTION_TYPE_SAVINGS,
                'reference'                 => _reference(),
                'amount'                    => (float)$amount,
                'description'               => request('description', date('F Y')),
                'payment_source_id'         => PAYMENT_SOURCE_CASH
            ]);

            _log('Credit Transaction of '.$amount.' created for '.$user->name.' via data migration', $user);
            $successCount++;
        }

        _log('Migrated Transactions Successfully.');

        $total 	 = $errorCount + $successCount;
        $message = "Upload was succesful. {$successCount} of {$total} was succesfully Saved.";

        return redirect('zeus/migration')->with('message', $message);
    }

    public function shares(Request $request)
    {
        if (!$request->has('upload')) {
            return redirect()->back()->with('error', 'You did not select a file to upload');
        }

        $this->validate($request, ['shares' => 'required|mimes:csv,txt']);

        $path = $request->file('shares')->store('shares');

        $file       	= fopen(public_path('uploads/'.$path), 'r');
        $shares   = [];

        while (!feof($file)) {
            $shares[] = fgetcsv($file);
        }

        $errorCount = $successCount = 0;

        for ($i = 1; $i <= count($shares)-1; $i++) {
            $row           = $shares[$i];

            $name = trim(strtolower($row[0] ?? 0));
            $amount = trim((float)$row[1] ?? 0);

            $user = User::getByName(request('company_id'), $name);

            Share::updateOrCreate(['user_id' => $user->id], [

                'company_id'		=> $user->company->id,
                'user_id'			=> $user->id,
                'units'				=> (int)$user->share->units + $amount,
                'currency_value'	=> $amount,
            ]);

            _log('#Share:: Created '.$amount.' units of shares for '.$user->name.' via data migration', $user);
            $successCount++;
        }

        _log('Migrated Shares Successfully.');

        $total 	 = $errorCount + $successCount;
        $message = "Upload was succesful. {$successCount} of {$total} was succesfully Saved.";

        return redirect('zeus/migration')->with('message', $message);
    }
}
