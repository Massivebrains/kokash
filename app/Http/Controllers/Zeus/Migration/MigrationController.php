<?php

namespace App\Http\Controllers\Zeus\Migration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MigrationController extends Controller
{
    public function index()
	{
		return view('zeus.migration.index');
	}
}
