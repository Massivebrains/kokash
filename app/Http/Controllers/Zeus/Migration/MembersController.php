<?php

namespace App\Http\Controllers\Zeus\Migration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Country;
use App\State;
use App\User;
use App\SavingsAccount;
use Auth;
use App\Bank;
use Illuminate\Support\Facades\Validator;

class MembersController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->has('upload')) {
            return redirect()->back()->with('error', 'You did not select a file to upload');
        }

        $this->validate($request, ['members' => 'required|mimes:csv,txt']);

        $path = $request->file('members')->store('members');


        $file       = fopen(public_path('uploads/'.$path), 'r');
        $members   	= [];

        while (!feof($file)) {
            $members[] = fgetcsv($file);
        }

        $errorCount = $successCount = 0;

        for ($i = 1; $i <= count($members)-1; $i++) {
            $row            	= $members[$i];

            $phone          		= trim(strtolower($row[0] ?? ''));
            $name      		= trim(strtolower($row[1] ?? ''));
            $email          		= trim(strtolower($row[2] ?? ''));
            $sex          			= trim(strtolower($row[3] ?? 'male'));
            $country        		= trim(strtolower($row[4] ?? 'nigeria'));
            $state         			= trim(strtolower($row[5] ?? 'lagos'));
            $marital_status  		= trim(strtolower($row[6] ?? 'english'));
            $address    			= trim(strtolower($row[7] ?? ''));
            $occupation     		= trim(strtolower($row[8] ?? ''));
            $employer         		= trim(strtolower($row[9] ?? ''));
            $employer_address   	= trim(strtolower($row[10] ?? ''));
            $next_of_kin         	= trim(strtolower($row[11] ?? ''));
            $next_of_kin_phone    	= trim(strtolower($row[12] ?? ''));
            $monthly_savings_amount = trim(strtolower($row[13] ?? ''));
            $bank_id 				= trim(strtolower($row[14] ?? ''));
            $bank_account_number 	= trim(strtolower($row[15] ?? ''));
            $bank_account_name 		= trim(strtolower($row[16] ?? ''));

            $company_id = request('company_id');
            $first_name = $name;
            $last_name = '';
            $names = explode(' ', $name);
            if (count($names) > 1) {
                $first_name = $names[0];
                $last_name = $names[1].' '.(isset($names[2]) ? $names[2] : '');
            }

            $payload = [
                'first_name'         	=> $first_name,
                'last_name'          	=> $last_name,
                'email'             	=> $email,
                'phone'             	=> $phone
            ];

            $validator = Validator::make($payload, [

                'first_name'        => 'string',
                'last_name'         => 'string',
                'phone'             => 'required',
                'email'             => 'email'
            ]);

            if ($validator->fails()) {
                $errorCount++;
                continue;
            } else {
                $phone = _to_phone($phone);

                $user = User::where(['company_id' => $company_id, 'first_name' => $first_name, 'last_name' => $last_name, 'phone' => $phone])->first();

                if ($user) {
                    $errorCount++;
                    continue;
                }

                $country 	= Country::firstOrNew(['name' => $country]);
                $state 		= State::firstOrNew(['name' => $state]);

                $user = User::updateOrCreate(['phone' => $phone], [

                    'company_id'       	=> $company_id,
                    'first_name'        => ucwords($first_name),
                    'last_name'         => ucwords($last_name),
                    'email'				=> ucwords($email),
                    'phone'             => _to_phone($phone),
                    'sex'             	=> $sex == 'male' ? 'male' : 'female',
                    'country_id'        => $country->id,
                    'state_id'          => $state->id,
                    'marital_status'    => $marital_status == 'married' ? 'married' : 'single',
                    'address'          	=> ucwords($address),
                    'occupation'        => ucwords($occupation),
                    'employer'			=> ucwords($employer),
                    'employer_address'	=> ucwords($employer_address),
                    'next_of_kin'		=> ucwords($next_of_kin),
                    'next_of_kin_phone'	=> $next_of_kin_phone,

                    'password'			=> bcrypt($first_name),
                    'type'				=> 'user',
                    'api_token'			=> str_random(16),
                    'status'			=> 'active'
                ]);

                $savings_account = SavingsAccount::updateOrCreate(['user_id' => $user->id], [

                    'user_id'           => $user->id,
                    'company_id'        => $user->company_id,
                    'monthly_savings'	=> (float) $monthly_savings_amount ?? 10000,
                    'account_number'    => SavingsAccount::nextAccountNumber(),
                    'balance'           => 0
                ]);

                $bank = Bank::find($bank_id);

                if ($bank) {
                    if (!_is_nuban_valid($bank->code.$bank_account_number)) {
                        $errorCount++;
                        continue;
                    }

                    $savings_account->update([

                        'bank_id'				=> $bank->id,
                        'bank_account_number'	=> $bank_account_number,
                        'bank_account_name'		=> $bank_account_name
                    ]);
                }

                $successCount++;
            }
        }

        _log('Uploaded new members information via data migration');

        $total 	 = $errorCount + $successCount;
        $message = "Upload was succesful. {$successCount} of {$total} was succesfully Saved.";

        return redirect('zeus/migration')->with('message', $message);
    }
}
