<?php

namespace App\Http\Controllers\Zeus;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentSourceRequest;
use App\PaymentSource;
use Auth;

class PaymentSourcesController extends Controller
{
    public function index()
    {
        $data['payment_sources']  = PaymentSource::get();
        $data['title']      	= 'payment_sources';

    	return view('zeus.payment-sources.index', $data);
    }

    public function paymentSource($id = 0)
    {
    	$data['payment_source']	= PaymentSource::firstOrNew(['id' => $id]);
    	$data['title']			= 'payment_source';

    	return view('zeus.payment-sources.form', $data);
    }

    public function savePaymentSource(PaymentSourceRequest $request)
    {
    	$payment_Source = PaymentSource::updateOrCreate(['id' => request('id')],[

            'name'   => request('name'),
            'status' => request('status')

    	]);

        _log('Saved payment source '.request('name'));
        session()->flash('message', 'Payment Source saved succesfully.');

        return redirect('zeus/payment-sources');
    }
}
