<?php

namespace App\Http\Controllers\Zeus;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;
use App\Http\Requests\CompanyRequest;
use App\Http\Requests\ZeusRequest;
use App\Setting;
use App\User;
use Auth;

class CompanyController extends Controller
{
    public function index(ZeusRequest $request)
    {
        $data['companies']  = Company::get();
        $data['title']      = 'companies';

        return view('zeus.companies.index', $data);
    }

    public function company(ZeusRequest $request, $id = 0)
    {
    	$data['company']	= Company::firstOrNew(['id' => $id]);
    	$data['title']		= 'company';

    	return view('zeus.companies.form', $data);
    }

    public function save(CompanyRequest $request, $id = 0)
    {
    	$company = Company::updateOrCreate(['id' => $id], [

            'name'      			=> request('name'),
            'description'			=> request('description'),
            'address'				=> request('address'),
            'contact_person'		=> request('contact_person'),
            'contact_person_phone'	=> request('contact_person_phone'),
            'contact_person_email'	=> request('contact_person_email')

        ]);

        _log('Company Data Saved - '.request('name'));

        session()->flash('message', 'Company saved succesfully.');

        return redirect('zeus/companies');
    }

    public function impersonate(ZeusRequest $request, $id = 0)
    {
        $company = Company::find($id);

        if(!$company)
            return response('zeus/companies');

        User::where(['id' => _id()])->update([

            'company_id'    => $company->id,
            'type'          => 'admin',
            'impersonating' => 1
        ]);

        _log('Impersonated '.$company->name);
        session()->flash('message', 'You are currently impersonating '.$company->name);

        return redirect('dashboard');
    }

    public function setting(ZeusRequest $request, $id = 0)
    {
        $company = Company::find($id);

        if(!$company) return redirect('zeus/companies');

        if(request()->isMethod('post')){

            foreach(Setting::keys() as $row){

                $value = request($row);

                if(!$value) continue;

                Setting::set($row, $value, $company->id);
            }

            _log('Updated settings '.$company->name);
            session()->flash('message', 'Settings Updated succesfully');

            return redirect('zeus/companies');
        }

        $data['title']     = $company->name.' - Settings and Configuration';
        $data['company']   = $company;
        $data['setting']   = Setting::setting($company->id);

        return view('zeus.companies.settings', $data);
    }
}
