<?php

namespace App\Http\Controllers\Zeus;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ZeusRequest;
use Auth;
use App\AppLog;
use App\Transaction;
use App\LoanTransaction;

class DashboardController extends Controller
{
    public function index(ZeusRequest $request)
    {
    	$data['title'] 				= 'Welcome '.Auth::user()->name.'!';
    	$data['transactions']		= Transaction::where(['status' => 'completed'])->latest()->take(20)->get();
    	$data['savings_month']     	= Transaction::where('amount', '>', 0)->whereYear('created_at', date('Y'))->whereMonth('created_at', date('m'))->sum('amount');
    	$data['savings']     		= Transaction::where('amount', '>', 0)->sum('amount');
    	$data['loans_month']     	= LoanTransaction::where('amount', '>', 0)->completed()->whereYear('created_at', date('Y'))->whereMonth('created_at', date('m'))->sum('amount');
    	$data['loans']     			= LoanTransaction::where('amount', '>', 0)->completed()->sum('amount');

    	return view('zeus.dashboard.index', $data);
    }

    public function appLogs()
    {
        $logs = AppLog::orderBy('id', 'desc')->take(200)->get();

        $data['title'] = 'Kokash Support App - Logs';
        $data['logs']   = $logs;

        return view('zeus.dashboard.app-logs', $data);
    }
}
