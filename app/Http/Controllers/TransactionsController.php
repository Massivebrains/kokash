<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\User;
use Auth;
use App\Http\Requests\TransactionRequest;
use App\Http\Requests\AdminRequest;

class TransactionsController extends Controller
{
    public function index(AdminRequest $request)
    {
        if (request()->isMethod('post') && request('query')) {
            $transactions   = Transaction::search(request('query'), Auth::user()->company_id);
            $data['query']  = request('query');
            $data['count']  = $transactions->count();
        } else {
            $transactions = Transaction::where(['company_id' => Auth::user()->company_id])->orderBy('id', 'desc')->paginate(50);
        }

        $data['title']          = 'transactions';
        $data['transactions']   = $transactions;

        return view('transactions.index', $data);
    }

    public function creditForm()
    {
        $data['title']          = 'New Transaction';
        $data['users']          = User::query()->user()->company(_cid())->active()->get();

        return view('transactions.credit', $data);
    }

    public function transaction(AdminRequest $request, $id = 0)
    {
        $transaction = Transaction::where(['id' => $id, 'company_id' => _cid()])->first();

        if (!$transaction) {
            return redirect('transactions');
        }

        $data['title']          = $transaction->reference;
        $data['active']         = 'transactions';
        $data['transaction']    = $transaction;

        return view('transactions.transaction', $data);
    }

    public function saveCreditTransaction(TransactionRequest $request)
    {
        $user = User::where(['id' => request('user_id'), 'type' => 'user', 'company_id' => _cid()])->first();

        if (!$user) {
            return redirect('transactions');
        }
        
        $response = Transaction::pay($user, request('amount'), request('description'), PAYMENT_SOURCE_CASH);
        
        if ($response->status == false) {
            return redirect()->back()->with('message', $response->message);
        }

        return redirect('transactions')->with('message', $response->message);
    }

    public function reciept($id = '')
    {
        $transaction = Transaction::find($id);

        if (!$transaction) {
            return show_404();
        }

        $data['transaction']    = $transaction;
        $data['user']           = $transaction->user;
        $data['company']        = $transaction->user->company;

        return view('transactions.reciept', $data);
    }
}
