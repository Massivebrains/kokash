<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminRequest;
use App\Http\Requests\ShareRequest;
use App\Share;
use App\User;
use App\Jobs\SendSMSJob;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;

class SharesController extends Controller
{
    public function index(AdminRequest $request)
    {
        if (request()->isMethod('post') && request('query')) {
            $shares         = Share::search(request('query'), Auth::user()->company_id);
            $data['query']  = request('query');
            $data['count']  = $shares->count();
        } else {
            $shares = Share::where(['company_id' => Auth::user()->company_id])->orderBy('id', 'desc')->paginate(50);
        }

        $data['title']  = 'shares';
        $data['active'] = 'share';
        $data['shares']  = $shares;

        return view('shares.index', $data);
    }

    public function form()
    {
        $data['title']          = 'Buy New Shares';
        $data['users']          = User::query()->user()->active()->company(_cid())->orderBy('first_name')->get();
        $data['share']			= new Share();

        return view('shares.form', $data);
    }

    public function save(ShareRequest $request)
    {
        $user = User::where(['id' => request('user_id')])->user()->company(_cid())->first();

        if (!$user) {
            return redirect('shares')->with('error', 'Invalid User selected');
        }

        $share = Share::updateOrCreate(['user_id' => $user->id], [

            'company_id'		=> $user->company->id,
            'user_id'			=> $user->id,
            'units'				=> (int)$user->share->units + request('units'),
            'currency_value'	=> (int)$user->share->currency_value + request('currency_value'),
        ]);

        $log = '#Share:: Created '.request('units').' units of shares for '.$user->name;

        activity()
        ->performedOn($share)
        ->causedBy(Auth::user())
        ->log($log);

        SendSMSJob::dispatch($user, _share_sms($share, request('units')));
        return redirect('share/0')->with('message', $log);
    }

    public function history($id = 0)
    {
        $share = Share::find($id);

        if (!$share) {
            return redirect('shares');
        }

        $data['title']  	= $share->user->name.' Share history';
        $data['active'] 	= 'share';
        $data['share']	 	= $share;
        $data['activities']	= Activity::where(['subject_id' => $share->id, 'subject_type' => 'App\Share'])->orderBy('created_at', 'desc')->get();

        return view('shares.history', $data);
    }

    public function updateForm($id = 0)
    {
        $share = Share::find($id);

        if (!$share) {
            return redirect('shares');
        }

        $data['title']      = 'Update '.$share->user->name.'\'s Shares.';
        $data['share']		= $share;

        return view('shares.form', $data);
    }
}
