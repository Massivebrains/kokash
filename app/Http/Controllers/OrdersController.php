<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderDetail;
use App\PaymentSource;
use Auth;
use App\Rules\AdminPassword;
use App\ProcessOrder;

class OrdersController extends Controller
{
    public function index()
    {
        if(request()->isMethod('post') && request('query')){

            $orders         = Order::search(request('query'), Auth::user()->company_id);
            $data['query']  = request('query');
            $data['count']  = $orders->count();

        }else{

            $orders = Order::where(['company_id' => Auth::user()->company_id])->pending()->paginate(50);
        }

        $data['orders'] = $orders;

        return view('orders.index', $data);
    }

    public function order(Request $request, $order_id = 0)
    {

        $order = Order::where([

            'id'                => $order_id, 
            'company_id'        => Auth::user()->company_id

        ])->first();

        if(!$order)
            return redirect('orders')->with('error', 'Order Not found');


        $data['order']	    = $order;
        $data['sources']    = PaymentSource::get();

        return view('orders.order', $data);
    }

    public function completeOrder(Request $request)
    {
        $this->validate($request, [

            'order_id'              => 'required|numeric',
            'payment_source_id'     => 'required|numeric',
            'password'              => ['required', new AdminPassword]
        ]);

        $order = Order::find(request('order_id'));

        if(!$order || $order->status != 'pending')
            return redirect('order/'.$order->id)->with('error', 'Transaction is Invalid');        

        $source = PaymentSource::find(request('payment_source_id'));

        if(!$source)
            return redirect('order/'.$order->id)->with('error', 'Payment Source is Invalid'); 

        $order->update([

            'transaction_reference' => str_random(8),
            'payment_source_id'     => $source->id,
            'transaction_date'      => date('Y-m-d H:i:s'),
            'status'                => 'paid'
        ]);
        
        ProcessOrder::process($order);

        _log('Complted Order '.$order->order_reference, $order->user);
        return redirect('order/'.$order->id)->with('message', 'Transaction has been successfully completed.');
    }

    public function status($id = 0, $status = '')
    {
        $order = Order::where(['id' => $id, 'status' => 'pending'])->first();

        if(!$order)
            return redirect('orders')->with('error', 'Pending payment not found');

        if(!in_array($status, ['cancelled', 'pending']))
            return redirect('orders')->with('error', 'This operation cannot be performed.');

        $order->update(['status' => $status]);

        return redirect('orders')->with('message', 'Pending payment have now being '.$status);

    }
}
