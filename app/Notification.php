<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use SoftDeletes;

    protected $hidden   = ['deleted_at'];
    protected $guarded  = ['updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeBy($query, User $user)
    {
        return $query->where(['user_id' => $user->id]);
    }

    public function notification_read()
    {
        return $this->hasMany('App\NotificationRead');
    }

    public function getScheduledAtAttribute($value)
    {
        if (is_null($value)) {
            return date('d-m-Y');
        }

        return date('d-m-Y', strtotime($value));
    }

    public function scopeCompany($query, $company_id = 0)
    {
        return $query->where(['company_id' => $company_id]);
    }

    public function scopePending($query)
    {
        return $query->where(['status' => 'pending']);
    }

    public function scopeSent($query)
    {
        return $query->where(['status' => 'sent']);
    }

    public function scopeCancelled($query)
    {
        return $query->where(['status' => 'cancelled']);
    }
}
