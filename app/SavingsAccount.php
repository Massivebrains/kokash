<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SavingsAccount extends Model
{
    use SoftDeletes;
    
    protected $guarded  = ['updated_at'];
    protected $hidden  = ['updated_at', 'deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function bank()
    {
        return $this->belongsTo('App\Bank')->withDefault(function(){

            return new \App\Bank();
        });
    }

    
    public static function nextAccountNumber()
    {
    	$number = substr(rand()*rand(), 0, 10);

    	if(SavingsAccount::where(['account_number' => $number])->count() > 0)
    		SavingsAccount::nextAccountNumber();

    	return $number;
    }
}
