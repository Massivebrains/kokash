<?php

use App\User;
use Illuminate\Support\Facades\Auth;

function _admin()
{
    return Auth::check() ? (Auth::user()->type == 'admin' ? true : false) : false;
}

function _id()
{
    return Auth::check() ? Auth::user()->id : 0;
}

function _cid()
{
    return Auth::check() ? Auth::user()->company_id : 0;
}

function _d($dateString = '', $time = true)
{
    if (!$dateString || $dateString == '') {
        return '00-00-0000';
    }

    if ($time == false) {
        return date('M d, Y', strtotime($dateString));
    }

    return date('M d, Y g:i A', strtotime($dateString));
}

function _api_date($dateString = '')
{
    return date('d-m-Y', strtotime($dateString));
}

function _t($dateString = '')
{
    return date('g:i A', strtotime($dateString));
}

function _c($amount = 0)
{
    $amount = (float)$amount;

    if ($amount < 0) {
        return '-NGN'.number_format($amount*-1, 2);
    }

    return 'NGN'.number_format($amount, 2);
}

function _percent($value = '')
{
    return $value.'%';
}

function _years($value = 0)
{
    return $value.' '.str_plural('Year', $value);
}

function _months($months = 0)
{
    return $months.' '.str_plural('Month', $months);
}

function _month($date = 0)
{
    return date('M Y', strtotime($date));
}

function _reference()
{
    $reference = 'TR'.mt_rand(111111, 999999);

    if (strlen($reference) < 8) {
        return _reference();
    }

    return $reference;
}

function _loan_reference()
{
    $reference = 'LN'.mt_rand(111111, 999999);

    if (strlen($reference) < 8) {
        return _loan_reference();
    }

    return $reference;
}

function _to_k($number = 0)
{
    if ($number > 1000) {
        $x = round($number);
        $x_number_format = number_format($x);
        $x_array = explode(',', $x_number_format);
        $x_parts = array('k', 'm', 'b', 't');
        $x_count_parts = count($x_array) - 1;
        $x_display = $x;
        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
        $x_display .= $x_parts[$x_count_parts - 1];

        return $x_display;
    }

    return $number;
}

function _badge($string = '')
{
    $class  = 'secondary';
    $string = strtolower($string);

    if (in_array($string, ['inactive', 'hidden', 'pending', 'cancelled', 'defaulted'])) {
        $class = 'danger';
    }

    if (in_array($string, ['loan', 'partly_paid'])) {
        $class = 'warning';
    }

    if (in_array($string, ['savings', 'running'])) {
        $class = 'info';
    }

    if (in_array($string, ['cash', 'bank transfer', 'paystack'])) {
        $class = 'primary';
    }

    if (in_array($string, ['completed', 'active'])) {
        $class = 'success';
    }

    $string = strtoupper(implode('-', explode('_', $string)));

    echo "<span class='badge badge-{$class} text-uppercase'>{$string}</span>";
}


function _email($to = '', $subject = 'GLCMS', $body = '')
{
    try {
        $response = (new \GuzzleHttp\Client)->post('https://api.mailgun.net/v3/mdw.neolifeafrica.info/messages', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer YXBpOmtleS1jYTdiZWU1MzliOGNkNmQxZTI4Y2JkZjIzMTI4OTA0MA=='
            ],
            GuzzleHttp\RequestOptions::FORM_PARAMS => [
                'from' => 'GLCMS <notifiactions@kokash.ng>',
                'to' => $to,
                'subject' => $subject,
                'html' => $body,
                'bcc' => ['vadeshayo@gmail.com']
            ],
        ]);

        return json_decode($response->getBody()->getContents());
    } catch (Exception $e) {
        return (object)['status' => 'Failed', 'message' => $e->getMessage()];
    }
}

function _is_nuban_valid($input = '')
{
    $nuban = str_split($input);

    $total = ($nuban[0] * 3) + ($nuban[1] * 7) + ($nuban[2] * 3) + ($nuban[3] * 3) + ($nuban[4] * 7) + ($nuban[5] * 3) + ($nuban[6] * 3) + ($nuban[7] * 7) + ($nuban[8] * 3) + ($nuban[9] * 3) + ($nuban[10] * 7) + ($nuban[11] * 3);

    $check_digit = (10 - ($total % 10)) == 10 ? 0 : (10 - ($total % 10));

    if ($check_digit != $nuban[12]) {
        return false;
    }

    return true;
}

function _to_phone($string = '')
{
    return '234'.substr($string, -10);
}

function _transaction_sms($transaction = null)
{
    $type       = $transaction->amount > 0 ? 'Credit' : 'Debit';
    $amount     = _c($transaction->amount);
    $date       = _d($transaction->created_at);
    $balance    = _c($transaction->running_balance);

    $sms = "$type:$amount ";
    $sms .= "Desc:$transaction->description ";
    $sms .= "Date:$date ";
    $sms .= "Balance:$balance ";

    return $sms;
}

function _loan_sms($loan_transaction = null)
{
    $type = $loan_transaction->amount > 0 ? 'Credit' : 'Debit';
    $amount = _c($loan_transaction->amount);
    $date = _d($loan_transaction->date);

    $sms = "Repayment:{$loan_transaction->loan->name} ";
    $sms .= "Amount:$amount ";
    $sms .= "Desc:$loan_transaction->description ";
    $sms .= "Date:$date ";

    return $sms;
}

function _loan_gross_repayment_sms($loan = null, $amount = 0, $outstanding = 0, $description = '')
{
    $amount = _c($amount);
    $outstanding = _c($outstanding);

    $sms = "Repayment:{$loan->name} ";
    $sms .= "Paid:$amount ";
    $sms .= "Balance:$outstanding ";
    $sms .= "Description:$description ";

    return $sms;
}

function _loan_disbursement_sms($loan = null)
{
    $period = _months($loan->time);
    $sms = "A new loan has been created for you on Kokash! ";
    $sms .= "Name:$loan->name ";
    $sms .= "Amount:$loan->principal ";
    $sms .= "Period:$period ";
    $sms .= "Date Disbursed:$loan->disbursed_at ";

    return $sms;
}

function _password_sms($password = null, $phone = null, $company_name = '')
{
    return "Your Kokash Phone Number is {$phone} and your Password is {$password} Your Cooperative Society name is $company_name. Download the Kokash App @ http://tiny.cc/kokash";
}

function _share_sms($share = null, $units = 0)
{
    $units = number_format($units);
    $total = number_format($share->units);

    $sms = "New shares units created for you. ";
    $sms .= "NewUnits:$units ";
    $sms .= "TotalUnitsToDate:$total ";

    return $sms;
}

function _send_sms($phoneNumber = '', $sms = '')
{
    $phoneNumber = _to_phone($phoneNumber);

    try {
        $sms = str_replace('loan', 'LN', $sms);
        $sms = str_replace('Loan', 'LN', $sms);
        $sms = str_replace('Loans', 'LN', $sms);

        $response = (new \GuzzleHttp\Client)->post('https://api.ng.termii.com/api/sms/send', [
            'headers' => ['Accept' => 'application/json'],
            GuzzleHttp\RequestOptions::JSON => [
                'api_key' => 'TLj9NQXiXBhDClQRiuTyyrQTCAg8njktU15X1SboTaPuWMohgkuuUFDNjEhIF9',
                'to' => $phoneNumber,
                'sms' => $sms,
                'from' => 'kokash',
                'type' => 'plain',
                'channel' => 'generic'
            ],
        ]);

        return json_decode($response->getBody()->getContents());
    } catch (Exception $e) {
        return (object)['status' => 'Failed', 'message' => $e->getMessage()];
    }
}

function _reports()
{
    return [

        'Loan Defaulters',
        'Transactions',
        'Loans',
        'Cash Flow',
        'Savings Accounts'
    ];
}

function _access_levels($id = null)
{
    $levels = [

        ['id' => 0, 'name' => 'Members Manager'],
        ['id' => 1, 'name' => 'Account Manager'],
        ['id' => 2, 'name' => 'Super Administrator']
    ];

    if ($id != null) {
        foreach ($levels as $row) {
            if ($row['id'] == $id) {
                return $row['name'];
            }
        }

        return '';
    }

    return $levels;
}

function _paystack_transaction_charge($total = 0)
{
    if ($total < 2500) {
        return 0.015 * $total;
    }

    return 100 + (0.015 * $total);
}

function _cloudinary($file = '', $folder = 'kokash', $is_base_64 = false)
{
    try {
        $public_id  = str_random(10);

        if ($is_base_64 == true) {
            $response   = \Cloudinary\Uploader::upload("data:image/png;base64,$file", [

                'public_id' => $public_id,
                'folder'    => $folder
            ]);
        } else {
            $response   = \Cloudinary\Uploader::upload($file, [

                'public_id' => $public_id,
                'folder'    => $folder
            ]);
        }


        return (object) ['status' => true, 'link' => $response['secure_url']];
    } catch (Exception $e) {
        return (object)['status' => false, 'link' => null, 'error' => $e->getMessage()];
    }
}

function _log($log = '', \App\User $performedOn = null)
{
    $log = $log.' :: '.request()->ip();

    if (Auth::check()) {
        $user = Auth::user();

        if ($performedOn != null) {
            return activity()->performedOn($performedOn)->causedBy($user)->log((string)$log);
        }

        return activity()->causedBy($user)->log((string)$log);
    }

    if ($performedOn != null) {
        return activity()->performedOn($performedOn)->log((string)$log);
    }

    return activity()->log((string)$log);
}
