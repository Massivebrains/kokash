<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BankStatementPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if($user->type == 'zeus') return true;
    }

    public function view(User $user, Statement $statement = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }

    
    public function update(User $user, Statement $statement = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }

    public function delete(User $user, Statement $statement = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }

    public function restore(User $user, Statement $statement = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }

    public function forceDelete(User $user, Statement $statement = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }
}
