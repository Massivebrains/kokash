<?php

namespace App\Policies;

use App\User;
use App\LoanTransaction;
use Illuminate\Auth\Access\HandlesAuthorization;

class LoanTransactionPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if($user->type == 'zeus') return true;
    }

    public function view(User $user, LoanTransaction $loanTransaction = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }

    public function create(User $user)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }

    public function update(User $user, LoanTransaction $loanTransaction = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }

    public function delete(User $user, LoanTransaction $loanTransaction = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }


    public function restore(User $user, LoanTransaction $loanTransaction = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }

    public function forceDelete(User $user, LoanTransaction $loanTransaction = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }
}
