<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SharePolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if($user->type == 'zeus') return true;
    }

    public function create(User $user)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }

    public function update(User $user, User $model = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 1;
    }

   
    public function delete(User $user, User $model = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }


    public function restore(User $user, User $model = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }

    public function forceDelete(User $user, User $model = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 1;
    }
}
