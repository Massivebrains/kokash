<?php

namespace App\Policies;

use App\User;
use App\Transaction;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if($user->type == 'zeus') return true;
    }
    
    public function view(User $user, Transaction $transaction = null)
    {
        return !is_null($user->access_level) && $user->access_level > 0;
    }

    public function create(User $user)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }

    public function update(User $user, Transaction $transaction = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 1;
    }

    public function delete(User $user, Transaction $transaction = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }

    public function restore(User $user, Transaction $transaction = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }

    public function forceDelete(User $user, Transaction $transaction = null)
    {
        return !is_null($user->access_level) && $user->access_level  > 0;
    }
}
