<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $guarded   = ['updated_at'];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public static function keys()
    {
        return [

            'savings_interest',
            'loan_fine_fee',
            'paystack_sub_account_code'
        ];

    }

    public static function setting($company_id)
    {
        return Setting::firstOrNew(['company_id' => $company_id]);
    }

    public static function value($key = null, $company_id = 0)
    {
    	if($key == null)
    		return null;

    	$company_id = $company_id == 0 ?? _cid();

    	$settings = Setting::where(['company_id' => $company_id])->first();

        if(!$settings || !isset($settings->{$key}))
            return null;

    	return $setting->{$key};
    }

    public static function set($key = null, $value = null, $company_id = 0)
    {
    	if($key == null || $value == null || $company_id == 0)
    		return null;

    	Setting::updateOrCreate(['company_id' => $company_id], [$key => $value]);

    	return $value;
    }
}
