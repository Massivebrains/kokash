<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankStatementBreakdown extends Model
{
    protected $hidden   = ['deleted_at'];
    protected $guarded  = ['updated_at'];

    public function bank_statement()
    {
    	return $this->belongsTo('App\BankStatement')->withDefault();
    }

    public function transaction()
    {
    	return $this->belongsTo('App\Transaction')->withDefault();
    }

    public function loan()
    {
    	return $this->belongsTo('App\Loan')->withDefault();
    }
}
