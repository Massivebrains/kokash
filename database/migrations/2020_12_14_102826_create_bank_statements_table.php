<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankStatementsTable extends Migration
{
    public function up()
    {
        Schema::create('bank_statements', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('company_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('possible_user_id')->nullable();
            $table->enum('bank', ['GTBank', 'StanbicBank'])->nullable();
            $table->string('account_number')->nullable();
            $table->datetime('transaction_date')->nullable();
            $table->decimal('amount', 8, 2)->nullable();
            $table->text('reference')->nullable();
            $table->datetime('business_month')->nullable();
            $table->enum('status', ['pending', 'matched', 'discarded'])->default('pending')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::dropIfExists('bank_statements');
    }
}
