<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanTransactionsTable extends Migration
{

    public function up()
    {
        Schema::create('loan_transactions', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('loan_id');
            $table->integer('user_id');
            $table->integer('company_id');
            $table->string('reference');
            $table->decimal('principal', 10, 2);
            $table->decimal('interest', 10, 2);
            $table->decimal('amount', 10, 2);
            $table->decimal('paid', 10, 2);
            $table->decimal('outstanding', 10, 2);
            $table->decimal('balance', 10, 2)->nullable();
            $table->datetime('date')->nullable();
            $table->text('description')->nullable();
            $table->integer('payment_source_id')->nullable();
            $table->integer('fine_transaction_id')->nullable();
            $table->string('gateway_reference')->nullable();
            $table->enum('status', ['pending', 'partly_paid', 'completed', 'cancelled'])->default('pending')->nullable();
            $table->datetime('payment_date')->nullabe();
            $table->datetime('fine_date')->nullabe();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::dropIfExists('loan_transactions');
    }
}
