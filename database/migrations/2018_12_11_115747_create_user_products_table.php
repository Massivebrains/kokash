<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProductsTable extends Migration
{

    public function up()
    {
        Schema::create('user_products', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id');
            $table->enum('type', ['savings', 'loan', 'charge'])->default('savings');
            $table->enum('for', ['self', 'beneficiary'])->default('self');
            $table->integer('for_user_id')->nullable();
            $table->integer('loan_id')->nullable();
            $table->string('label')->nullable();
            $table->string('description')->nullable();
            $table->string('notes')->nullable();
            $table->decimal('amount', 10, 2)->default(0)->nullable();
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::dropIfExists('user_products');
    }
}
