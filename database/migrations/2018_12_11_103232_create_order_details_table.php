<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('order_id');
            $table->enum('type', ['savings', 'loan', 'charge'])->default('savings');
            $table->enum('for', ['self', 'beneficiary'])->default('self');
            $table->integer('user_id')->default(0)->nullable();
            $table->integer('loan_id')->default(0)->nullable();
            $table->integer('loan_transaction_id')->default(0)->nullable();
            $table->integer('transaction_id')->default(0)->nullable();
            $table->decimal('amount', 10, 2)->nullable();
            $table->enum('status', ['pending', 'paid', 'cancelled', 'completed'])->default('pending');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
