<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatementsTable extends Migration
{

    public function up()
    {
        Schema::create('statements', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id');
            $table->integer('company_id');
            $table->integer('transaction_id')->nullable();
            $table->datetime('start_date');
            $table->datetime('end_date');
            $table->enum('status', ['pending', 'completed'])->default('pending')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('statements');
    }
}
