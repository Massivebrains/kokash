<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSavingsAccountsTable extends Migration
{

    public function up()
    {
        Schema::create('savings_accounts', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id');
            $table->integer('company_id');
            $table->string('account_number')->nullable();
            $table->integer('bank_id')->nullable();
            $table->string('bank_account_number')->nullable();
            $table->string('bank_account_name')->nullable();
            $table->decimal('monthly_savings', 10, 2)->nullable()->default(0);
            $table->decimal('balance', 10, 2)->nullable()->default(0);
            $table->decimal('savings_interest')->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('savings_account');
    }
}
