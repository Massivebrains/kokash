<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppLogsTable extends Migration
{
    public function up()
    {
        Schema::create('app_logs', function (Blueprint $table) {

            $table->increments('id');
            $table->text('log')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('app_logs');
    }
}
