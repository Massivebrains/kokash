<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id');
            $table->integer('company_id');
            $table->integer('loan_category_id');
            $table->string('name');
            $table->string('reference');
            $table->decimal('principal', 9, 2);
            $table->decimal('disbursed', 9, 2);
            $table->decimal('rate', 9, 2);
            $table->decimal('time', 9, 2);
            $table->decimal('interest', 9, 2)->nullable();
            $table->decimal('amount', 9, 2)->nullable();
            $table->decimal('outstanding', 9, 2)->nullable();
            $table->decimal('monthly_payment', 9, 2)->nullable();
            $table->enum('type', ['simple', 'compound'])->default('simple')->nullable();
            $table->enum('status', ['pending', 'running', 'completed', 'cancelled'])->default('pending');
            $table->timestamp('start_at')->nullable();
            $table->enum('repayment_type', ['deduct_interest', 'add_interest'])->default('deduct_interest')->nullable();
            $table->timestamp('disbursed_at')->nullable();
            $table->text('disbursement_notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
