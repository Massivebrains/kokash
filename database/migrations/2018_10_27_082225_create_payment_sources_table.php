<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentSourcesTable extends Migration
{
    public function up()
    {
        Schema::create('payment_sources', function (Blueprint $table) {

         $table->increments('id');
         $table->string('name');
         $table->enum('status', ['active', 'inactive'])->default('active');
         $table->timestamps();
         $table->softDeletes();

     });
    }


    public function down()
    {
        Schema::dropIfExists('payment_sources');
    }
}
