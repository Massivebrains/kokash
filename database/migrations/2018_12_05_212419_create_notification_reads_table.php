<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationReadsTable extends Migration
{
    public function up()
    {
        Schema::create('notification_reads', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('notification_id');
            $table->integer('user_id');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('notification_reads');
    }
}
