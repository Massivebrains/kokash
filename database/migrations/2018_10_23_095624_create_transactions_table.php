<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{

    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id');
            $table->integer('company_id');
            $table->integer('transaction_type_id');
            $table->string('reference');
            $table->decimal('amount', 10, 2);
            $table->decimal('running_balance', 10, 2)->nullable();
            $table->text('description')->nullable();
            $table->integer('payment_source_id')->nullable();
            $table->string('gateway_reference')->nullable();
            $table->enum('status', ['pending', 'completed', 'cancelled'])->default('completed')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
