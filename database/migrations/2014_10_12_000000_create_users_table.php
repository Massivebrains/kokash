<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->enum('sex', ['male', 'female'])->default('male')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('state_id')->nullable();
            $table->enum('marital_status', ['single', 'married'])->default('single')->nullable();
            $table->text('address')->nullable();
            $table->string('occupation')->nullable();
            $table->string('employer')->nullable();
            $table->string('employer_address')->nullable();
            $table->integer('referral_id')->nullable();
            $table->string('next_of_kin')->nullable();
            $table->string('next_of_kin_phone')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->boolean('change_password_on_login')->default(false)->nullable();
            $table->enum('type', ['user', 'admin', 'zeus'])->nullable();
            $table->integer('impersonating')->nullable()->default(0);
            $table->string('api_token')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('inactive')->nullable();
            $table->enum('access_level', [0, 1, 2])->default(0)->nullable()->comment('0 : members_manager, 1: account_manager, 2: management');
            $table->string('photo')->nullable();
            $table->rememberToken();
            $table->timestamp('last_login_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
