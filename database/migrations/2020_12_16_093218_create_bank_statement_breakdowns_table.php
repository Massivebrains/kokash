<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankStatementBreakdownsTable extends Migration
{
    
    public function up()
    {
        Schema::create('bank_statement_breakdowns', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('bank_statement_id')->nullable();
            $table->enum('type', ['transaction', 'share', 'registration', 'loan'])->nullable();
            $table->integer('transaction_id')->nullable();
            $table->integer('loan_id')->nullable();
            $table->integer('loan_category_id')->nullable();
            $table->integer('share_id')->nullable();
            $table->double('amount', 8, 2)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bank_statement_breakdowns');
    }
}
