<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDebitCardsTable extends Migration
{
    public function up()
    {
        Schema::create('debit_cards', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id');
            $table->integer('company_id')->nullable();
            $table->string('email')->nullable();
            $table->string('authorization_code')->nullable();
            $table->string('bin')->nullable();
            $table->string('last4')->nullable();
            $table->string('exp_month')->nullable();
            $table->string('exp_year')->nullable();
            $table->string('channel')->nullable();
            $table->string('card_type')->nullable();
            $table->string('bank')->nullable();
            $table->string('country_code')->nullable();
            $table->string('brand')->nullable();
            $table->boolean('reusable')->nullable();
            $table->string('signature')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();

        });
    }


    public function down()
    {
        Schema::dropIfExists('debit_cards');
    }
}
