<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{

    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('company_id');
            $table->string('savings_interest')->nullable()->default(0);
            $table->boolean('disburse_offline')->nullable()->default(0);
            $table->boolean('disburse_online')->nullable()->default(0);
            $table->decimal('loan_fine_fee', 10, 2)->nullable()->default(0);
            $table->string('paystack_sub_account_code')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
