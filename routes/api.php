<?php

use Illuminate\Http\Request;

Route::post('login', 'AuthController@login');
Route::get('process-order/{order_id}', 'OrdersController@process');

Route::group(['middleware' => 'auth:api'], function(){

    Route::get('dashboard', 'DashboardController@index');
    Route::get('profile', 'ProfileController@profile');
    Route::get('transactions', 'TransactionsController@index');
    Route::get('products', 'UserProductsController@index');
    Route::get('loans', 'LoansController@index');
    Route::get('loans/transactions/{id}', 'LoansController@transactions');
    Route::get('notifications', 'NotificationsController@index');
    Route::get('faqs', 'FaqsController@index');
    Route::get('statements', 'StatementsController@index');
    Route::post('statement', 'StatementsController@save');
    Route::post('upload-photo', 'ProfileController@uploadPhoto');
    Route::post('edit-profile', 'ProfileController@editProfile');
    Route::post('change-password', 'ProfileController@changePassword');
    Route::get('banks', 'ProfileController@banks');

    Route::group(['prefix' => 'order'], function(){

        Route::post('/', 'OrdersController@order');
    });

    Route::group(['prefix' => 'paystack'], function(){

        Route::post('initialize', 'PaystackController@initialize');
    });
 
});
