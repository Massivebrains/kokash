<?php

use App\Jobs\SendEmailJob;
use App\Loan;
use App\Transaction;
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('/', 'HomeController@index');
    Route::any('contact', 'ContactController@index');
    Route::any('terms', 'ContactController@index');
});

Route::get('login', 'AuthController@index');
Route::post('login', 'AuthController@login')->name('login');
Route::get('logout', 'AuthController@logout')->name('logout');
Route::get('activate/{token?}', 'AuthController@activate');
Route::get('undo-impersonation', 'AuthController@undoImpersonate');
Route::any('magic-link/{token?}', 'AuthController@magicLink');
Route::any('forgot-password/{token?}', 'AuthController@forgotPassword');
Route::any('reset-password/{token?}', 'AuthController@resetPassword');
Route::any('transaction-reciept/{id?}', 'TransactionsController@reciept');


Route::group(['middleware' => ['auth', 'admin']], function () {
    Route::get('dashboard', 'DashboardController@index');

    Route::any('members', 'MembersController@index');
    Route::get('member/{id?}', 'MembersController@member');
    Route::post('create-member', 'MembersController@createMember');
    Route::post('update-member', 'MembersController@updateMember');


    Route::get('member-info/{id?}', 'MemberController@index');
    Route::get('member-status/{id?}', 'MemberController@status');
    Route::post('save-bank-information/{id?}', 'MemberController@bank');
    Route::post('save-beneficiary', 'MemberController@saveBeneficiary');
    Route::get('remove-beneficiary/{id?}', 'MemberController@removeBeneficiary');
    Route::post('save-monthly-savings', 'MemberController@saveMonthlySavings');
    Route::post('change-member-password', 'MemberController@changePassword');
    Route::get('delete-member/{id?}', 'MemberController@deleteMember');

    Route::get('deposit', 'SavingsController@deposit');

    Route::any('transactions', 'TransactionsController@index');
    Route::any('transaction/{id?}', 'TransactionsController@transaction');
    Route::any('credit-form', 'TransactionsController@creditForm');
    Route::any('save-credit-transaction', 'TransactionsController@saveCreditTransaction');

    Route::any('bulk-transactions', 'BulkTransactionsController@index');
    Route::any('review-bulk-transactions', 'BulkTransactionsController@review');
    Route::any('save-bulk-transactions', 'BulkTransactionsController@save');
    Route::get('upload-bulk-transactions', 'BulkTransactionsController@uploadPage');
    Route::post('upload-bulk-transactions', 'BulkTransactionsController@upload');

    Route::any('bulk-loans', 'BulkLoansController@index');
    Route::any('review-bulk-loans', 'BulkLoansController@review');
    Route::any('save-bulk-loans', 'BulkLoansController@save');
    Route::get('upload-bulk-loans', 'BulkLoansController@uploadPage');
    Route::post('upload-bulk-loans', 'BulkLoansController@upload');

    Route::any('statements', 'StatementsController@index');
    Route::any('statement/{id?}', 'StatementsController@statement');


    Route::any('notifications', 'NotificationsController@index');
    Route::get('notification-switch', 'NotificationsController@switchNotification');
    Route::get('notification/{id?}', 'NotificationsController@form');
    Route::post('save-notification/{id?}', 'NotificationsController@save');

    Route::any('faqs', 'FaqsController@index');
    Route::get('faq/{id?}', 'FaqsController@form');
    Route::post('save-faq/{id?}', 'FaqsController@save');

    Route::get('loan-categories', 'LoanCategoriesController@index');
    Route::get('loan-category/{id}', 'LoanCategoriesController@form');
    Route::post('loan-category', 'LoanCategoriesController@save');

    Route::any('loans', 'LoansController@index');
    Route::any('loan/{id?}', 'LoansController@loan');
    Route::any('loan-form', 'LoansController@form');
    Route::any('review-loan', 'LoansController@review');
    Route::any('save-loan', 'LoansController@save');
    Route::any('disburse-offline', 'LoansController@disburseOffline');
    Route::post('complete-loan-transaction', 'LoansController@completeLoanTransaction');

    Route::any('orders', 'OrdersController@index');
    Route::any('order/{order_id?}', 'OrdersController@order');
    Route::any('order-status/{order_id?}/{status?}', 'OrdersController@status');
    Route::post('complete-order', 'OrdersController@completeOrder');

    Route::get('report', 'ReportsController@index');
    Route::post('report', 'ReportsController@report');
    Route::post('report-daily-payouts', 'ReportsController@dailyPayouts');

    Route::any('admins', 'AdminsController@index');
    Route::get('admin/{id?}', 'AdminsController@admin');
    Route::post('save-admin/{id?}', 'AdminsController@saveAdmin');

    Route::any('shares', 'SharesController@index');
    Route::any('share/{id?}', 'SharesController@form');
    Route::any('save-shares', 'SharesController@save');
    Route::any('share-history/{id?}', 'SharesController@history');
    Route::get('update-share/{id?}', 'SharesController@updateForm');

    Route::group(['namespace' => 'BankStatement', 'prefix' => 'bank-statement'], function () {
        Route::any('/matched', 'MatchedController@index');
        Route::get('/discarded', 'MatchedController@discarded');
        Route::get('/restore/{id?}', 'MatchedController@restore');
        Route::post('/match', 'MatchController@index');
        Route::get('/breakdown/{id?}', 'MatchedController@breakdown');
        Route::any('/unmatched', 'UnmatchedController@index');
        Route::get('/unmatched/statements', 'UnmatchedController@statements');
        Route::get('/unmatched/discard/{id?}', 'UnmatchedController@discard');
        Route::get('/unmatched/statement/{id?}/{new_possible_user_id?}', 'UnmatchedController@statement');
        Route::get('/upload', 'UploadController@index');
        Route::post('upload', 'UploadController@upload');
        Route::get('/report', 'ReportController@index');
        Route::post('report', 'ReportController@report');
        Route::get('reverse/{id}', 'ReversalController@index');
        Route::get('split/{id}', 'SplitController@index');
        Route::post('split', 'SplitController@split');
    });
});

Route::group(['middleware' => ['auth', 'zeus'], 'prefix' => 'zeus', 'namespace' => 'Zeus'], function () {
    Route::get('dashboard', 'DashboardController@index');
    Route::get('dashboard/app-logs', 'DashboardController@appLogs');

    Route::get('companies', 'CompanyController@index');
    Route::any('company/setting/{id?}', 'CompanyController@setting');
    Route::get('company/impersonate/{id?}', 'CompanyController@impersonate');
    Route::get('company/{id}', 'CompanyController@company');
    Route::post('save-company/{id?}', 'CompanyController@save');

    Route::get('payment-sources', 'PaymentSourcesController@index');
    Route::get('payment-source/{id}', 'PaymentSourcesController@paymentSource');
    Route::post('save-payment-source', 'PaymentSourcesController@savePaymentSource');

    Route::get('transaction-types', 'TransactionTypesController@index');
    Route::get('transaction-type/{id}', 'TransactionTypesController@transactionType');
    Route::post('save-transaction-type', 'TransactionTypesController@saveTransactionType');

    Route::group(['namespace' => 'Migration'], function () {
        Route::get('migration', 'MigrationController@index');
        Route::post('migration/members', 'MembersController@index');
        Route::post('migration/transactions', 'TransactionsController@index');
        Route::post('migration/shares', 'TransactionsController@shares');
        Route::post('migration/loans', 'LoansController@index');
        Route::get('migration/process', 'LoansController@process');
    });
});

Route::group(['prefix' => 'utils'], function () {
    Route::get('states/{id?}', 'UtilityController@states');
});

Route::group(['prefix' => 'paystack'], function () {
    Route::get('pay/{order_id?}', 'PaystackController@pay');
    Route::get('requery/{order_id?}/{reference?}', 'PaystackController@requery');
    Route::get('cards/{order_id?}', 'PaystackController@cards');
    Route::get('charge/{order_id?}/{debit_card_id?}', 'PaystackController@charge');
});

Route::group(['prefix' => 'webview'], function () {
    Route::get('pay', 'PayController@form');
    Route::post('review', 'PayController@review');
    Route::post('confirm', 'PayController@confirm');
    Route::any('password-reset', 'AuthController@webViewPasswordReset');
    Route::any('resend-login-details', 'AuthController@resendLoginDetails');
});


Route::get('cron', 'Cron\JobsController@bankStatementPossibleUsers');

Route::get('/resend-transaction-email/{id}', function () {
    $transaction = Transaction::find(request('id'));
    $amount = number_format($transaction->amount, 2);
    $subject    = 'Transaction Alert [SAVINGS: '.$amount.' ]';
    $body       = view('emails.transaction-alert', ['transaction' => $transaction])->render();
    SendEmailJob::dispatch($transaction->user, $subject, $body);
    echo $body;
});

Route::get('/resend-loan-email', function () {
    if (request('resend')) {
        $loans = Loan::get();
        foreach ($loans as $row) {
            $subject    = $row->name.' Loan Alert - as at Dec 2021';
            $body       = view('emails.loan-upload', ['loan' => $row])->render();
            SendEmailJob::dispatch($row->user, $subject, $body);
        }
        echo $body;
    }
});
